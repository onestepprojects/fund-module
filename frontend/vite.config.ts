import { defineConfig } from '@onestepprojects/frontend-infrastructure/vite.config'

export default defineConfig({ base: '/fund/', tailwindcss: true })
