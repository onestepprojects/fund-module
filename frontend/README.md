# health-module

> OneStep Health Module

[![pipeline status](https://gitlab.com/onestepprojects/fund-module/badges/main/pipeline.svg)](https://gitlab.com/onestepprojects/fund-module/-/commits/main)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

### Developing with the app webpack dev server

Install dependencies and start module in watch mode.

```bash
# health-module/frontend
npm install
npm start
```

Link module and start app dev server.

```bash
# one-step-ui
npm install
npm link ../fund-module/frontend
npm start
```

Open https://localhost:3000/ in your browser.

## Usage

```bash
npm install --save @onestepprojects/fund-module
```

```jsx
import FundModule from '@onestepprojects/fund-module'
import '@onestepprojects/fund-module/dist/index.css'

const Example = () => {
  return <FundModule />
}
```

## Environment Variables

- REACT_APP_FUND_MODULE_API_URL

## License

MIT © [mdeforest](https://github.com/mdeforest)
