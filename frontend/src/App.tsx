import { type FC } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.pcss'
import { blockchainApi, fundApi, organizationApi } from './services'
import type { Person } from './types'
import { FundContribute, FundCreate, FundList, FundManage, FundView } from './views'

interface FundModuleProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
  /** Module router base name. */
  mountPath: string
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  authHelper?: any
}

const FundModule: FC<FundModuleProps> = ({ auth, authHelper, mountPath }) => {
  /** Set the axios auth header immediately and keep it updated. */
  ;[blockchainApi, fundApi, organizationApi].forEach((api) => {
    if (auth?.token) {
      api.defaults.headers.Authorization = `Bearer ${auth.token}`
    } else {
      delete api.defaults.headers.Authorization
    }
  })

  return (
    <BrowserRouter basename={mountPath}>
      <div id='fundModule' className='container mx-auto p-8 min-h-[calc(100vh-120px)]'>
        <Routes>
          <Route path='/' element={<FundList auth={auth} />} />
          <Route path='/view/persions/:persionId/funds' element={<FundList auth={auth} />} />
          <Route
            path='/view/organizations/:organizationId/funds'
            element={<FundList auth={auth} />}
          />
          <Route
            path='/view/organizations/:organizationId/projects/:projectId/funds'
            element={<FundList auth={auth} />}
          />
          <Route path='/create' element={<FundCreate auth={auth} />} />
          <Route path='/manage/:id' element={<FundManage auth={auth} />} />
          <Route path='/view/:id' element={<FundView auth={auth} />} />
          <Route path='/contribute/:id' element={<FundContribute />} />
          {/* <Route
            exact
            path='/distribute/:id'
            render={(props) => (
              <FundDistribute {...props} authHelper={authHelper} person={person} />
            )}
          /> */}
          {/* <Route
            path='/view/:id/discussions'
            element={<Discussions auth={auth} authHelper={authHelper} />}
          /> */}
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default FundModule
