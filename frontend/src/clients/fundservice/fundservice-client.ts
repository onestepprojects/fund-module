/*
    Client functions to access OneStep relief platform's Crowd Fund Service API.
*/

import axios, { AxiosResponse } from 'axios'

// TODO: set from env, but how?
const API_URL =
  process.env.REACT_APP_FUND_MODULE_API_URL +
  (process.env.REACT_APP_FUND_MODULE_API_URL.endsWith('/') ? '' : '/')
const ORG_API_URL =
  process.env.REACT_APP_ORG_MODULE_API_URL +
  (process.env.REACT_APP_ORG_MODULE_API_URL.endsWith('/') ? '' : '/')
// const API_URL = "http://localhost:8080/crowdfund/";
// const API_URL = "https://onestep-48a12201efc6b936.elb.us-east-1.amazonaws.com:8080/crowdfund/";

export interface Fund {
  name: string
  escrow: {
    address: string
  }
}

/*
    Gets a fund by ID.

    @param fundId ID of fund
    @return fund in data
*/
export async function getFund(fundId): Promise<AxiosResponse<Fund, any>> {
  const path = 'fund/'
  console.log('ATTEMPTING GET: ' + API_URL + path + fundId)
  try {
    const response = await axios.get<Fund>(API_URL + path + fundId)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Gets all funds.

    @return list of funds in data
*/
export async function getFunds(params = {}) {
  const path = 'funds'
  const url = new URL(API_URL + path)
  Object.keys(params).forEach((key) => url.searchParams.append(key, params[key]))
  try {
    const response = await axios.get(url.toString())
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Gets all active funds (PUBLISHED and IN_PROGRESS).

    @return list of funds in data
*/
export async function getActiveFunds() {
  const path = 'funds/active'
  try {
    const response = await axios.get(API_URL + path)

    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Gets funds by a desired visibility (PUBLISHED | UNPUBLISHED).

    @return list of funds in data
*/
export async function getFundsByVisibility(vis) {
  const path = 'funds/visibility/'
  try {
    const response = await axios.get(API_URL + path + vis)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Gets funds for the logged in user by auth token.

    @return list of funds in data
*/
export async function getFundsByUser(authHelper) {
  const path = 'funds/user'
  try {
    // const response = await axios.get(API_URL + path);
    const response = await authHelper.Get(API_URL + path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Creates a fund.

    @param data details of fund to create. should be ready to be shipped off in a POST (may revisit)
    @return fund in data
*/
export async function createFund(authHelper, data) {
  const path = 'fund'
  try {
    // const response = await axios.post(API_URL + path, data);
    const response = await authHelper.Post(API_URL + path, data)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Updates a fund.

    @param data details of fund to create. should be ready to be shipped off in a PUT (may revisit)
    @return fund in data
*/
export async function updateFund(authHelper, data) {
  const path = 'fund'

  try {
    // const response = await axios.put(API_URL + path, data);
    const response = await authHelper.Put(API_URL + path, data)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Uploads fund image.

    @param data details of fund to create. should be ready to be shipped off in a PUT (may revisit)
    @return fund in data
*/
export async function uploadFundImage(authHelper, data, fundId) {
  const path = 'fund/image/'

  try {
    // const response = await axios.put(API_URL + path + fundId, data);
    const response = await authHelper.Put(API_URL + path + fundId, data)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Deletes a fund by ID.

    @param fundId ID of fund
    @return 204 on success or 404 if not found
*/
export async function deleteFund(authHelper, fundId) {
  const path = 'fund/'
  try {
    // const response = await axios.delete(API_URL + path + fundId);
    const response = await authHelper.Delete(API_URL + path + fundId)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Creates an Escrow account for a fund by ID.

    @param fundId ID of fund
    @return { data: { escrowAddress: ... , escrowMnemonic: ... } }
*/
export async function createEscrow(authHelper, fundId) {
  const path = 'escrow/'
  try {
    // const response = await axios.get(API_URL + path + fundId);
    const response = await authHelper.Get(API_URL + path + fundId)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Gets a fund balance via ID by querying blockchain.

    @param fundId ID of fund
    @return { data: { fundBalance: ... } }, or FALSE if unable to retrieve
*/
export async function getFundBalance(fundId) {
  const path = 'fund/' + fundId + '/balance'
  try {
    const response = await axios.get(API_URL + path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Retrieves list of transactions for given fund ID.

    @param fundId ID of fund
    @return { data: [ {fund1}, {fund2}, etc. ] }
*/
export async function getFundTransactions(fundId) {
  const path = 'fund/' + fundId + '/transactions'
  try {
    const response = await axios.get(API_URL + path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Donates to a fund.

    @param data details of fund to create. should be ready to be shipped off in a POST
    @return response
    }
*/
export async function makeDonation(authHelper, data) {
  const path = 'donation'

  try {
    // anyone can donate. if authHelper is available, use that so that backend can ID the person.
    const response = authHelper
      ? await authHelper.Post(API_URL + path, data)
      : await axios.post(API_URL + path, data)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Distributes money from a fund to a receiver account.

    @param data details of fund to create. should be ready to be shipped off in a POST (may revisit)
    @return response object. pertinent data is in:
    { ...
        data: { txId: ..., amount: ..., ... }
    }
*/
export async function distributeFunds(authHelper, data) {
  const path = 'distribution'

  try {
    // const response = await axios.post(API_URL + path, data);
    const response = await authHelper.Post(API_URL + path, data)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Retrieves list of orgs based on person.

    @param personId
    @return { data: [ {org1}, {org2}, etc. ] }
*/

export async function getOrgs(personId) {
  const path = ORG_API_URL + 'organizations/get-org-roles-by-person/' + personId
  try {
    const response = await axios.get(path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Retrieve org by org ID.

    @param orgId
    @return org data
*/

export async function getOrg(orgId) {
  const path = ORG_API_URL + 'organizations/' + orgId
  try {
    const response = await axios.get(path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Retrieves list of projects based on org (parent).

    @param orgId
    @return list of projects
*/

export async function getProjects(orgId) {
  const path = ORG_API_URL + 'projects/sub-projects/' + orgId
  try {
    const response = await axios.get(path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Retrieves project by ID.

    @param projectId
    @return project
*/

export async function getProject(projectId) {
  const path = ORG_API_URL + 'projects/' + projectId
  try {
    const response = await axios.get(path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Retrieves person by ID.

    @param personId
    @return person
*/

export async function getPerson(personId) {
  const path = ORG_API_URL + 'persons/' + personId
  try {
    const response = await axios.get(path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

/*
    Returns true if orgArray contains Admin role.
    if orgId is provided. OrgId is also matched before checking for Admin role

    @param orgs- a list of org objects,  orgId = id for a specific org to check for.
    @return true/false
*/

export function isAdmin(orgs = [], orgId) {
  const adminRoles = ['GLOBAL_ADMIN', 'ADMIN', 'RELIEFORGADMIN']

  for (const org of orgs) {
    for (const role of org.roles) {
      // If role is an admin role then check orgID if required
      if (adminRoles.includes(role)) {
        if (orgId === '') return true
        else {
          if (org.id === orgId) return true
        }
      }
    }
  }
  return false
}
