import axios from 'axios'
import type { Organization, PersonRole, Project } from '../types'

export const organizationApi = axios.create({
  baseURL: process.env.REACT_APP_ORG_MODULE_API_URL,
})

export const getOrganizationById = async (id: string) => {
  const { data } = await organizationApi.get<Organization>(`organizations/${id}`)
  return data
}

export const getOrganizationsByPersonId = async (personId: string) => {
  const { data } = await organizationApi.get<Organization[]>(
    `organizations/get-org-roles-by-person/${personId}`
  )
  return data
}

export const getOrganizationRolesByPersonId = async (id: string) => {
  const { data } = await organizationApi.get<PersonRole[]>(
    `organizations/get-org-roles-by-person/${id}`
  )
  return data
}

export function isOrganizationAdmin(personRoles: PersonRole[], organizationId?: string) {
  const adminRoles = ['GLOBAL_ADMIN', 'ADMIN', 'RELIEFORGADMIN']

  return !!personRoles?.some(
    ({ id, roles }) =>
      roles.some((role) => adminRoles.includes(role)) && (!organizationId || id === organizationId)
  )
}

export const getProjectById = async (id: string) => {
  const { data } = await organizationApi.get<Project>(`projects/${id}`)
  return data
}

export const getSubProjectByOrganizationId = async (organizationId: string) => {
  const { data } = await organizationApi.get<Project[]>(`projects/sub-projects/${organizationId}`)
  return data
}
