import axios from 'axios'
import type { Fund } from '../types'

interface ErrorResponse {
  message?: string
}

export const fundApi = axios.create({
  baseURL: process.env.REACT_APP_FUND_MODULE_API_URL,
})

export const getFunds = async (params = {} as { organizationId?: string; projectId?: string }) => {
  const { data } = await fundApi.get<Fund[]>(`funds?${new URLSearchParams(params)}`)
  return data
}

export const getActiveFunds = async () => {
  const { data } = await fundApi.get<Fund[]>(`funds/active`)
  return data
}

export const getMyFunds = async () => {
  const { data } = await fundApi.get<Fund[]>(`funds/user`)
  return data
}

export const getFundById = async (id: string) => {
  const { data } = await fundApi.get<Fund>(`fund/${id}`)
  return data
}

export const createFund = async (params: Partial<Fund>) => {
  const { data } = await fundApi.post<Fund>('fund', params)
  return data
}

export const updateFund = async (params: Partial<Fund>) => {
  const { data } = await fundApi.put<Fund>('fund', params)
  return data
}

export const uploadFundImage = async ({
  id,
  ...params
}: {
  id: string
  name: string
  type: string
  data: string
}) => {
  const { data } = await fundApi.put<Fund>(`fund/image/${id}`, params)
  return data
}

export async function makeFundDonation(params: {
  payId: string
  amount: number
  fundId: string
  currency: string
}): Promise<Fund & ErrorResponse> {
  const { data } = await fundApi.post<Fund & ErrorResponse>('donation', params)
  return data
}

export function submitContributor(params: {
  name: string
  email: string
  country: string
  fundId: string
  amountContributed?: number // Only available for contribute with QRCode
  pagoTransactionId?: string // Only available for contribute with Pago wallet
}) {
  return fundApi.post<void>('donations', params)
}
