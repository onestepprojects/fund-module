import { useRequest } from 'ahooks'
import axios from 'axios'
import type { Fund, TransactionContribution } from '../types'

export const blockchainApi = axios.create({
  baseURL: process.env.REACT_APP_BLOCKCHAIN_API_URI,
})

export const getAccountTransactionContributions = async (params: {
  address: string
  startDate?: string
  endDate?: string
}) => {
  const { address, startDate, endDate } = params
  const query = new URLSearchParams({ startDate, endDate })
  const { data } = await blockchainApi.get<TransactionContribution[]>(
    `account/${address}/transaction-contributions?${query}`
  )
  return data
}

export const useFundAccountTransactionContributions = (fund: Fund) => {
  return useRequest(
    async () => {
      const address = fund.escrow.address
      const contributions = await getAccountTransactionContributions({
        address,
        startDate: fund.startDate,
        endDate: fund.endDate,
      })
      return contributions
    },
    {
      refreshDeps: [fund],
      ready: !!fund?.escrow?.address,
      retryCount: 2,
      cacheKey: `fund-${fund?.id}-account-transaction-contributions`,
      staleTime: 60 * 1000,
    }
  )
}

export const useFundTotalAmountRaised = (fund: Fund) => {
  const { data: contributions } = useFundAccountTransactionContributions(fund)
  const totalAmountRaised = contributions?.[0]?.transactionTotalAmount || 0
  return totalAmountRaised
}
