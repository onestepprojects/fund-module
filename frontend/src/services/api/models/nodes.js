import { post } from '../api'

/**
 * Sets a node to be published
 *
 * @param {string} uuid
 * @param {string} language
 */
export async function publishNode(auth, uuid, language) {
  post(auth, `/funds/nodes/${uuid}/languages/${language}/published`, {})
}
