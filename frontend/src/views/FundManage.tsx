import { CreditCardOutlined, FundOutlined, LeftOutlined, SaveOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import {
  Breadcrumb,
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Modal,
  Select,
  Spin,
  Switch,
  Typography,
} from 'antd'
import type { AxiosError } from 'axios'
import { FC } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { UploadItem } from '../components'
import { ALGO_USDC_SCALE } from '../constants'
import {
  getFundById,
  getOrganizationsByPersonId,
  getSubProjectByOrganizationId,
  updateFund,
} from '../services'
import type { Fund, Person } from '../types'
import { dayjs } from '../utils'

type SubmitError = AxiosError<{
  message: string
}>

interface FundManageProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const FundManage: FC<FundManageProps> = ({ auth }) => {
  const navigate = useNavigate()
  const { id } = useParams()

  const [form] = Form.useForm<Fund>()

  const { loading: fundLoading, data: fund } = useRequest(
    async () => {
      const fund = await getFundById(id)
      form.setFieldsValue(fund)
      return fund
    },
    {
      refreshDeps: [form],
    }
  )

  const { loading: organizationOptionsLoading, data: organizationOptions } = useRequest(
    async () => {
      const organizations = await getOrganizationsByPersonId(auth.person.uuid)
      const organizationOptions = organizations.map(({ id, name }) => ({ label: name, value: id }))
      return organizationOptions
    },
    {
      refreshDeps: [auth],
      ready: !!auth?.person?.uuid,
    }
  )

  const organizationId = Form.useWatch('organizationId', form)
  const { loading: projectOptionsLoading, data: projectOptions } = useRequest(
    async () => {
      const projects = await getSubProjectByOrganizationId(organizationId)
      const projectOptions = projects.map(({ id, name }) => ({ label: name, value: id }))
      return projectOptions
    },
    {
      refreshDeps: [organizationId],
      ready: !!organizationId,
    }
  )

  const {
    loading: submitting,
    runAsync: submit,
    error,
  } = useRequest(
    async (values: Fund) => {
      const payload: Fund = { ...values, id: fund.id, currency: fund.currency }

      const res = await updateFund(payload)
      if (res?.id) {
        navigate(`/view/${res.id}`)
      }
    },
    {
      manual: true,
      refreshDeps: [fund],
      onError: (error: SubmitError) => {
        const { message, response } = error
        Modal.error({
          title: 'Something went wrong',
          content: `Server response: ${message}${response ? `, ${response.data.message}` : ''}.`,
        })
      },
    }
  )

  return (
    <div className='space-y-8'>
      <div>
        <div className='text-sm'>RELIEF FUNDS</div>
        <Breadcrumb className='text-xl'>
          <Breadcrumb.Item>
            <Link to='/'>
              <CreditCardOutlined style={{ fontSize: 20 }} /> Crowdfunds
            </Link>
          </Breadcrumb.Item>
          {fundLoading ? (
            <Breadcrumb.Item>
              <Spin size='small' />
            </Breadcrumb.Item>
          ) : (
            <>
              {fund && (
                <Breadcrumb.Item>
                  <Link to={`/view/${fund.id}`}>
                    <FundOutlined style={{ fontSize: 20 }} /> {fund.name}
                  </Link>
                </Breadcrumb.Item>
              )}
              <Breadcrumb.Item>Manage Fund</Breadcrumb.Item>
            </>
          )}
        </Breadcrumb>
      </div>

      <Spin spinning={fundLoading}>
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16, offset: 6, pull: 5 }}
          colon={false}
          form={form}
          onFinish={(values) => submit(values)}
        >
          <Form.Item
            label='Fund Name'
            name='name'
            rules={[{ required: true, message: 'Name is required' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='Description'
            name='description'
            rules={[{ required: true, message: 'Description is required' }]}
          >
            <Input.TextArea autoSize />
          </Form.Item>
          <Form.Item
            label='Relief Organization'
            name='organizationId'
            rules={[{ required: true, message: 'Organization is required' }]}
          >
            <Select
              allowClear
              showSearch
              loading={organizationOptionsLoading}
              options={organizationOptions}
            />
          </Form.Item>
          <Form.Item label='Relief Project' name='projectId' shouldUpdate>
            <Select
              allowClear
              showSearch
              loading={projectOptionsLoading}
              options={projectOptions}
            />
          </Form.Item>
          <Form.Item
            label='Start Date'
            name='startDate'
            rules={[{ required: true, message: 'Start Date is required' }]}
            getValueProps={(v) => ({ value: v ? dayjs(v, 'YYYY-MM-DD') : v })}
            getValueFromEvent={(e) => (e ? dayjs(e).format('YYYY-MM-DD') : '')}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item
            label='End Date'
            name='endDate'
            rules={[{ required: true, message: 'End Date is required' }]}
            getValueProps={(v) => ({ value: v ? dayjs(v, 'YYYY-MM-DD') : v })}
            getValueFromEvent={(e) => (e ? dayjs(e).format('YYYY-MM-DD') : '')}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item label='Blockchain Address' shouldUpdate>
            {(form) => {
              const address = form.getFieldValue(['escrow', 'address'])
              return (
                <Typography.Link
                  href={`https://algoexplorer.io/address/${address}`}
                  target='_blank'
                >
                  {address}
                </Typography.Link>
              )
            }}
          </Form.Item>
          <Form.Item
            label='Goal Amount'
            name='goalAmount'
            rules={[{ required: true, message: 'Goal Amount is required' }]}
            getValueProps={(v) => ({ value: Number.isFinite(v) ? v * ALGO_USDC_SCALE : v })}
            getValueFromEvent={(e) => (Number.isFinite(e) ? e / ALGO_USDC_SCALE : e)}
          >
            <InputNumber addonAfter='USDC' />
          </Form.Item>
          <Form.Item label='Cover Image' name='imageUrl'>
            <UploadItem />
          </Form.Item>
          <Form.Item label='Cover Video URL' name='videoUrl' extra='YouTube, Facebook, Vimeo.'>
            <Input />
          </Form.Item>
          <Form.Item label='Banner Image' name='bannerImageUrl'>
            <UploadItem />
          </Form.Item>
          <Form.Item label='Body Images'>
            <div className='flex'>
              <Form.Item noStyle name='bodyLeftImageUrl'>
                <UploadItem title='Left' />
              </Form.Item>
              <Form.Item noStyle name='bodyRightImageUrl'>
                <UploadItem title='Right' />
              </Form.Item>
            </div>
          </Form.Item>
          <Form.Item
            label='Body Left Video URL'
            name='bodyLeftVideoUrl'
            extra='YouTube, Facebook, Vimeo.'
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='Body Right Video URL'
            name='bodyRightVideoUrl'
            extra='YouTube, Facebook, Vimeo.'
          >
            <Input />
          </Form.Item>
          <Form.Item label='Report Images'>
            <div className='flex'>
              <Form.Item noStyle name='reportLeftImageUrl'>
                <UploadItem title='Left' />
              </Form.Item>
              <Form.Item noStyle name='reportMiddleImageUrl'>
                <UploadItem title='Middle' />
              </Form.Item>
              <Form.Item noStyle name='reportRightImageUrl'>
                <UploadItem title='Right' />
              </Form.Item>
            </div>
          </Form.Item>
          <Form.Item
            label='Public in the funds list'
            name='fundVisibility'
            valuePropName='checked'
            getValueProps={(v) => ({ checked: v === 'PUBLISHED' })}
            getValueFromEvent={(e) => (e ? 'PUBLISHED' : 'UNPUBLISHED')}
          >
            <Switch checkedChildren='Published' unCheckedChildren='Unpublished' />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 6 }}>
            <div className='space-x-8'>
              <Button icon={<LeftOutlined />} onClick={() => navigate(-1)}>
                Go Back
              </Button>
              <Button type='primary' htmlType='submit' icon={<SaveOutlined />} loading={submitting}>
                Save Fund
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Spin>
    </div>
  )
}
