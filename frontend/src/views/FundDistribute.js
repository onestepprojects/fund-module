/* eslint-disable no-unused-vars */
import React from 'react'
import { Link } from 'react-router-dom'

import FundDistributeForm from '../components/onestep/FundDistributeForm'

import { Button, Container, Row } from 'shards-react'

import { getFund } from '../clients/fundservice/fundservice-client'
import PageTitle from '../components/common/PageTitle'

class FundDistribute extends React.Component {
  constructor(props) {
    super(props)

    this.state = { fundName: '' }
  }

  componentDidMount() {
    // get fund name to display
    getFund(this.props.match.params.id).then((res) => {
      if (res) {
        this.setState({
          fundName: res.data.name,
          fundCurrency: res.data.currency,
        })
      }
    })
  }

  render() {
    return (
      <Container fluid className='main-content-container px-4'>
        {/* Page Header */}
        <Row noGutters className='page-header py-4'>
          <PageTitle
            sm='4'
            title={this.state.fundName}
            subtitle='Distribute Funds'
            className='text-sm-left'
          />

          <Link to={`/view/${this.props.match.params.id}`}>
            <Button size='sm' theme='primary' className='mb-2 mr-1 btn-primary'>
              Go to Fund
            </Button>
          </Link>

          <Link to='/'>
            <Button size='sm' theme='primary' className='mb-2 mr-1 btn-primary'>
              View All Funds
            </Button>
          </Link>
        </Row>

        <Row>
          <div className='fund-tile-card'>
            <FundDistributeForm
              id={this.props.match.params.id}
              authHelper={this.props.authHelper}
              fundCurrency={this.state.fundCurrency}
            />
          </div>
        </Row>
      </Container>
    )
  }
}

export default FundDistribute
