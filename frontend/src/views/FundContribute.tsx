import { CreditCardOutlined, FundOutlined, HomeOutlined, LeftOutlined } from '@ant-design/icons'
import { useMemoizedFn, useRequest } from 'ahooks'
import {
  Breadcrumb,
  Button,
  Card,
  Form,
  Input,
  InputNumber,
  QRCode,
  Radio,
  Result,
  Select,
  Space,
  Spin,
  Steps,
  Switch,
  Typography,
  type FormInstance,
  type StepProps,
} from 'antd'
import type { AxiosError } from 'axios'
import classNames from 'classnames'
import { useCallback, useState, type FC } from 'react'
import { Link, useParams } from 'react-router-dom'
import { FundContributeResult, SUBMITTING_STATUS } from '../components/onestep/FundContributeResult'
import { countryOptions } from '../constants'
import { getFundById, makeFundDonation, submitContributor } from '../services'

type ContributeError = AxiosError<{
  message: string
}>

const ALGO_USDC_SCALE = 1 / 1000000

const steps: StepProps[] = ['Basic Info', 'Contribute', 'Result'].map((title) => ({
  key: title,
  title,
}))

enum CONTRIBUTE_TYPE {
  QR_CODE,
  PAGO,
}

const NewTabLink: FC<{ href: string }> = ({ href, children }) => (
  <Typography.Link href={href} target='_blank'>
    {children}
  </Typography.Link>
)

export const FundContribute: FC = () => {
  const { id } = useParams()
  const [currentStep, setCurrentStep] = useState(0)
  const [submittingStatus, setSubmittingStatus] = useState<SUBMITTING_STATUS>(
    SUBMITTING_STATUS.NOT_SUBMIT
  )
  const [errorText, setErrorText] = useState<string>('')
  const [contributeType, setContributeType] = useState<CONTRIBUTE_TYPE>(CONTRIBUTE_TYPE.QR_CODE)

  const { data: fund, loading: fundLoading } = useRequest(() => getFundById(id))

  const [contributorForm] = Form.useForm()
  const [contributeForm] = Form.useForm()

  /** Clean all form errors, keep values and the other status untouched. */
  const resetFormErrors = useMemoizedFn((form: FormInstance) => {
    form.setFields(form.getFieldsError().map(({ name }) => ({ name, errors: [] })))
  })

  // Callback of contributor form submitted
  const onContributorFinished = useCallback(() => {
    setCurrentStep(1)
  }, [setCurrentStep])

  // Callback of contribute form submitted
  const onDonationFinished = useCallback(
    async (values: any) => {
      setCurrentStep(2)
      setSubmittingStatus(SUBMITTING_STATUS.LOADING)

      let contributeRes: any

      try {
        // If contribute by Pago, then send the transaction to the blockchain
        if (contributeType === CONTRIBUTE_TYPE.PAGO) {
          contributeRes = await makeFundDonation({
            payId: values.payId,
            amount: values.amount / ALGO_USDC_SCALE,
            fundId: id,
            currency: 'USDC',
          })

          if (contributeRes?.message) {
            setSubmittingStatus(SUBMITTING_STATUS.FAILED)
            return setErrorText(contributeRes?.message || 'Unknown error')
          }
        }

        // The contributor info is always submitted
        const contributorInfo = contributorForm.getFieldsValue()
        const extraRequest: any = {}
        // FIXME: Follow amount and contributeRes.transactionId is fake so far.
        if (contributeType === CONTRIBUTE_TYPE.QR_CODE) extraRequest.amount = values.amount
        else if (contributeType === CONTRIBUTE_TYPE.PAGO)
          extraRequest.pagoTransactionId = contributeRes.id
        await submitContributor({
          name: contributorInfo.name || '',
          email: contributorInfo.email || '',
          country: contributorInfo.country || '',
          fundId: id,
          ...extraRequest,
        })

        setSubmittingStatus(SUBMITTING_STATUS.SUCCEED)
      } catch (err: unknown) {
        const error = err as ContributeError
        const { message, response } = error
        setSubmittingStatus(SUBMITTING_STATUS.FAILED)
        setErrorText(
          `${message}${response ? `, ${response.data.message}` : ''}.` || 'Unknown error'
        )
      }
    },
    [id, setSubmittingStatus, setCurrentStep, setErrorText, contributorForm, contributeType]
  )

  if (fundLoading) {
    return <Spin size='large' />
  }

  if (!fund) {
    return (
      <Result
        status='404'
        title='Fund is not found'
        extra={
          <Link to='/'>
            <Button type='primary' icon={<HomeOutlined />}>
              Back Home
            </Button>
          </Link>
        }
      />
    )
  }

  if (!fund.escrow?.address)
    return (
      <Result
        title='Fund is not ready for donations yet'
        subTitle='Please contact the fund owner to setup the blockchain address.'
        extra={
          <Link to={`/view/${id}`}>
            <Button type='primary'>Back to Fund</Button>
          </Link>
        }
      />
    )

  return (
    <div className='space-y-8'>
      <div className='flex max-md:flex-col items-center'>
        <div className='flex-1'>
          <div className='text-sm'>MAKE A CONTRIBUTION</div>
          <Breadcrumb className='text-xl'>
            <Breadcrumb.Item>
              <Link to='/'>
                <CreditCardOutlined style={{ fontSize: 20 }} /> Crowdfunds
              </Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <Link to={`/view/${id}`} data-testid='fundLink'>
                <FundOutlined style={{ fontSize: 20 }} /> {fund.name}
              </Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Contribute</Breadcrumb.Item>
          </Breadcrumb>
        </div>
      </div>

      <Steps current={currentStep} items={steps} type='navigation' />

      <div style={{ display: currentStep === 0 ? 'block' : 'none' }}>
        <Form
          form={contributorForm}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 12 }}
          colon={false}
          onFinish={onContributorFinished}
          onFinishFailed={(e) => console.warn('Fund Contribute Basic Info validate failed', e)}
        >
          <Card>
            <Form.Item label='Name' name='name' rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item label='Email' name='email' rules={[{ required: true }]}>
              <Input type='email' />
            </Form.Item>
            <Form.Item label='Country' name='country' rules={[{ required: true }]}>
              <Select
                showSearch
                filterOption={(input, option) => {
                  const lowerCaseInput = input.toLowerCase()
                  if (option.shortcut?.startsWith(lowerCaseInput)) return true
                  return option.label.toLowerCase().includes(lowerCaseInput)
                }}
                options={countryOptions}
              />
            </Form.Item>
            <Form.Item className='[&_label]:!hidden' label=' '>
              <Typography.Text type='secondary'>
                Note: This information will not be shared with third-parties. It is collected for
                the purposes of regulatory compliance.
              </Typography.Text>
            </Form.Item>
            <Form.Item className='[&_label]:!hidden' label=' '>
              <Form.Item
                noStyle
                name='agreement'
                valuePropName='checked'
                rules={[
                  {
                    required: true,
                    transform: (value) => (value ? String(value) : undefined),
                    message: 'Please check the box if you want to continue',
                  },
                ]}
              >
                <Switch />
              </Form.Item>
              <span className='ml-4'>
                I agree to the{' '}
                <NewTabLink href='https://www.onestepprojects.org/terms-and-conditions'>
                  Terms & Conditions
                </NewTabLink>{' '}
                and{' '}
                <NewTabLink href='https://www.onestepprojects.org/privacy-policy/'>
                  Privacy Policy
                </NewTabLink>
                .
              </span>
            </Form.Item>
            <Form.Item className='[&_label]:!hidden' label=' '>
              <Form.Item noStyle name='receiveUpdates' valuePropName='checked'>
                <Switch />
              </Form.Item>
              <span className='ml-4'>
                I would like to receive occasional updates about this crowdfunding campaign.
              </span>
            </Form.Item>
          </Card>
          <Form.Item wrapperCol={{ offset: 10, span: 18 }} style={{ marginTop: 8 }}>
            <Space>
              <Button type='primary' ghost disabled icon={<LeftOutlined />}>
                Back
              </Button>
              <Button type='primary' ghost htmlType='submit'>
                Next
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </div>

      <div style={{ display: currentStep === 1 ? 'block' : 'none' }}>
        <Form
          form={contributeForm}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          onFinish={onDonationFinished}
          onFinishFailed={(e) => console.warn('Fund Contribute Contribute validate failed', e)}
        >
          <div className='flex flex-col md:flex-row items-stretch gap-8'>
            <Card
              className={classNames(
                'flex-1',
                contributeType === CONTRIBUTE_TYPE.QR_CODE
                  ? 'outline outline-blue-500 shadow'
                  : 'opacity-60'
              )}
              title={
                <Radio
                  className='w-full'
                  checked={contributeType === CONTRIBUTE_TYPE.QR_CODE}
                  onClick={() => {
                    setContributeType(CONTRIBUTE_TYPE.QR_CODE)
                    resetFormErrors(contributeForm)
                  }}
                >
                  <Typography.Title level={5} style={{ marginBottom: 0 }}>
                    Contribute Using an Algorand-Compatible Exchange
                  </Typography.Title>
                </Radio>
              }
            >
              <Typography>
                Use any Algorand-compatible exchange to scan the QR code below to contribute{' '}
                <Typography.Text strong>USDC</Typography.Text> to this fund.
                <br />
                <br />
                If you need help, please refer to the{' '}
                <NewTabLink href='https://www.onestepprojects.org/algorand-exchange-user-guide'>
                  Algorand Exchange User Guide.
                </NewTabLink>
              </Typography>

              <QRCode
                color={contributeType === CONTRIBUTE_TYPE.QR_CODE ? '#60b358' : '#ccc'}
                icon={contributeType === CONTRIBUTE_TYPE.QR_CODE && '/assets/favicon.png'}
                value={fund.escrow.address}
              />
              <Typography.Text className='text-xs' copyable={true} data-testid='fundAddress'>
                {fund.escrow.address}
              </Typography.Text>
              <Form.Item
                labelAlign='left'
                label='Amount Contributed'
                name='amount'
                labelCol={{ span: 8 }}
                rules={[{ required: contributeType === CONTRIBUTE_TYPE.QR_CODE }]}
              >
                <InputNumber
                  className='w-full'
                  addonAfter={fund.currency}
                  disabled={contributeType !== CONTRIBUTE_TYPE.QR_CODE}
                />
              </Form.Item>
              <Form.Item wrapperCol={{ span: 24 }}>
                <Form.Item
                  noStyle
                  name='confirmed'
                  valuePropName='checked'
                  rules={[
                    {
                      required: contributeType === CONTRIBUTE_TYPE.QR_CODE,
                      transform: (value) => (value ? String(value) : undefined),
                      message: 'Please confirm the amount and check the box',
                    },
                  ]}
                >
                  <Switch disabled={contributeType !== CONTRIBUTE_TYPE.QR_CODE} />
                </Form.Item>
                <span className='ml-4'>
                  I confirm that I have contributed the amount noted above.
                </span>
              </Form.Item>
            </Card>
            <Card
              className={classNames(
                'flex-1',
                contributeType === CONTRIBUTE_TYPE.PAGO
                  ? 'outline outline-blue-500 shadow'
                  : 'opacity-60'
              )}
              title={
                <Radio
                  className='w-full'
                  checked={contributeType === CONTRIBUTE_TYPE.PAGO}
                  onClick={() => {
                    setContributeType(CONTRIBUTE_TYPE.PAGO)
                    resetFormErrors(contributeForm)
                  }}
                >
                  <Typography.Title level={5} style={{ marginBottom: 0 }}>
                    Contribute Using Your Pago Wallet
                  </Typography.Title>
                </Radio>
              }
            >
              <Typography>
                Download the Pago Wallet on the{' '}
                <NewTabLink href='https://apps.apple.com/ro/app/pago-wallet/id1581076779?platform=iphone'>
                  iOS App Store
                </NewTabLink>{' '}
                or{' '}
                <NewTabLink href='https://play.google.com/store/apps/details?id=com.pago.wallet.androidApp&hl=en&gl=US&pli=1'>
                  Google Play
                </NewTabLink>{' '}
                to contribute <Typography.Text strong>USDC</Typography.Text> to this fund.
                <br />
                <br />
                If you need help, please refer to the{' '}
                <NewTabLink href='https://www.onestepprojects.org/pago-wallet-user-guide'>
                  Pago Wallet User Guide
                </NewTabLink>
                .
              </Typography>
              <br />
              <Form.Item
                labelAlign='left'
                label='Pago Paystring'
                name='payId'
                rules={[{ required: contributeType === CONTRIBUTE_TYPE.PAGO }]}
              >
                <Input disabled={contributeType !== CONTRIBUTE_TYPE.PAGO} />
              </Form.Item>
              <Form.Item
                labelAlign='left'
                label='Amount'
                name='amount'
                rules={[{ required: contributeType === CONTRIBUTE_TYPE.PAGO }]}
              >
                <InputNumber
                  className='w-full'
                  addonAfter={fund.currency}
                  disabled={contributeType !== CONTRIBUTE_TYPE.PAGO}
                />
              </Form.Item>
              <Form.Item wrapperCol={{ span: 24 }}>
                <Form.Item
                  noStyle
                  name='contributionConfirmed'
                  valuePropName='checked'
                  rules={[
                    {
                      required: contributeType === CONTRIBUTE_TYPE.PAGO,
                      transform: (value) => (value ? String(value) : undefined),
                      message: 'Please confirm the contribution and check the box',
                    },
                  ]}
                >
                  <Switch disabled={contributeType !== CONTRIBUTE_TYPE.PAGO} />
                </Form.Item>
                <span className='ml-4'>
                  After I submit this form, I will use my Pago Wallet to Accept and Complete this
                  transaction.
                </span>
              </Form.Item>
            </Card>
          </div>
          <Space direction='vertical' className='m-8 flex justify-center gap-8'>
            <Typography.Title level={5} style={{ textAlign: 'center', marginBottom: 0 }}>
              Please double-check your exchange or wallet balance to confirm that your transaction
              was successfully submitted.
            </Typography.Title>
            <Space direction='horizontal' className='flex justify-center'>
              <Button
                type='primary'
                ghost
                icon={<LeftOutlined />}
                onClick={() => setCurrentStep(0)}
              >
                Back
              </Button>
              <Button type='primary' htmlType='submit'>
                Submit
              </Button>
            </Space>
          </Space>
        </Form>
      </div>

      <div style={{ display: currentStep === 2 ? 'block' : 'none' }}>
        <Card>
          <FundContributeResult
            fund={fund}
            submittingStatus={submittingStatus}
            errorText={errorText}
          />
        </Card>
        <div className='m-8 flex justify-center gap-8'>
          <Button type='primary' ghost icon={<LeftOutlined />} onClick={() => setCurrentStep(1)}>
            Back
          </Button>
          <Link to='/'>
            <Button type='primary' ghost>
              Return to All Funds
            </Button>
          </Link>
        </div>
      </div>
    </div>
  )
}
