import { CreditCardOutlined, LeftOutlined, SaveOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import { Breadcrumb, Button, DatePicker, Form, Input, InputNumber, Select } from 'antd'
import { useMemo, type FC } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { ALGO_USDC_SCALE } from '../constants'
import { createFund, getOrganizationsByPersonId, getSubProjectByOrganizationId } from '../services'
import type { Fund, Person } from '../types'
import { dayjs } from '../utils'

interface FundCreateProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const FundCreate: FC<FundCreateProps> = ({ auth }) => {
  const navigate = useNavigate()

  const [form] = Form.useForm<Fund>()

  const initialValues = useMemo<Partial<Fund>>(
    () => ({
      startDate: dayjs().format('YYYY-MM-DD'),
      endDate: dayjs().add(3, 'month').format('YYYY-MM-DD'),
    }),
    []
  )

  const { loading: organizationOptionsLoading, data: organizationOptions } = useRequest(
    async () => {
      const organizations = await getOrganizationsByPersonId(auth.person.uuid)
      const organizationOptions = organizations.map(({ id, name }) => ({ label: name, value: id }))
      return organizationOptions
    },
    {
      refreshDeps: [auth],
      ready: !!auth?.person?.uuid,
    }
  )

  const organizationId = Form.useWatch('organizationId', form)
  const { loading: projectOptionsLoading, data: projectOptions } = useRequest(
    async () => {
      form.setFieldValue('projectId', undefined)
      const projects = await getSubProjectByOrganizationId(organizationId)
      const projectOptions = projects.map(({ id, name }) => ({ label: name, value: id }))
      return projectOptions
    },
    {
      refreshDeps: [organizationId],
      ready: !!organizationId,
    }
  )

  const { loading: submitting, runAsync: submit } = useRequest(
    async (values: Fund) => {
      const res = await createFund({ ...values, currency: 'USDC' })
      if (res?.id) {
        navigate(`/view/${res.id}`)
      }
    },
    {
      manual: true,
    }
  )

  return (
    <div className='space-y-8'>
      <div>
        <div className='text-sm'>RELIEF FUNDS</div>
        <Breadcrumb className='text-xl'>
          <Breadcrumb.Item>
            <Link to='/'>
              <CreditCardOutlined style={{ fontSize: 20 }} /> Crowdfunds
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Create New Fund</Breadcrumb.Item>
        </Breadcrumb>
      </div>

      <div>
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16, offset: 6, pull: 5 }}
          colon={false}
          form={form}
          initialValues={initialValues}
          onFinish={(values) => submit(values)}
        >
          <Form.Item
            label='Fund Name'
            name='name'
            rules={[{ required: true, message: 'Name is required' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='Description'
            name='description'
            rules={[{ required: true, message: 'Description is required' }]}
          >
            <Input.TextArea autoSize />
          </Form.Item>
          <Form.Item
            label='Relief Organization'
            name='organizationId'
            rules={[{ required: true, message: 'Organization is required' }]}
          >
            <Select
              allowClear
              showSearch
              loading={organizationOptionsLoading}
              options={organizationOptions}
              onChange={() => form.setFieldValue('projectId', undefined)}
            />
          </Form.Item>
          <Form.Item label='Relief Project' name='projectId'>
            <Select
              allowClear
              showSearch
              loading={projectOptionsLoading}
              options={projectOptions}
            />
          </Form.Item>
          <Form.Item
            label='Start Date'
            name='startDate'
            rules={[{ required: true, message: 'Start Date is required' }]}
            getValueProps={(v) => ({ value: v ? dayjs(v, 'YYYY-MM-DD') : v })}
            getValueFromEvent={(e) => (e ? dayjs(e).format('YYYY-MM-DD') : '')}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item
            label='End Date'
            name='endDate'
            rules={[{ required: true, message: 'End Date is required' }]}
            getValueProps={(v) => ({ value: v ? dayjs(v, 'YYYY-MM-DD') : v })}
            getValueFromEvent={(e) => (e ? dayjs(e).format('YYYY-MM-DD') : '')}
          >
            <DatePicker />
          </Form.Item>
          <Form.Item
            label='Blockchain Address'
            name={['escrow', 'address']}
            rules={[{ required: true, message: 'Blockchain Address is required' }]}
          >
            <Input.TextArea autoSize />
          </Form.Item>
          <Form.Item
            label='Goal Amount'
            name='goalAmount'
            rules={[{ required: true, message: 'Goal Amount is required' }]}
            getValueProps={(v) => ({ value: Number.isFinite(v) ? v * ALGO_USDC_SCALE : v })}
            getValueFromEvent={(e) => (Number.isFinite(e) ? e / ALGO_USDC_SCALE : e)}
          >
            <InputNumber addonAfter='USDC' />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 6 }}>
            <div className='space-x-8'>
              <Button icon={<LeftOutlined />} onClick={() => navigate(-1)}>
                Go Back
              </Button>
              <Button type='primary' htmlType='submit' icon={<SaveOutlined />} loading={submitting}>
                Create Fund
              </Button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}
