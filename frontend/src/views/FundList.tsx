import {
  BankOutlined,
  CreditCardOutlined,
  ReloadOutlined,
  ShoppingOutlined,
} from '@ant-design/icons'
import { useRequest } from 'ahooks'
import { Badge, Breadcrumb, Button, Result, Spin, Tooltip } from 'antd'
import { FC } from 'react'
import { Link, useParams } from 'react-router-dom'
import { FundCard } from '../components'
import {
  getActiveFunds,
  getFunds,
  getMyFunds,
  getOrganizationById,
  getOrganizationRolesByPersonId,
  getProjectById,
  isOrganizationAdmin,
} from '../services'
import type { Person } from '../types'

interface FundListProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const FundList: FC<FundListProps> = ({ auth }) => {
  const params = useParams<'persionId' | 'organizationId' | 'projectId'>()

  const { loading: organizationLoading, data: organization } = useRequest(
    () => params.organizationId && getOrganizationById(params.organizationId),
    { refreshDeps: [params] }
  )
  const { loading: projectLoading, data: project } = useRequest(
    () => params.projectId && getProjectById(params.projectId),
    {
      refreshDeps: [params],
    }
  )

  const { data: personRoles } = useRequest(async () => {
    const personId = auth?.person?.uuid
    if (!personId) return
    return await getOrganizationRolesByPersonId(personId)
  })

  const {
    loading: fundsLoading,
    data: funds,
    error: fundsError,
    refreshAsync: refreshFunds,
  } = useRequest(
    async () => {
      const funds = await (params.persionId
        ? getMyFunds()
        : params.organizationId || params.projectId
        ? getFunds(params)
        : getActiveFunds())
      return funds
    },
    {
      refreshDeps: [params],
      retryCount: 2,
    }
  )

  const isAdmin = isOrganizationAdmin(personRoles)

  if (fundsError)
    return (
      <Result
        status='warning'
        title={fundsError.message}
        extra={
          <div className='space-x-8'>
            <Button type='primary' size='large' icon={<ReloadOutlined />} onClick={refreshFunds}>
              Reload Page
            </Button>
          </div>
        }
      />
    )

  return (
    <div className='space-y-8'>
      <div className='flex max-md:flex-col items-center'>
        <div className='flex-1'>
          <div className='text-sm'>RELIEF PROJECTS</div>
          <Breadcrumb className='text-xl'>
            <Breadcrumb.Item>
              <Link to='/'>
                <CreditCardOutlined style={{ fontSize: 20 }} /> Crowdfunds
              </Link>
            </Breadcrumb.Item>
            {organizationLoading || projectLoading ? (
              <Breadcrumb.Item>
                <Spin size='small' />
              </Breadcrumb.Item>
            ) : (
              <>
                <Breadcrumb.Item>
                  {organization && (
                    <Link to={`/view/organizations/${organization.id}/funds`}>
                      <BankOutlined style={{ fontSize: 20 }} /> {organization.name}
                    </Link>
                  )}
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  {organization && project && (
                    <Link
                      to={`/view/organizations/${organization.id}/projects/${project.id}/funds`}
                    >
                      <ShoppingOutlined style={{ fontSize: 20 }} /> {project.name}
                    </Link>
                  )}
                </Breadcrumb.Item>
                {params.persionId ? (
                  <Breadcrumb.Item>My Funds</Breadcrumb.Item>
                ) : (
                  <Breadcrumb.Item>All Funds</Breadcrumb.Item>
                )}
              </>
            )}
          </Breadcrumb>
        </div>

        {isAdmin && (
          <div className='flex gap-4'>
            <div>
              <Link to={params.persionId ? '/' : `/view/persions/${auth?.person?.uuid}/funds`}>
                <Button>{params.persionId ? 'Back to Main Page' : 'View My Funds'}</Button>
              </Link>
            </div>
            <div>
              <Link to='/create'>
                <Button type='primary'>Create New Fund</Button>
              </Link>
            </div>
          </div>
        )}
      </div>

      <Spin spinning={fundsLoading}>
        <div className='flex flex-col gap-8'>
          {funds?.map((fund) => {
            let card = <FundCard fund={fund} />
            if (isAdmin) {
              if (params.persionId) {
                card = (
                  <Badge.Ribbon
                    text={fund.fundVisibility === 'PUBLISHED' ? 'Published' : 'Unpublish'}
                    color={fund.fundVisibility === 'PUBLISHED' ? 'blue' : 'lightgray'}
                  >
                    <FundCard fund={fund} />
                  </Badge.Ribbon>
                )
              } else if (fund.creatorId === auth?.person?.uuid) {
                card = (
                  <Badge.Ribbon text='My Fund' color='gold'>
                    <FundCard fund={fund} />
                  </Badge.Ribbon>
                )
              }
            }
            return (
              <Tooltip key={fund.id} title='Show Fund Details'>
                <Link to={`/view/${fund.id}`}>{card}</Link>
              </Tooltip>
            )
          })}
        </div>
      </Spin>
    </div>
  )
}
