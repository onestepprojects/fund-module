import '@testing-library/jest-dom'
import { fireEvent, render, waitFor } from '@testing-library/react'
import { createServer } from 'miragejs'
import { BrowserRouter } from 'react-router-dom'
import { FundView } from '../FundView'
import * as mockData from './mockData'

// The mock API server
let server: ReturnType<typeof createServer>

beforeAll(() => {
  server = createServer({
    environment: 'test',
    routes: function () {
      this.get(
        `${process.env.REACT_APP_BLOCKCHAIN_API_URI}/account/:accountEscrowAddress/transaction-contributions`,
        () => mockData.transactionContributions
      )
      this.get(`${process.env.REACT_APP_FUND_MODULE_API_URL}/fund/:fundId`, () => mockData.fund)
      this.get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/organizations/get-org-roles-by-person/:personUUID`,
        () => []
      )
      this.get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/organizations/:organizationId`,
        () => mockData.organization
      )
      this.get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/projects/:projectId`,
        () => mockData.project
      )
    },
  })
})

afterAll(() => {
  server.shutdown()
})

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest.fn().mockReturnValue({ id: '1' }),
}))

describe('FundList', () => {
  it('render loading', async () => {
    const { asFragment } = render(
      <BrowserRouter>
        <FundView auth={mockData.authHelper} />
      </BrowserRouter>
    )
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders correctly', async () => {
    const { asFragment, container, getByText } = render(
      <BrowserRouter>
        <FundView auth={mockData.authHelper} />
      </BrowserRouter>
    )

    // Render as expected
    await waitFor(() => getByText(mockData.organization.name))
    expect(asFragment()).toMatchSnapshot()

    // Toggle the show all status button
    const showAllStatusButton = container.querySelector('button#showAllStatusButton')
    expect(showAllStatusButton.innerHTML).toBe('<span>Show All</span>')
    fireEvent.click(showAllStatusButton)
    await waitFor(() => expect(showAllStatusButton.innerHTML).toBe('<span>Show Latest</span>'))
  })
})
