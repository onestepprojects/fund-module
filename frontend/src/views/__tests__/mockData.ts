/** The data was dump from staging environment */

import type { Person } from '../../types'

const fundId = '1'
const organizationId = '3e6c0943-a859-42ad-b034-1eefda3dd8c9'
const escrowAddress = 'TC3LTKPN3RCXQLXGPC4MLDU3X5C7BWWEHMGIY4W5OS6DVFKJGSLSSH424E'

export const fund = {
  id: fundId,
  name: 'Water Crisis Response for Climate Refugees in Bunambutye, Uganda',
  escrow: {
    address: escrowAddress,
  },
  description:
    'Despite relocating 303 households from 2018-2022, the Ugandan government has failed to allocate further development funds for Bunambutye, leaving the community without access to safe water. With no national or international NGOs responding to the pressing needs raised by the local government of Bulambuli District, your support is crucial in ensuring the survival and well-being of the climate refugees facing a water crisis in Uganda.',
  creatorId: '64c65b19-fedc-48a8-84f2-fa26bc324b4b',
  projectId: '4eea4966-860b-450f-8326-bee44d167927',
  organizationId,
  startDate: '2023-02-12',
  endDate: '2024-01-31',
  goalAmount: 1.0e12,
  currency: 'USDC',
  fundVisibility: 'PUBLISHED',
  imageUrl: 'https://fundresource.s3.amazonaws.com/adb8df22-3249-496f-8553-b6059db45675_cover.png',
  videoUrl: null,
  bannerImageUrl:
    'https://fundresource.s3.amazonaws.com/adb8df22-3249-496f-8553-b6059db45675_banner.png',
  bodyLeftImageUrl: null,
  bodyLeftVideoUrl: 'https://www.youtube.com/watch?v=eGoN6T75QAY',
  bodyRightImageUrl: null,
  bodyRightVideoUrl: 'https://www.youtube.com/watch?v=1SDbPOurX4U',
  reportLeftImageUrl: null,
  reportMiddleImageUrl: null,
  reportRightImageUrl: null,
  totalAmountRaised: 0.0,
  fundState: 'IN_PROGRESS',
}

export const transactionContributions = [
  {
    assetId: 31566704,
    assetName: 'USDC',
    decimals: 6,
    transactionCount: 6,
    transactionTotalAmount: 19000005,
    transactionDisplayAmount: 19,
  },
]

export const organization = {
  id: '3e6c0943-a859-42ad-b034-1eefda3dd8c9',
  name: 'MPowa',
  type: 'HYBRID',
  tagline: 'Democratizing the clean-tech revolution',
  logo: {
    name: '1676161915372Mpowa-logo.png',
    bucketName: 'onestep-org-pics',
    url: 'https://onestep-org-pics.s3.amazonaws.com/1676161915372Mpowa-logo.png',
    caption: '',
    format: 'PNG',
    size: 38084,
    creationDate: null,
    creator: null,
    body: null,
  },
  email: 'ryan@mpowa.io',
  phones: {
    phone: '447495326404',
  },
  website: 'https://mpowa.io/',
  address: {
    address: '71- 75 Shelton Street',
    city: 'London',
    district: null,
    state: null,
    postalCode: 'WC2H 9JQ',
    country: 'United Kingdom',
  },
  creatorUUID: '64c65b19-fedc-48a8-84f2-fa26bc324b4b',
  statusLabel: 'ACTIVE',
  statusLastActive: null,
  description: {
    description:
      'MPowa is a new impact enterprise creating new opportunities for growth by blending clean technology, blockchain, and impact investing. MPowa supports clean-tech energy, food, and water projects as well as disaster relief initiatives to support some of the most vulnerable populations in the world.',
    serviceArea: [
      {
        latitude: 51.514816,
        longitude: -0.1235229,
      },
    ],
    projectTypes: ['appear', 'religious'],
  },
  requesterRoles: [],
  socialMedia: {
    youtube: 'https://www.youtube.com/channel/UC5Uoh5W27YN7fo7le_NOExw',
    twitter: 'https://www.twitter.com/decarbonizerapp',
    facebook: 'https://www.facebook.com/decarbonizerapp',
    linkedin: 'https://www.linkedin.com/company/mpowa-io',
  },
  paymentAccount: {
    payID: null,
    blockchainAddress: null,
    status: null,
    assets: {},
  },
  supplier: false,
}

export const project = {
  parentType: null,
  parentId: null,
  id: '4eea4966-860b-450f-8326-bee44d167927',
  name: 'Uganda',
  description:
    'Despite relocating 303 households from 2018-2022, the Ugandan government has failed to allocate further development funds for Bunambutye, leaving the community without access to safe water. With no national or international NGOs responding to the pressing needs raised by the local government of Bulambuli District, your support is crucial in ensuring the survival and well-being of the climate refugees facing a water crisis in Uganda.',
  targetArea: {},
  primaryType: 'CLEAN_WATER_AND_SANITATION',
  secondaryType: 'HEALTH_AND_WELL_BEING',
  tagline: 'Led by Abel Odeke & Otim Akonopeesa Levi',
  profilePicture: {
    name: null,
    bucketName: null,
    url: 'https://onestep-project-pics.s3.amazonaws.com/defaultproject.svg',
    caption: null,
    format: null,
    size: 0,
    creationDate: '1676234022927',
    creator: null,
    body: null,
  },
  creationDate: '1676234022835',
  startDate: '1675283609625',
  closeDate: '1706733209625',
  currentStatusUpdate: {
    authorID: '64c65b19-fedc-48a8-84f2-fa26bc324b4b',
    date: '1676234022835',
    title: 'My first update',
    description: 'The first update for project Uganda',
  },
  statusUpdatesHistory: Array.from({ length: 4 }, (_, i) => ({
    date: new Date(`2023-05-09 20:0${i}:00`).getTime(),
    title: `Test status update ${i}`,
    description: `Details of status update ${i}`,
  })),
  partnerOrganizations: {},
  urgencyLevel: 'HIGH',
  projectStage: 'IMPLEMENTATION',
  requesterRoles: [],
  socialMedia: null,
  health: false,
}

export const authHelper = {
  token: '1234567',
  person: {
    uuid: '1234-5678',
  } as Person,
  roles: [],
}
