import '@testing-library/jest-dom'
import { render } from '@testing-library/react'
import { createServer } from 'miragejs'
import { BrowserRouter } from 'react-router-dom'
import { FundCreate } from '../FundCreate'
import * as mockData from './mockData'

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import MockDate from 'mockdate'

dayjs.extend(utc)

// The mock API server
let server: ReturnType<typeof createServer>

beforeAll(() => {
  server = createServer({
    environment: 'test',
    routes: function () {
      this.get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/organizations/get-org-roles-by-person/:personUUID`,
        () => [
          {
            id: '1',
            name: 'org1',
          },
        ]
      )
    },
  })

  MockDate.set('2023-05-10')
})

afterAll(() => {
  server.shutdown()
  MockDate.reset()
})

describe('FundCreate', () => {
  it('renders correctly', async () => {
    const { asFragment } = render(
      <BrowserRouter>
        <FundCreate auth={mockData.authHelper} />
      </BrowserRouter>
    )
    expect(asFragment()).toMatchSnapshot()
  })
})
