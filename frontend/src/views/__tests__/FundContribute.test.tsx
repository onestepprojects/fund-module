import '@testing-library/jest-dom'
import { fireEvent, render, waitFor } from '@testing-library/react'
import { createServer } from 'miragejs'
import { BrowserRouter } from 'react-router-dom'
import * as services from '../../services/fundApi'
import type { Fund } from '../../types'
import { FundContribute } from '../FundContribute'

// Mock the customized component Ant Design's Select to normal element
// Optimized from https://stackoverflow.com/a/71576445/439964
jest.mock('antd', () => {
  const antd = jest.requireActual('antd')

  const Select = ({
    onChange,
    id,
    options,
  }: {
    onChange: (value: string) => void
    id: string
    options: { value: string; label: string }[]
  }) => (
    <select id={id} role='combobox' onChange={(e) => onChange(e.target.value)}>
      {options.map((option) => (
        <Select.Option key={option.value} value={option.value}>
          {option.label}
        </Select.Option>
      ))}
    </select>
  )

  Select.Option = ({
    children,
    value,
    ...otherProps
  }: {
    children: React.ReactNode
    value: string
    otherProps?: string[]
  }) => (
    <option role='option' value={value} {...otherProps}>
      {children}
    </option>
  )

  return {
    ...antd,
    Select,
  }
})

// The mock API server
let server: ReturnType<typeof createServer>

// Abstract the steps
const steps = {
  async fillInfoForm() {
    server.get('fund/1', () => mockFund)

    const { getByText, getByTestId, container } = render(
      <BrowserRouter>
        <FundContribute />
      </BrowserRouter>
    )

    await waitFor(() => getByText('MAKE A CONTRIBUTION'))

    // Fill the form
    fireEvent.change(container.querySelector('#name'), {
      target: {
        value: 'Test',
      },
    })
    fireEvent.change(container.querySelector('#email'), {
      target: {
        value: 'test@onestepprojects.org',
      },
    })

    await waitFor(() => getByText('United States of America'))

    fireEvent.select(container.querySelector('#country'), {
      target: {
        value: 'United States of America',
      },
    })

    // Agreement
    fireEvent.click(container.querySelector('#agreement'))

    // Submit
    fireEvent.click(container.querySelector('button[type="submit"]'))

    await waitFor(() =>
      getByText(
        'Please double-check your exchange or wallet balance to confirm that your transaction was successfully submitted.'
      )
    )

    return {
      getByText,
      getByTestId,
      container,
    }
  },
}

beforeEach(() => {
  server = createServer({
    environment: 'test',
    urlPrefix: process.env.REACT_APP_FUND_MODULE_API_URL,
  })
})
afterEach(() => server.shutdown())

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest.fn().mockReturnValue({ id: '1' }),
}))

const mockFund = {
  escrow: {
    name: 'Test fund',
    address: 'Test address',
    currency: 'USDT',
  },
} as unknown as Fund

jest.spyOn(services, 'makeFundDonation')
jest.spyOn(services, 'submitContributor')

describe('FundContribute', () => {
  it('render loading', async () => {
    server.get('fund/1', () => null)
    const { asFragment } = render(
      <BrowserRouter>
        <FundContribute />
      </BrowserRouter>
    )
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders correctly', async () => {
    server.get('fund/1', () => mockFund)

    const { asFragment, getByText, getByTestId } = render(
      <BrowserRouter>
        <FundContribute />
      </BrowserRouter>
    )

    await waitFor(() => getByText('MAKE A CONTRIBUTION'))

    expect(getByTestId('fundAddress')).toHaveTextContent(mockFund.escrow.address)

    const fundLink = getByTestId('fundLink')
    expect(fundLink).toHaveAttribute('href', '/view/1')

    expect(asFragment()).toMatchSnapshot()
  })

  it('renders 404 page when fund not found', async () => {
    server.get('fund/1', () => null)

    const { asFragment, getByText } = render(
      <BrowserRouter>
        <FundContribute />
      </BrowserRouter>
    )
    await waitFor(() => getByText('Fund is not found'))
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders fund is not ready when address is not set', async () => {
    server.get('fund/1', () => ({}))

    const { asFragment, getByText } = render(
      <BrowserRouter>
        <FundContribute />
      </BrowserRouter>
    )
    await waitFor(() => getByText('Fund is not ready for donations yet'))
    expect(asFragment()).toMatchSnapshot()
  })

  it('Contribute with Algorand-Compatible Exchange should be OK', async () => {
    server.post('/donations', () => null)
    server.post('/donation', () => ({
      id: 12345,
    }))

    const { container, getByText } = await steps.fillInfoForm()

    // First radio should be checked
    const antRadios = container.querySelectorAll('.ant-card-head-title .ant-radio')
    expect(antRadios.item(0).className.includes('ant-radio-checked')).toBeTruthy()
    expect(antRadios.item(1).className.includes('ant-radio-checked')).toBeFalsy()

    // Second tab should be disabled
    // @ts-expect-error disabled doesn't defined in the HTMLElement type declaration but exist in MDN
    expect(container.querySelector<HTMLElement>('#payId').disabled).toBeTruthy()

    const amountInputs = container.querySelectorAll('#amount')
    fireEvent.change(amountInputs.item(0), {
      target: {
        value: '123',
      },
    })

    // Typing into the first amount input should affect the second one
    expect(amountInputs.item(1)).toHaveValue('123')

    fireEvent.click(container.querySelector('#confirmed'))
    fireEvent.click(container.querySelectorAll('.ant-btn[type=submit]').item(1))

    await waitFor(() => getByText('Thank you for your contribution!'))
  })

  it('Contribute with Pago Wallet should be OK', async () => {
    server.post('/donations', () => null)
    server.post('/donation', () => ({
      id: 12345,
    }))

    const { container, getByText } = await steps.fillInfoForm()

    fireEvent.click(container.querySelectorAll('.ant-card-head-title .ant-radio').item(1))
    const antRadios = container.querySelectorAll('.ant-card-head-title .ant-radio')
    expect(antRadios.item(0).className.includes('ant-radio-checked')).toBeFalsy()
    expect(antRadios.item(1).className.includes('ant-radio-checked')).toBeTruthy()

    const amountInputs = container.querySelectorAll('#amount')
    fireEvent.change(amountInputs.item(1), {
      target: {
        value: '234',
      },
    })
    // Typing into the first amount input should affect the second one
    expect(amountInputs.item(0)).toHaveValue('234')
    fireEvent.change(container.querySelector('#payId'), {
      target: {
        value: '1234567890',
      },
    })

    fireEvent.click(container.querySelector('#contributionConfirmed'))
    fireEvent.click(container.querySelectorAll('.ant-btn[type=submit]').item(1))

    await waitFor(() => getByText('Thank you for your contribution!'))
  })

  it('Contribute with Pago Wallet but got server error', async () => {
    server.post('/donations', () => null)
    server.post('/donation', () => ({
      message: 'Failed to make donation - 500 - Send USDC request failed',
    }))
    const { container, getByText } = await steps.fillInfoForm()

    fireEvent.click(container.querySelectorAll('.ant-card-head-title .ant-radio').item(1))
    const antRadios = container.querySelectorAll('.ant-card-head-title .ant-radio')
    expect(antRadios.item(0).className.includes('ant-radio-checked')).toBeFalsy()
    expect(antRadios.item(1).className.includes('ant-radio-checked')).toBeTruthy()

    const amountInputs = container.querySelectorAll('#amount')
    fireEvent.change(amountInputs.item(1), {
      target: {
        value: '234',
      },
    })
    // Typing into the first amount input should affect the second one
    expect(amountInputs.item(0)).toHaveValue('234')
    fireEvent.change(container.querySelector('#payId'), {
      target: {
        value: '1234567890',
      },
    })

    fireEvent.click(container.querySelector('#contributionConfirmed'))
    fireEvent.click(container.querySelectorAll('.ant-btn[type=submit]').item(1))

    await waitFor(() => getByText('Service is unavailable right now, please try again later.'))
    expect(container.querySelector<HTMLElement>('.ant-result-subtitle').innerHTML).toBe(
      'Failed to make donation - 500 - Send USDC request failed'
    )
  })

  it('Contribute with Pago Wallet but got code error', async () => {
    jest.spyOn(services, 'makeFundDonation').mockRejectedValue(new Error('Some error'))

    server.post('/donation', () => ({
      message: 'Failed to make donation - 500 - Send USDC request failed',
    }))
    const { container, getByText } = await steps.fillInfoForm()

    fireEvent.click(container.querySelectorAll('.ant-card-head-title .ant-radio').item(1))
    const antRadios = container.querySelectorAll('.ant-card-head-title .ant-radio')
    expect(antRadios.item(0).className.includes('ant-radio-checked')).toBeFalsy()
    expect(antRadios.item(1).className.includes('ant-radio-checked')).toBeTruthy()

    const amountInputs = container.querySelectorAll('#amount')
    fireEvent.change(amountInputs.item(1), {
      target: {
        value: '234',
      },
    })
    // Typing into the first amount input should affect the second one
    expect(amountInputs.item(0)).toHaveValue('234')
    fireEvent.change(container.querySelector('#payId'), {
      target: {
        value: '1234567890',
      },
    })

    fireEvent.click(container.querySelector('#contributionConfirmed'))
    fireEvent.click(container.querySelectorAll('.ant-btn[type=submit]').item(1))

    await waitFor(() => getByText('Service is unavailable right now, please try again later.'))
    expect(container.querySelector<HTMLElement>('.ant-result-subtitle').innerHTML).toBe(
      'Some error.'
    )
  })
})
