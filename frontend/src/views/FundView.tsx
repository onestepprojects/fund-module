import {
  BankOutlined,
  CreditCardOutlined,
  EditOutlined,
  FilterOutlined,
  FundOutlined,
  ShoppingOutlined,
} from '@ant-design/icons'
import { useRequest } from 'ahooks'
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Descriptions,
  Empty,
  FloatButton,
  Image,
  Progress,
  Row,
  Spin,
  Timeline,
} from 'antd'
import numeral from 'numeral'
import { useState, type FC } from 'react'
import ReactPlayer from 'react-player'
import { Link, useParams } from 'react-router-dom'

import { ShareButtons } from '../components/common/ShareButtons'
import { ALGO_USDC_SCALE } from '../constants'
import {
  getFundById,
  getOrganizationById,
  getOrganizationRolesByPersonId,
  getProjectById,
  isOrganizationAdmin,
  useFundTotalAmountRaised,
} from '../services'
import type { Person } from '../types'
import { dayjs } from '../utils'

interface FundViewProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const FundView: FC<FundViewProps> = ({ auth }) => {
  const params = useParams()

  const { loading: fundLoading, data: fund } = useRequest(() => getFundById(params.id), {
    refreshDeps: [params],
  })

  const totalAmountRaised = useFundTotalAmountRaised(fund)

  const fundTotalAmountRaisedPercent = Math.floor(
    (totalAmountRaised * 100) / (fund?.goalAmount || 0)
  )

  const { loading: organizationLoading, data: organization } = useRequest(
    () => getOrganizationById(fund.organizationId),
    {
      refreshDeps: [fund],
      ready: !!fund?.organizationId,
    }
  )
  const { loading: projectLoading, data: project } = useRequest(
    () => getProjectById(fund.projectId),
    {
      refreshDeps: [fund?.projectId],
      ready: !!fund?.projectId,
    }
  )

  // Set the status updates to show all or only 3 items
  const [showAllStatus, setShowAllStatus] = useState(false)

  const statusUpdates = project?.statusUpdatesHistory ?? []

  const { data: personRoles } = useRequest(
    () => auth?.person?.uuid && getOrganizationRolesByPersonId(auth?.person?.uuid),
    { refreshDeps: [auth] }
  )

  const isAdmin = isOrganizationAdmin(personRoles, fund?.organizationId)

  const organizationFundsLink = organization && `/view/organizations/${organization.id}/funds`
  const projectFundsLink =
    organization && project && `/view/organizations/${organization.id}/projects/${project.id}/funds`

  // discussion board logic - upon access, attempts to get the board, and creates on the fly if doesn't exist, then links
  // const handleDiscussions = () => {
  //   console.log('Discussion board clicked')

  //   getDiscussion(this.props.authHelper, this.state.id).then((res) => {
  //     console.log('Attempting to retrieve discussion for ID: ' + this.state.id)

  //     // if discussion board doesn't exist, create, and then link upon completion
  //     if (!res.data?.nodes.elements.length) {
  //       console.log('Not found. Attempting to create discussion for ID: ' + this.state.id)

  //       createDiscussion(this.props.authHelper, this.state.id, this.state.name).then((value) => {
  //         console.log('Success. Publishing with ID: ' + this.state.id + 'and uuid: ' + value.uuid)
  //         publishNode(this.props.authHelper, value.uuid, value.language).then((res) => {
  //           this.props.history.push(`/discussions/${this.state.id}`)
  //         })
  //       })
  //     } else {
  //       this.props.history.push(`/discussions/${this.state.id}`)
  //     }
  //   })
  // }

  return (
    <div className='space-y-8'>
      <div className='flex max-md:flex-col items-center'>
        <div className='flex-1'>
          <div className='text-sm'>FUND DETAILS</div>
          <Breadcrumb className='text-xl'>
            <Breadcrumb.Item>
              <Link to='/'>
                <CreditCardOutlined style={{ fontSize: 20 }} /> Crowdfunds
              </Link>
            </Breadcrumb.Item>
            {organizationLoading || projectLoading ? (
              <Breadcrumb.Item>
                <Spin size='small' />
              </Breadcrumb.Item>
            ) : (
              <>
                {organizationFundsLink && (
                  <Breadcrumb.Item>
                    <Link to={organizationFundsLink}>
                      <BankOutlined style={{ fontSize: 20 }} /> {organization?.name}
                    </Link>
                  </Breadcrumb.Item>
                )}
                {projectFundsLink && (
                  <Breadcrumb.Item>
                    <Link to={projectFundsLink}>
                      <ShoppingOutlined style={{ fontSize: 20 }} /> {project?.name}
                    </Link>
                  </Breadcrumb.Item>
                )}
                <Breadcrumb.Item>
                  <FundOutlined style={{ fontSize: 20 }} /> {fund?.name}
                </Breadcrumb.Item>
              </>
            )}
          </Breadcrumb>
        </div>
        <ShareButtons fund={fund} />
      </div>

      <div className='flex flex-col gap-4'>
        <div className='flex flex-col md:flex-row gap-4'>
          <div className='basis-7/12 space-y-4'>
            <Card
              className='h-min'
              loading={fundLoading}
              cover={
                fund ? (
                  fund.videoUrl ? (
                    <ReactPlayer width='100%' controls url={fund.videoUrl} />
                  ) : (
                    <Image
                      preview={{ bodyStyle: { margin: 'auto', maxWidth: '90vw' } }}
                      src={fund.imageUrl}
                    />
                  )
                ) : null
              }
              actions={
                isAdmin && [
                  <Link key='manage' to={`/manage/${fund?.id}`}>
                    <Button type='primary' icon={<EditOutlined />}>
                      Manage Fund
                    </Button>
                  </Link>,
                  // <Link key='discussion' to={`/manage/${fund?.id}`}>
                  // <Button icon={<CommentOutlined />}>Discussions</Button>,
                  // </Link>
                ]
              }
            >
              <Card.Meta title={fund?.name} description={fund?.description} />
            </Card>

            <Card title='Status Updates' loading={projectLoading}>
              {statusUpdates.length ? (
                <Timeline>
                  {statusUpdates
                    .slice(0, showAllStatus ? statusUpdates.length : 3)
                    .map(({ date, title, description }, index) => (
                      <Timeline.Item key={index} color={!index ? 'green' : 'blue'}>
                        <time className='mr-2 text-slate-500'>
                          {dayjs(Number(date)).format('YYYY-MM-DD hh:mm')}
                        </time>
                        <div className='font-medium'>{title}</div>
                        <div className='text-slate-500'>{description}</div>
                      </Timeline.Item>
                    ))}
                </Timeline>
              ) : (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='No status' />
              )}
              {statusUpdates.length > 3 ? (
                <Row justify='end'>
                  <Col>
                    <Button
                      id='showAllStatusButton'
                      onClick={() => setShowAllStatus(!showAllStatus)}
                    >
                      {showAllStatus ? 'Show Latest' : 'Show All'}
                    </Button>
                  </Col>
                </Row>
              ) : null}
            </Card>
          </div>

          <div className='basis-5/12 space-y-4'>
            {fund?.bannerImageUrl && <Image preview={false} src={fund.bannerImageUrl} />}

            <Card loading={fundLoading}>
              <div className='space-y-4'>
                <Link to={`/contribute/${fund?.id}`}>
                  <Button block type='primary' size='large'>
                    Contribute
                  </Button>
                </Link>
                <Card.Meta
                  title={`${numeral(totalAmountRaised * ALGO_USDC_SCALE).format('0,0.00')} ${
                    fund?.currency
                  } Raised`}
                />
                <Descriptions column={1} size='small' labelStyle={{ width: 128 }}>
                  <Descriptions.Item label='Start Date'>
                    {dayjs(fund?.startDate, 'YYYY-MM-DD').format('L')}
                  </Descriptions.Item>
                  <Descriptions.Item label='End Date'>
                    {dayjs(fund?.endDate, 'YYYY-MM-DD').format('L')}
                  </Descriptions.Item>
                  <Descriptions.Item label='Goal Amount'>
                    {`${numeral((fund?.goalAmount || 0) * ALGO_USDC_SCALE).format('0,0.00')} ${
                      fund?.currency
                    }`}
                  </Descriptions.Item>
                </Descriptions>
                <div>{`${fundTotalAmountRaisedPercent} % Raised`}</div>
                <Progress percent={fundTotalAmountRaisedPercent} />
              </div>
            </Card>

            <Card
              loading={fundLoading || organizationLoading}
              title='Organization'
              actions={
                organizationFundsLink && [
                  <Link to={organizationFundsLink}>
                    <Button icon={<FilterOutlined />}>View Organization Funds</Button>
                  </Link>,
                ]
              }
            >
              <Descriptions column={1} size='small' labelStyle={{ width: 128 }}>
                <Descriptions.Item label='Name'>{organization?.name}</Descriptions.Item>
                <Descriptions.Item label='Tagline'>{organization?.tagline}</Descriptions.Item>
              </Descriptions>
            </Card>

            {project ? (
              <Card
                loading={fundLoading || projectLoading}
                title='Project'
                actions={
                  projectFundsLink && [
                    <Link to={projectFundsLink}>
                      <Button icon={<FilterOutlined />}>View Project Funds</Button>
                    </Link>,
                  ]
                }
              >
                <Descriptions column={1} size='small' labelStyle={{ width: 128 }}>
                  <Descriptions.Item label='Name'>{project?.name}</Descriptions.Item>
                </Descriptions>
              </Card>
            ) : null}
          </div>
        </div>

        <div className='flex flex-col md:flex-row gap-4'>
          {fund?.bodyLeftVideoUrl ? (
            <ReactPlayer width='100%' controls url={fund.bodyLeftVideoUrl} />
          ) : (
            <Image preview={false} src={fund?.bodyLeftImageUrl} />
          )}
          {fund?.bodyRightVideoUrl ? (
            <ReactPlayer width='100%' controls url={fund.bodyRightVideoUrl} />
          ) : (
            <Image preview={false} src={fund?.bodyRightImageUrl} />
          )}
        </div>

        <div className='flex flex-col md:flex-row gap-4'>
          <Image preview={false} src={fund?.reportLeftImageUrl} />
          <Image preview={false} src={fund?.reportMiddleImageUrl} />
          <Image preview={false} src={fund?.reportRightImageUrl} />
        </div>
      </div>

      <Link to={`/contribute/${fund?.id}`}>
        <FloatButton
          className='md:hidden w-24'
          type='primary'
          shape='square'
          description='Contribute'
          onClick={() => {}}
        />
      </Link>
    </div>
  )
}
