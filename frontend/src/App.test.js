import ReactDOM from 'react-dom'
import FundModule from './App'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FundModule />, div)
  ReactDOM.unmountComponentAtNode(div)
})
