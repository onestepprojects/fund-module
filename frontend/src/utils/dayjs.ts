import _dayjs from 'dayjs'
import localeData from 'dayjs/plugin/localeData'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import weekday from 'dayjs/plugin/weekday'

_dayjs.extend(localeData)
_dayjs.extend(localizedFormat)
_dayjs.extend(weekday)

export const dayjs = _dayjs
