export async function readFileDataURL(file: File): Promise<string> {
  return new Promise<string>((resolve) => {
    const reader = new FileReader()
    reader.addEventListener(
      'load',
      function () {
        resolve(this.result as string)
      },
      false
    )
    reader.readAsDataURL(file)
  })
}

export async function readFileBase64(file: File): Promise<string> {
  const dataUrl = await readFileDataURL(file)
  return dataUrl.split(',').pop()
}
