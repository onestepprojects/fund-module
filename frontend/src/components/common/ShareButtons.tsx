import { FC } from 'react'
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from 'react-share'
import type { Fund } from '../../types'

interface ShareButtonsProps {
  fund: Fund
}

export const ShareButtons: FC<ShareButtonsProps> = ({ fund }) => {
  const shareUrl = window.location.href
  const shareTitle = `I just contributed to the "${fund?.name}" hosted on @OneStepProjects. Please join me in supporting vulnerable communities using a transparent, #blockchain powered nonprofit #disasterrelief software platform.`
  const shareButtonSize = 32

  return (
    <div>
      <FacebookShareButton url={shareUrl} quote={shareTitle}>
        <FacebookIcon size={shareButtonSize} />
      </FacebookShareButton>
      <TwitterShareButton url={shareUrl} title={shareTitle}>
        <TwitterIcon size={shareButtonSize} />
      </TwitterShareButton>
      <LinkedinShareButton url={shareUrl} title={shareTitle}>
        <LinkedinIcon size={shareButtonSize} />
      </LinkedinShareButton>
      <WhatsappShareButton url={shareUrl} title={shareTitle}>
        <WhatsappIcon size={shareButtonSize} />
      </WhatsappShareButton>
      <EmailShareButton
        url={shareUrl}
        subject={`[OneStep] Support ${fund?.name} on One Step Projects`}
        body={shareTitle}
      >
        <EmailIcon size={shareButtonSize} />
      </EmailShareButton>
    </div>
  )
}
