import { render } from '@testing-library/react'
import type { Fund } from '../../../types'
import { ShareButtons } from '../ShareButtons'

describe('ShareButtons', () => {
  it('render correctly', () => {
    const fund = {
      name: 'Test Fund',
    } as unknown as Fund
    const { asFragment } = render(<ShareButtons fund={fund} />)
    expect(asFragment()).toMatchSnapshot()
  })

  // TODO: User interaction tests
})
