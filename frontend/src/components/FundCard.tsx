import { useResponsive } from 'ahooks'
import { Card, Progress, Typography } from 'antd'
import numeral from 'numeral'
import { type FC } from 'react'
import { ALGO_USDC_SCALE } from '../constants'
import { useFundTotalAmountRaised } from '../services'
import type { Fund } from '../types'
import './FundCard.pcss'

interface FundCardProps {
  fund: Fund
}

export const FundCard: FC<FundCardProps> = ({ fund }) => {
  const responsive = useResponsive()

  const totalAmountRaised = useFundTotalAmountRaised(fund)
  const fundTotalAmountRaisedPercent = Math.floor(
    (totalAmountRaised * 100) / (fund?.goalAmount || 0)
  )

  return (
    <Card
      className={responsive.sm && 'ant-card-horizontal'}
      cover={
        <div
          className='h-full min-h-[128px] bg-cover bg-center'
          style={{ backgroundImage: `url(${fund.imageUrl})` }}
        />
      }
    >
      <Card.Meta
        title={fund.name}
        description={
          <Typography.Paragraph ellipsis={{ rows: 3 }}>{fund.description}</Typography.Paragraph>
        }
      />
      <div className='flex flex-col gap-2'>
        <div className='flex'>
          <div className='flex-1'>Goal Amount:</div>
          <div className='flex-1'>
            {numeral(fund.goalAmount * ALGO_USDC_SCALE).format('0,0.00')} {fund.currency}
          </div>
        </div>
        <div>{`${fundTotalAmountRaisedPercent} % Raised`}</div>
        <Progress percent={fundTotalAmountRaisedPercent} />
      </div>
    </Card>
  )
}
