import dateFnsFormat from 'date-fns/format'
import dateFnsParse from 'date-fns/parse'
import _set from 'lodash/set'
import React from 'react'
import { DateUtils } from 'react-day-picker'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'
import {
  Button,
  Col,
  Container,
  Form,
  FormCheckbox,
  FormGroup,
  FormInput,
  FormTextarea,
  Row,
} from 'shards-react'
import {
  createEscrow,
  getFund,
  getOrg,
  getPerson,
  getProject,
  updateFund,
  uploadFundImage,
} from '../../clients/fundservice/fundservice-client'
import { getAccountTransactionContributions } from '../../services'

const ALGO_USDC_SCALE = 1 / 1000000

// Source Code Example used: https://gist.github.com/whoisryosuke/578be458b5fdb4e71b75b205608f3733
class FundManageForm extends React.Component {
  constructor(props) {
    super(props)

    this.format = 'MM/dd/yyyy'
    this.getData = this.getData.bind(this)
    this.handleEndDateChange = this.handleEndDateChange.bind(this)
    this.handleCreateEscrow = this.handleCreateEscrow.bind(this)
    this.handleVisibilityChange = this.handleVisibilityChange.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.onFileChange = this.onFileChange.bind(this)

    this.state = {
      id: 'Loading...',
      name: 'Loading...',
      description: 'Loading...',
      creatorId: 'Loading...',
      projectId: 'Loading...',
      organizationId: 'Loading...',
      startDate: 'Loading...',
      endDate: '01/01/2021',
      goalAmount: 'Loading...',
      goalAmountDisplay: 'Loading...',
      currency: 'Loading...',
      fundVisibility: 'Loading...',
      imageUrl: 'Loading...',
      videoUrl: 'Loading...',
      totalAmountRaised: 'Loading...',
      fundBalance: 'Loading...',
      transactions: [],
      fundState: 'Loading...',
      creatorName: 'Loading...',
      orgName: 'Loading...',
      projectName: 'Loading...',

      submitState: 'not_submitted',
      escrowCreateState: 'not_creating',

      selectedFile: null,
      imageUploadState: '',
    }
  }

  // on page load, retrieve fund details, balance, transactions
  componentDidMount() {
    this.getData()
  }

  getData() {
    // get fund details
    getFund(this.props.id).then((res) => {
      if (res?.status !== 200) {
        return this.setState({ submitState: 'error' })
      }

      const fund = res.data

      this.setState({
        id: fund.id,
        name: fund.name,
        description: fund.description,
        creatorId: fund.creatorId,
        projectId: fund.projectId,
        organizationId: fund.organizationId,
        startDate: fund.startDate,
        endDate: fund.endDate,
        goalAmount: fund.goalAmount,
        goalAmountDisplay: fund.goalAmount * ALGO_USDC_SCALE,
        currency: fund.currency,
        fundVisibility: fund.fundVisibility,
        imageUrl: fund.imageUrl,
        videoUrl: fund.videoUrl,
        totalAmountRaised: fund.totalAmountRaised,
        fundState: fund.fundState,
      })

      const address = fund.escrow?.address
      if (address) {
        getAccountTransactionContributions(address).then(([contribution]) => {
          this.setState({ fundBalance: contribution.transactionTotalAmount })
        })
      }

      getPerson(fund.creatorId).then((res) => {
        if (res) {
          this.setState({ creatorName: res.data.name })
        }
      })

      getOrg(fund.organizationId).then((res) => {
        if (res) {
          this.setState({ orgName: res.data.name })
        }
      })

      getProject(fund.projectId).then((res) => {
        if (res) {
          this.setState({ projectName: res.data.name })
        }
      })
    })

    // // get fund balance
    // getFundBalance(this.props.id).then((res) => {
    //   if (res?.status !== 200) {
    //     return this.setState({ fundBalance: 'Escrow unavailable' })
    //   }
    //   this.setState({ fundBalance: res.data.fundBalance })
    // })

    // // get fund transactions
    // getFundTransactions(this.props.id).then((res) => {
    //   if (res) {
    //     this.setState({ transactions: res.data })
    //   }
    // })
  }

  // form handle methods
  handleChange(event) {
    this.setState((prev) => _set(prev, event.target.name, event.target.value))
  }

  // form visibility handle methods
  handleVisibilityChange(event) {
    this.setState({ fundVisibility: event.target.checked ? 'PUBLISHED' : 'UNPUBLISHED' })
  }

  async handleSubmit(e) {
    e.preventDefault()

    console.log('A form was submitted: ' + this.state.name + ' ' + this.state.description)

    if (this.state.selectedFile) {
      this.setState({ imageUploadState: 'uploading image' })
      try {
        await this.uploadImage()
        this.setState({ imageUploadState: 'image uploaded' })
        setTimeout(() => this.setState({ imageUploadState: '' }))
      } catch (err) {
        return this.setState({ imageUploadState: `Image upload error: ${err.message}` })
      }
    }

    this.setState({ submitState: 'fetching' })

    const res = await updateFund(this.props.authHelper, {
      id: this.state.id,
      name: this.state.name,
      description: this.state.description,
      endDate: new Date(this.state.endDate).getTime(),
      goalAmount: this.state.goalAmount,
      imageUrl: this.state.imageUrl,
      videoUrl: this.state.videoUrl,
      fundVisibility: this.state.fundVisibility,
    })

    this.setState({ submitState: 'fetched', submitResponse: res })

    // update view
    this.getData()
  }

  handleCreateEscrow() {
    this.setState({ escrowCreateState: 'fetching' })

    createEscrow(this.props.authHelper, this.props.id).then((res) => {
      this.setState({ escrowCreateState: 'fetched', escrowResponse: res })
    })
  }

  handleGoalChange = (event) => {
    this.setState({
      goalAmount: event.target.value / ALGO_USDC_SCALE,
      goalAmountDisplay: event.target.value,
    })
  }

  // Date Picker Functions.
  handleEndDateChange(day) {
    this.setState({
      endDate: day,
    })
  }

  formatDate(date, format, locale) {
    return dateFnsFormat(date, format, { locale })
  }

  parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, new Date(), { locale })
    if (DateUtils.isDate(parsed)) {
      return parsed
    }
    return undefined
  }

  onFileChange(event) {
    this.setState({ selectedFile: event.target.files[0] })
  }

  async uploadImage() {
    const formData = new FormData()
    formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)

    // prepare and send data
    const buffer = await this.state.selectedFile.arrayBuffer()

    // convert to base64 - // https://stackoverflow.com/questions/9267899/arraybuffer-to-base64-encoded-string
    const bytes = new Uint8Array(buffer)
    let binary = ''
    const len = bytes.byteLength
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i])
    }
    const base64 = window.btoa(binary)

    const data = {
      name: this.state.selectedFile.name,
      type: this.state.selectedFile.type,
      data: base64,
    }

    const res = await uploadFundImage(this.props.authHelper, data, this.props.id)
    if (res?.status !== 200) {
      throw new Error(res.data?.message ?? res.statusText ?? 'Image upload error')
    }
    this.setState({ imageUrl: res.data.imageUrl })
  }

  render() {
    // set results display, if applicable
    const { submitState, imageUploadState } = this.state
    let submitResult = ''
    if (submitState === 'fetching') {
      // submitResult = <Alert theme="info">Updating fund, please wait...</Alert>;
      submitResult = <div className='processingMessage'>Updating fund, please wait...</div>
    }
    if (submitState === 'fetched') {
      if (this.state.submitResponse.status === 200) {
        // submitResult = <Alert theme="success">Fund updated successfully!</Alert>;
        submitResult = <div className='successMessage'>Fund updated successfully!</div>
      } else {
        // submitResult = <Alert theme="danger">ERROR while updating fund: {this.state.submitResponse.data.message}</Alert>;
        submitResult = (
          <div className='failMessage'>
            Unable to update fund: {this.state.submitResponse.data.message}
          </div>
        )
      }
    }

    // display escrow or button to create escrow
    // let escrowDisplay
    // if (
    //   this.state.fundBalance === 'Escrow unavailable' &&
    //   this.state.escrowCreateState === 'not_creating'
    // ) {
    //   escrowDisplay = null
    // } else if (this.state.escrowCreateState === 'fetching') {
    //   // escrowDisplay = <Alert theme="info">Creating escrow, please wait...</Alert>;
    //   escrowDisplay = <div className='processingMessage'>Creating escrow, please wait...</div>
    // } else if (this.state.escrowCreateState === 'fetched') {
    //   if (this.state.escrowResponse.status === 200) {
    //     // escrowDisplay = <Alert theme="success">Escrow successfully created!</Alert>;
    //     escrowDisplay = <div className='successMessage'>Escrow created successfully!</div>
    //   } else {
    //     // escrowDisplay= <Alert theme="danger">ERROR while creating escrow: {this.state.escrowResponse.data.message}</Alert>;
    //     escrowDisplay = (
    //       <div className='failMessage'>
    //         ERROR while creating escrow: {this.state.response.data.message}
    //       </div>
    //     )
    //   }
    // } else {
    //   escrowDisplay = (
    //     <label htmlFor='fundName'>
    //       Current Escrow Balance: {this.state.fundBalance * ALGO_USDC_SCALE}
    //     </label>
    //   )
    // }

    return (
      <Container className='fund-form'>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <label htmlFor='fundName'>Fund Name</label>
            <FormInput
              id='fundName'
              placeholder={this.state.name}
              name='name'
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <label htmlFor='fundDescription'>Description</label>
            <FormTextarea
              id='fundDescription'
              name='description'
              rows='3'
              placeholder={this.state.description}
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>Creator</label>
              </Col>
              <Col>
                <a href={`/organizations/persons/${this.state.creatorId}`}>
                  {this.state.creatorName}
                </a>
              </Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>Organization</label>
              </Col>
              <Col>
                <a href={`/organizations/org/${this.state.organizationId}`}>{this.state.orgName}</a>
              </Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>Project</label>
              </Col>
              <Col>
                <a href={`/organizations/projects/${this.state.projectId}`}>
                  {this.state.projectName}
                </a>
              </Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>Start Date</label>
              </Col>
              <Col>{this.state.startDate}</Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>End Date</label>
              </Col>
              <Col>
                <DayPickerInput
                  id='endDatePicker'
                  value={this.state.endDate}
                  onDayChange={this.handleEndDateChange}
                />
              </Col>
            </Row>
          </FormGroup>

          {/* <FormGroup>
            <span>{escrowDisplay}</span>
          </FormGroup> */}

          <FormGroup>
            <label htmlFor='goalAmount'>Goal Amount ({this.state.currency})</label>
            <FormInput
              id='goalAmount'
              placeholder={this.state.goalAmountDisplay}
              name='goalAmount'
              onChange={this.handleGoalChange}
            />
          </FormGroup>

          <Row>
            <Col sm={12} lg={2}>
              <label>Total Amount Raised</label>
            </Col>
            <Col>{(this.state.fundBalance || 0) * ALGO_USDC_SCALE}</Col>
          </Row>

          <Row>
            <Col sm={12} lg={2}>
              <label>Fund State</label>
            </Col>
            <Col>{this.state.fundState}</Col>
          </Row>

          <FormGroup>
            <label htmlFor='videoUrl'>Cover Image</label>
            <FormInput type='file' onChange={this.onFileChange} />
          </FormGroup>

          <FormGroup>
            <label htmlFor='videoUrl'>Fund YouTube Video ID </label>
            <FormInput
              id='videoUrl'
              placeholder={this.state.videoUrl}
              name='videoUrl'
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <FormCheckbox
              inline
              name='fundVisibility'
              checked={this.state.fundVisibility === 'PUBLISHED'}
              onChange={this.handleVisibilityChange}
            >
              Public in the funds list
            </FormCheckbox>
          </FormGroup>

          <Button type='submit' theme='accent' className='btn-primary'>
            Update Fund
          </Button>
          {submitResult}
          <p>{imageUploadState}</p>
        </Form>
      </Container>
    )
  }
}

export default FundManageForm
