import { fireEvent, render, waitFor } from '@testing-library/react'
import type { Fund } from '../../../types'
import { FundContributeResult, SUBMITTING_STATUS } from '../FundContributeResult'

describe('FundContributeResult', () => {
  const fund = {
    id: 123,
    name: 'Test Fund',
  } as unknown as Fund

  it('renders correctly when submittingStatus=SUBMITTING_STATUS.NOT_SUBMIT', () => {
    const { asFragment } = render(
      <FundContributeResult
        fund={fund}
        submittingStatus={SUBMITTING_STATUS.NOT_SUBMIT}
        errorText=''
      />
    )
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders correctly when submittingStatus=SUBMITTING_STATUS.LOADING', () => {
    const { asFragment } = render(
      <FundContributeResult fund={fund} submittingStatus={SUBMITTING_STATUS.LOADING} errorText='' />
    )
    expect(asFragment()).toMatchSnapshot()
  })

  it('renders correctly when submittingStatus=SUBMITTING_STATUS.SUCCEED', async () => {
    const { asFragment, container } = render(
      <FundContributeResult fund={fund} submittingStatus={SUBMITTING_STATUS.SUCCEED} errorText='' />
    )
    expect(asFragment()).toMatchSnapshot()

    // Override the location object from jsdom with writable property
    Object.defineProperty(window, 'location', {
      value: {
        href: '',
      },
      writable: true, // possibility to override
    })

    // Click one the 'Return to fund' button and check the location
    const button = container.querySelector('button')
    fireEvent.click(button)
    await waitFor(() => {
      expect(location.href).toBe(`/view/${fund.id}`)
    })
  })

  it('renders correctly when submittingStatus=SUBMITTING_STATUS.FAILED', () => {
    const { asFragment } = render(
      <FundContributeResult
        fund={fund}
        submittingStatus={SUBMITTING_STATUS.FAILED}
        errorText='Test error'
      />
    )
    expect(asFragment()).toMatchSnapshot()
  })

  it('returns null when submittingStatus does not supported', () => {
    const { asFragment } = render(
      <FundContributeResult
        fund={fund}
        submittingStatus={123 as unknown as SUBMITTING_STATUS}
        errorText='Test error'
      />
    )
    expect(asFragment()).toMatchSnapshot()
  })
})
