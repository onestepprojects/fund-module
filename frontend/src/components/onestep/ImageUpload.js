import {
  Container,
  Row,
  Col,
  Card,
  ListGroup,
  ListGroupItem,
  Form,
  Button,
  CardHeader,
  CardBody
} from 'shards-react'
import React, { Component } from 'react'

import { uploadFundImage } from '../../clients/fundservice/fundservice-client'

class ImageUpload extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedFile: null,
      uploadSuccess: ''
    }
  }

  // On file select (from the pop up)
  onFileChange = (event) => {
    this.setState({ selectedFile: event.target.files[0] })
  }

  // On file upload (click the upload button)
  onFileUpload = (e) => {
    e.preventDefault()

    const formData = new FormData()
    formData.append(
      'myFile',
      this.state.selectedFile,
      this.state.selectedFile.name
    )
    console.log(this.state.selectedFile)

    // prepare and send data
    this.state.selectedFile.arrayBuffer().then((buffer) => {
      // convert to base64 - // https://stackoverflow.com/questions/9267899/arraybuffer-to-base64-encoded-string
      var binary = ''
      var bytes = new Uint8Array(buffer)
      var len = bytes.byteLength
      for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      var base64 = window.btoa(binary)

      var data = {
        name: this.state.selectedFile.name,
        type: this.state.selectedFile.type,
        data: base64
      }
      uploadFundImage(this.props.authHelper, data, this.props.fundId).then(
        (res) => {
          this.setState({ uploadSuccess: 'Successfully uploaded image!' })

          // send url to parent
          this.props.updateImage(res.data.image)
        }
      )
    })

    // send
  }

  // File content to be displayed after
  // file upload is complete
  fileData = () => {
    if (this.state.selectedFile) {
      return (
        <div>
          <h5>File Details:</h5>
          <p>File Name: {this.state.selectedFile.name}</p>
          <p>File Type: {this.state.selectedFile.type}</p>
          <p>
            Last Modified:{' '}
            {this.state.selectedFile.lastModifiedDate.toDateString()}
          </p>
        </div>
      )
    }
  }

  render() {
    return (
      <Card className='fund-tile-card'>
        <h6>Upload image</h6>
        <input type='file' onChange={this.onFileChange} />
        <Button className='btn-primary' onClick={this.onFileUpload}>
          Upload
        </Button>
        {this.state.uploadSuccess}
      </Card>
    )
  }
}

export default ImageUpload
