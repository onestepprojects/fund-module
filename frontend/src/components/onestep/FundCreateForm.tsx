import dateFnsFormat from 'date-fns/format'
import dateFnsParse from 'date-fns/parse'
import _set from 'lodash/set'
import React, { FC } from 'react'
import { DateUtils } from 'react-day-picker'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Row,
} from 'shards-react'
import { createFund, getOrgs, getProjects } from '../../clients/fundservice/fundservice-client'

const ALGO_USDC_SCALE = 1 / 1000000

export const FundCreateForm: FC = (props) => {
  const navigate = useNavigate()

  return <FundCreateFormInternal {...props} navigate={navigate} />
}

/**
 * TODO: Refactor this component to use hooks instead of class components
 *
 * Source Code Example used @see https://gist.github.com/whoisryosuke/578be458b5fdb4e71b75b205608f3733
 */
class FundCreateFormInternal extends React.Component {
  constructor(props) {
    super(props)
    this.format = 'MM/dd/yyyy'
    this.handleStartDateChange = this.handleStartDateChange.bind(this)
    this.handleEndDateChange = this.handleEndDateChange.bind(this)

    const startDate = new Date()
    const endDate = new Date()
    endDate.setMonth(endDate.getMonth() + 3) // Set end date to 3 months from now

    this.state = {
      name: '',
      description: '',
      organizationId: '',
      projectId: '',
      startDate: dateFnsFormat(startDate, this.format),
      endDate: dateFnsFormat(endDate, this.format),
      escrow: { payId: '', address: '' },
      goalAmount: 1000.0,
      currency: 'USDC',
      orgs: [],
      projects: [],

      submitState: 'not_submitted',
    }
  }

  getProjectsForOrg = (orgId) => {
    console.log('getting projects for: ' + orgId)

    // Get projects. Sets default chosen project (can be blank, if list is empty)
    getProjects(orgId).then((res) => {
      if (res) {
        this.setState({
          projects: res.data,
        })
        if (res.data.length > 0) {
          this.setState({
            projectId: res.data[0].id,
          })
        } else {
          this.setState({
            projectId: '',
          })
        }
      }
    })
  }

  componentDidMount() {
    if (this.props.person !== null) {
      // Get orgs
      getOrgs(this.props.person.uuid).then((res) => {
        if (res) {
          this.setState({
            orgs: res.data,
          })
          if (res.data.length > 0) {
            this.setState({
              organizationId: res.data[0].id,
            })

            // fill up proj menu based on default selection
            this.getProjectsForOrg(res.data[0].id)
          }
        }
      })
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.person !== prevProps.person) {
      // Get orgs
      getOrgs(this.props.person.uuid).then((res) => {
        if (res) {
          this.setState({
            orgs: res.data,
          })
          if (res.data.length > 0) {
            this.setState({
              organizationId: res.data[0].id,
            })

            // fill up proj menu based on default selection
            this.getProjectsForOrg(res.data[0].id)
          }
        }
      })
    }
  }

  handleChange = (event) => {
    this.setState((prev) => _set(prev, event.target.name, event.target.value))
  }

  handleOrgChange = (event) => {
    console.log('setting org ID to: ' + event.target.value)
    this.setState({ organizationId: event.target.value })

    // fill up proj menu if org changes
    this.getProjectsForOrg(event.target.value)
  }

  handleProjChange = (event) => {
    console.log('setting proj ID to: ' + event.target.value)
    this.setState({ projectId: event.target.value })
  }

  handleSubmit = (e) => {
    e.preventDefault()

    console.log('A form was submitted: ' + this.state.name + ' ' + this.state.description)

    this.setState({ submitState: 'fetching' })

    createFund(this.props.authHelper, {
      name: this.state.name,
      description: this.state.description,
      organizationId: this.state.organizationId,
      projectId: this.state.projectId,
      startDate: new Date(this.state.startDate).getTime(),
      endDate: new Date(this.state.endDate).getTime(),
      escrow: { address: this.state.escrow.address },
      goalAmount: this.state.goalAmount / ALGO_USDC_SCALE,
      currency: this.state.currency,
    }).then((res) => {
      this.setState({ submitState: 'fetched', response: res })
      const id = res?.data?.id
      if (res.status === 200 && id) {
        this.props.navigate(`/view/${id}`)
      }
    })
  }

  // Date Picker Functions.
  handleStartDateChange(day) {
    this.setState({
      startDate: day,
    })
  }

  // Date Picker Functions.
  handleEndDateChange(day) {
    this.setState({
      endDate: day,
    })
  }

  formatDate(date, format, locale) {
    return dateFnsFormat(date, format, { locale })
  }

  parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, new Date(), { locale })
    if (DateUtils.isDate(parsed)) {
      return parsed
    }
    return undefined
  }

  render() {
    // set results display, if applicable
    const submitState = this.state.submitState
    let submitResult = ''
    if (submitState === 'fetching') {
      // submitResult = <Alert theme="info">Creating fund, please wait...</Alert>;
      submitResult = <div className='processingMessage'>Creating fund, please wait...</div>
    }
    if (submitState === 'fetched') {
      // submitResult = <Alert theme="danger">Error while creating fund: {this.state.response.data.message}</Alert>;
      submitResult = (
        <div className='failMessage'>Unable to create fund: {this.state.response.data.message}</div>
      )
    }

    return (
      <Container className='fund-form'>
        <Form onSubmit={this.handleSubmit}>
          {/* Name */}
          <FormGroup>
            <label htmlFor='fundName'>Fund Name</label>
            <FormInput
              id='fundName'
              placeholder='Fund Name'
              name='name'
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <label htmlFor='fundDescription'>Description</label>
            <FormTextarea
              id='fundDescription'
              name='description'
              rows='3'
              placeholder='Provide some details about the fund'
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <label>Relief Organization</label>
            <FormSelect
              name='organizationId'
              onChange={this.handleOrgChange}
              value={this.state.selectedOrg}
            >
              {this.props.person &&
                this.state.orgs.map((org, index) => (
                  <option key={org.id} value={org.id}>
                    {org.name}
                  </option>
                ))}
            </FormSelect>
          </FormGroup>

          {/* the following needs to be pulled from team 1 API based on logged in user */}
          <FormGroup>
            <label>Relief Project</label>
            <FormSelect
              name='projectId'
              onChange={this.handleProjChange}
              value={this.state.selectedProj}
            >
              {this.props.person &&
                this.state.projects.map((proj, index) => (
                  <option key={proj.id} value={proj.id}>
                    {proj.name}
                  </option>
                ))}
            </FormSelect>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>Start Date</label>
              </Col>
              <Col>
                <DayPickerInput
                  id='startDatePicker'
                  formatDate={this.formatDate}
                  format={this.format}
                  parseDate={this.parseDate}
                  placeholder={this.state.startDate}
                  onDayChange={this.handleStartDateChange}
                />
              </Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col sm={12} lg={2}>
                <label>End Date</label>
              </Col>
              <Col>
                <DayPickerInput
                  id='endDatePicker'
                  formatDate={this.formatDate}
                  format={this.format}
                  parseDate={this.parseDate}
                  placeholder={this.state.endDate}
                  onDayChange={this.handleEndDateChange}
                />
              </Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <label htmlFor='escrowAddress'>Blockchain Address (Only Support USDC so far)</label>
            <FormInput
              id='escrowAddress'
              placeholder={this.state.escrow.address}
              name='escrow.address'
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <label htmlFor='goalAmount'>Goal Amount (Only Support USDC so far)</label>
            <FormInput
              id='goalAmount'
              placeholder={this.state.goalAmount}
              name='goalAmount'
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup style={{ display: 'none' }}>
            <label htmlFor='projectType'>Currency</label>
            <FormSelect
              id='currency'
              name='currency'
              onChange={this.handleChange}
              title='OneStep only supports USDC so far.'
              value='USDC'
              disabled
            >
              <option value='USDC'>USDC</option>
            </FormSelect>
          </FormGroup>

          <Button type='submit' theme='accent' className='btn-primary'>
            Create Fund
          </Button>
          {submitResult}
        </Form>
      </Container>
    )
  }
}
