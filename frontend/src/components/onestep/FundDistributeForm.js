import React from 'react'
import 'react-day-picker/lib/style.css'
import { Link } from 'react-router-dom-v5'

import {
  Button,
  Card,
  CardHeader,
  Col,
  Form,
  FormInput,
  ListGroup,
  ListGroupItem,
  Row,
} from 'shards-react'

import { distributeFunds } from '../../clients/fundservice/fundservice-client'

const ALGO_USDC_SCALE = 1 / 1000000

//Source Code Example used: https://gist.github.com/whoisryosuke/578be458b5fdb4e71b75b205608f3733
class FundDistributeForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      receiverAddress: '',
      amount: 0.0,
      fundId: this.props.id,

      submitState: 'not_submitted',
      amountError: false,
      distributeSuccess: false,
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleSubmit = (event) => {
    event.preventDefault()

    console.log('Validating fields...')
    if (this.state.amount <= 0 || isNaN(this.state.amount) || this.state.amount == '') {
      this.setState({ amountError: true })
      return
    } else {
      this.setState({ amountError: false })
    }

    this.setState({ submitState: 'fetching' })

    distributeFunds(this.props.authHelper, {
      receiverAddress: this.state.receiverAddress,
      amount: this.state.amount / ALGO_USDC_SCALE,
      fundId: this.state.fundId,
    }).then((res) => {
      this.setState({ submitState: 'fetched', response: res })

      if (res.status === 200) {
        this.setState({ distributeSuccess: true })
      }
    })
  }

  render() {
    // set results display, if applicable
    const submitState = this.state.submitState
    var submitResult = ''
    var backToFundBtn = ''
    if (submitState === 'fetching') {
      // submitResult = <Alert theme="info">Processing transaction, please wait...</Alert>;
      submitResult = <div className='processingMessage'>Processing transaction, please wait...</div>
    }
    if (submitState === 'fetched') {
      if (this.state.response.status === 200) {
        // submitResult = <Alert theme="info">Transaction complete!</Alert>;
        submitResult = <div className='successMessage'>Transaction complete!</div>
        backToFundBtn = (
          <Link to={`/view/${this.state.response.data.fundId}`}>Return To Fund Page</Link>
        )
      } else {
        // submitResult = <Alert theme="danger">Unable to distribute funds: {this.state.response.data.message}</Alert>;
        submitResult = (
          <div className='failMessage'>
            Unable to distribute funds: {this.state.response.data.message}
          </div>
        )
      }
    }

    return (
      <Card small className='mb-4'>
        <CardHeader className='border-bottom'>
          <h6 className='m-0'>Enter Details</h6>
        </CardHeader>
        <ListGroup flush>
          <ListGroupItem className='p-3'>
            <Row>
              <Col>
                <Form onSubmit={this.handleSubmit}>
                  <Row form>
                    <Col md='6' className='form-group'>
                      <label htmlFor='receiverAddress'>Recipient Account Address</label>
                      <FormInput
                        id='receiverAddress'
                        placeholder='Enter Recipient Account Address'
                        name='receiverAddress'
                        onChange={this.handleChange}
                      />
                    </Col>
                  </Row>
                  <Row form>
                    <Col md='6' className='form-group'>
                      <label htmlFor='amount' key={this.props.fundCurrency}>
                        Amount ({this.props.fundCurrency})
                      </label>
                      <FormInput
                        id='amount'
                        name='amount'
                        rows='3'
                        placeholder='Enter Amount'
                        onChange={this.handleChange}
                      />
                    </Col>
                  </Row>

                  {/* Do not display button if transaction went through */}
                  {!this.state.distributeSuccess && (
                    <Button type='submit' theme='accent' className='btn-primary'>
                      Distribute
                    </Button>
                  )}

                  {/* Form messages--error, submit status, and button to return to fund page */}
                  {this.state.amountError && (
                    <div className='errorFormMessage'>
                      Please enter a donation amount greater than zero.
                    </div>
                  )}
                  {submitResult}
                  {backToFundBtn}
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Card>
    )
  }
}

export default FundDistributeForm
