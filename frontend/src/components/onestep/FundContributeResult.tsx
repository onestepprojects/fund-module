import { Button, Result, Spin } from 'antd'
import { type FC } from 'react'
import type { Fund } from '../../types'

export enum SUBMITTING_STATUS {
  NOT_SUBMIT,
  LOADING,
  SUCCEED,
  FAILED,
}

interface FundContributeResultProps {
  fund: Fund
  submittingStatus: SUBMITTING_STATUS
  errorText: string
}

export const FundContributeResult: FC<FundContributeResultProps> = ({
  fund,
  submittingStatus,
  errorText,
}) => {
  switch (submittingStatus) {
    case SUBMITTING_STATUS.NOT_SUBMIT:
      return <Result status='404' title='' />
    case SUBMITTING_STATUS.LOADING:
      return (
        <Result
          icon={<Spin size='large' />}
          title='Please wait while we process your donation'
          subTitle='We will notify you once the transaction is confirmed.'
        />
      )
    case SUBMITTING_STATUS.SUCCEED:
      return (
        <Result
          status='success'
          title='Thank you for your contribution!'
          extra={
            <Button
              type='primary'
              style={{
                width: '100%',
                height: 'auto',
                whiteSpace: 'normal',
              }}
              onClick={() => {
                location.href = `/view/${fund.id}`
              }}
            >
              Return to "{fund.name}"
            </Button>
          }
        />
      )
    case SUBMITTING_STATUS.FAILED:
      return (
        <Result
          status='warning'
          title='Service is unavailable right now, please try again later.'
          subTitle={errorText}
        />
      )
    default:
      return null
  }
}
