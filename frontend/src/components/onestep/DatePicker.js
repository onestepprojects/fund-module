import React, { useState } from 'react'

import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

export default function TableDatePicker() {
  let [date, setDate] = useState(new Date())

  return (
    <DatePicker
      className='mb-3'
      selected={date}
      onChange={(date) => setDate(date)}
    />
  )
}
