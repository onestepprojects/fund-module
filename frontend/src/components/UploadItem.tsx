import { PlusOutlined } from '@ant-design/icons'
import { Upload, type UploadFile, type UploadProps } from 'antd'
import { useCallback, useMemo, type FC } from 'react'
import { readFileDataURL } from '../utils'

interface UploadItemProps
  extends Omit<
    UploadProps,
    'maxCount' | 'listType' | 'showUploadList' | 'beforeUpload' | 'fileList' | 'onChange'
  > {
  title?: string
  value?: string
  onChange?: (value: string) => void
}

const UploadButton: FC<{ title: string }> = ({ title }) => (
  <div>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>{title}</div>
  </div>
)

export const UploadItem: FC<UploadItemProps> = (props) => {
  const {
    accept = 'image/jpeg, image/png',
    title = 'Upload',
    value,
    onChange,
    ...uploadProps
  } = props

  const showUploadList = useMemo(
    () => (value ? { showRemoveIcon: true, showPreviewIcon: false } : false),
    [value]
  )

  const fileList = useMemo(() => [{ url: value } as UploadFile], [value])

  const beforeUpload = useCallback(() => false, [])

  return (
    <Upload
      {...uploadProps}
      accept={accept}
      maxCount={1}
      listType='picture-card'
      showUploadList={showUploadList}
      beforeUpload={beforeUpload}
      fileList={fileList}
      onChange={async (e) => {
        if (e.file.status === 'removed') {
          return onChange('')
        }
        const url = await readFileDataURL(e.file as unknown as File)
        onChange(url)
      }}
    >
      {!value && <UploadButton title={title} />}
    </Upload>
  )
}
