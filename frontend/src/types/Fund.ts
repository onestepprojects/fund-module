export interface Fund {
  id: string
  name: string
  escrow: { address: string }
  description: string
  creatorId: string
  projectId: string
  organizationId: string
  startDate: string
  endDate: string
  goalAmount: number
  currency: string
  fundVisibility: string
  imageUrl: string
  videoUrl: string
  bannerImageUrl: string
  bodyLeftImageUrl: string
  bodyLeftVideoUrl: string
  bodyRightImageUrl: string
  bodyRightVideoUrl: string
  reportLeftImageUrl: string
  reportMiddleImageUrl: string
  reportRightImageUrl: string
  totalAmountRaised: number
  fundState: string
}
