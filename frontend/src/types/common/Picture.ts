export interface Picture {
  name?: string // unique name as id
  size?: number
  format?: 'JPG' | 'PNG'
  body?: string
  caption?: string
  bucketName?: string
  creator?: string
  url?: string
}
