export interface PersonRole {
  id: string
  name: string
  roles: string[]
}
