export interface TransactionContribution {
  assetId: number
  assetName: string
  decimals: number
  transactionCount: number
  transactionTotalAmount: number
  transactionDisplayAmount: number
}
