<!-- Publish revision: 1 -->

# **Team 3 Project Area**

## Table of Contents
- [**Team 3 Project Area**](#team-3-project-area)
  - [Table of Contents](#table-of-contents)
  - [Team Members](#team-members)
  - [Product Owner](#product-owner)
  - [Faculty](#faculty)
  - [Technologies](#technologies)
  - [Project Overview](#project-overview)
  - [Project Plan](#project-plan)
  - [Fund Module](#fund-module)
    - [Introduction](#introduction)
    - [Requirements](#requirements)
    - [Organization](#organization)
      - [Back-End](#back-end)
        - [API](#api)
          - [Files:](#files)
        - [Model](#model)
          - [Files:](#files-1)
        - [Service](#service)
          - [Files:](#files-2)
      - [Front-End](#front-end)
  - [Resource Module](#resource-module)


## Team Members

* [Erik Subatis](ers739@g.harvard.edu)
* [Gregory Retter](grr486@g.harvard.edu)
* [Joshua Markovic](jom5985@g.harvard.edu)
* [Shantha Kumari Rajendran](rajendran@g.harvard.edu)
* [Sharjil Khan](shk305@g.harvard.edu)
* [Zane Hernandez](zah693@g.harvard.edu)

## Product Owner

* Nitya Timalsina

## Faculty

* Annie Kamlang
* Eric Gieseke
* Hannah Riggs

## Technologies

* Programming Languages
    * Java
    * Javascript
* Data Persistence
    * Dynamo DB
    * S3
* Misc.
    * React
    * DropWizard
    * IntelliJ
    * Node.js
    * Shards

## Project Overview

This repository is set for the development of one quarter of the OneStep Relief
Platform. Specifically the code and assets stored here are for the 
organization, definition, and implementation of the platform's Fund and
Resource Modules. Each of these modules will be further elaborated on in their
own sections as seen below.

## Project Plan

1. Sprint 1
    1. Fund Module
2. Sprint 2
3. Sprint 3
4. Sprint 4

## Fund Module

### Introduction
The Fund Module facilitates all the crowdfunding and general funding functions 
within the OneStep Relief Platform. Such function include creating a relief 
project fund and the reception and distribution of funds. The module allows for 
the creation, update and management of Crowdfunds, which represent a 
fundraising goal for a designated purpose; concrete examples could include 
building a new school or purchasing food/beds for disaster stricken areas.

### Requirements

The requirements of the OneStep Relief Platform's Fund Module are:
* Create and Manage Crowfunds of for OneStep Relief Projects
* Transfer funds to/from Crowdfunds via blockchain utility service

### Organization

The following outlines the layout of the fund module portion of Team 3's 
OneStep Relief Repository.

#### Back-End

This is where the fund module's back-end code and tests are stored. These subsections are 
further divided into three main folders: API, Model, and Service. 
All three use the following path: org.onestep.relief.crowfund

##### API

This subsection contains the REST API for the Fund Module. 
Utilizing DropWizard, the code contained here collects requests and sends 
responses between the UI of the front-end and the Dynamo DB database of the
back-end.

###### Files:
* App.java - Application class to run the fund module's back-end

* CrowdFundConfiguration.java - Configuration class to define the config of the 
fund module

* DefaultHealthCheck.java - Definition of the health check for the fund module

* FundRequest.java - Class to define fund related requests to the database

* FundResponse.java - Class to define fund related responses to the database

* Page.java - Class to define pagination for the fund module's UI

##### Model

This subsection contains the Dynamo DB service and object classes of the Fund
Module. The fund module's Dynamo Database and its tables are accessed here in 
response to requests from the REST API.

###### Files:
* Donation.java - Definition of a donation object. One of three transaction
classes employed by the module to transfer money between funds (user to fund)

* DynamoDBConfiguration.java - Configuration class to define the parameters of
the Amazon AWS Dynamo DB database employed by the fund module.

* Fund.java - Definition of a fund object. Funds are created to store money for
particular relief projects.

* FundDynamoDBService.java - Service class that contains java code that enables
direct interaction with the Fund Module's Dynamo DB database.

* FundState.java - Enumeration class used to define the current status of a 
Fund. Employed by the Fund object class.

##### Service

This subsection contains the service implementation that translates REST API
requests and responses to Dynamo DB calls.

###### Files:
* CrowdFundException.java - Exception class for the Fund Module. Triggers for 
various errors that could occur in using the Fund Module.

* CrowdFundService.java - Service implementation class. Acts as the bridge
connecting the REST API calls to the DynamoService to affect changes to
the database.

* CrowdFundServiceResource.java - Class to answer requests from the UI 
supplying relevant data from the Dynamo DB database through the 
DynamoDBService and the implementation of the Crowd Fund Service.

* ICrowdFundService.java - Java interface for the crowdfund service.
Defines the methods available to the service implementation class.

#### Front-End

This is where the fund module's front-end code is stored. It is broken into 
various subsections containing code written in javascript. 
These sections include but are not limited too:

* Assets - Contains features that add more functionality to a website
(e.g. date picker)

* Components - Contains the various elements and widgets of a view webpage.

* Data - Contains the various subelements for various pieces of the front-end
(e.g. the buttons on the side bar menu)

* Flux - Contains JS code to handle elements of the layout that can change
(e.g. the side bar)

* Images - Contains various images used by the front-end of the Fund Module.
Examples include user avatars.

* Layouts - Contains html to define the layout of special elements such as:
    * Navigation Bar
    * Side Bar
    * Footer

* Shards-Dashboard - Contains details and examples of how to use the bootstrap
tool shards dashboard for a user interface.

* Styles - Contains CSS code defining attributes used in determining the look
of views in the Fund Module.

* Utils - Contains utility elements for a web app such as JS code to create 
charts.

* Views - Contains JS code describing the design of the webpages used by the
Fund Module.

## Resource Module
To Be Added...
