# OneStep Funding Module Backend

## How to Run

The backend is written in Java, and the entry point to the program is Dropwizard's
App.java in org.onestep.relief.crowdfund.api. It is recommended to appropriately configure
certain environment variables for the DB and external API clients that the service uses.
For more information see the bottom of this README.

**_TODO: This will be running from a docker image; update this with relevant instructions_**

## Environment Variables

The backend points to OneStep's blockchain service that it leverages via REST. It
also points to an AWS Dynamo DB instance to store its persistent data. URLs and paths
should be set via environment variables. Defaults, outlined below as of 3/25/21, are only used
if the application fails to load values from the environment variables defined below. Note
that this is NOT the case for the AWS Dynamo DB credentials/keys.

#### Dynamo DB

- ONESTEP_CROWDFUND_DB_REGION - The region of the DB, ex. "us-east-1".
- ONESTEP_CROWDFUND_DB_URL - The URL (which includes region) of the DB, ex. "https://dynamodb.us-east-1.amazonaws.com""
- Public/private keys for DB access should also be configured per Dynamo DB specs.

#### Blockchain Service

- ONESTEP_BLOCKCHAIN_URL - Base URL of the blockchain service, ex. "https://www.onestepprojects.tk/onestep/blockchain"
- ONESTEP_BLOCKCHAIN_CREATE_ACCOUNT - Path for create account, ex. "/account/createAccount"
- ONESTEP_BLOCKCHAIN_SEND_ALGO - Path for send algo, ex. "/transaction/sendAlgo"
- ONESTEP_BLOCKCHAIN_SEND_USDC - Path for send USDC, ex. "/transaction/sendUsdc"
- ONESTEP_BLOCKCHAIN_OPTIN_USDC - Path to opt in account to USDC, ex. "/transaction/optInUsdc"
- ONESTEP_BLOCKCHAIN_GET_BALANCE - Path to get account balance, ex. "/account/getBalance"

## Debugging in VSCode

Create a `.env` file.

```bash
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=

ONESTEP_CROWDFUND_CLIENT_ID=
ONESTEP_CROWDFUND_SECRET_KEY=

ONESTEP_CROWDFUND_DB_REGION=us-east-2
ONESTEP_CROWDFUND_DB_URL=https://dynamodb.us-east-2.amazonaws.com
ONESTEP_CROWDFUND_DB_PREFIX=staging
ONESTEP_CROWDFUND_S3_REGION=us-east-1
ONESTEP_CROWDFUND_S3_BUCKET=fundresource

ONESTEP_AUTHORIZATION_URL=https://staging.onestepprojects.org/onestep/authorization
ONESTEP_BLOCKCHAIN_URL=https://staging.onestepprojects.org/
ONESTEP_ORG_URL=https://staging.onestepprojects.org/onestep/
ONESTEP_REWARDS_URL=https://staging.onestepprojects.org/
```

Create a `.vscode/launch.json`.

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "java",
      "name": "Organization Service",
      "cwd": "${workspaceFolder}/backend",
      "envFile": "${workspaceFolder}/backend/.env",
      "request": "launch",
      "mainClass": "org.onestep.relief.dropwizard.App",
      "args": "server",
      "vmArgs": "-DskipTests -Ddw.server.applicationConnectors[0].port=3105 -Ddw.server.adminConnectors[0].port=3205"
    }
  ]
}
```

This will run java backend server under debug mode. The api will be available at `http://127.0.0.1:3105`.

## REST API

**_TODO: The URLs below should be updated after docker deployment_**

### Get All Funds

**_TODO: Escrow representation likely to change. At a minimum, will not return escrow mnemonic in practice_**

GET .../crowdfund/fund \
_Attributes_: \
None
<br><br>
_Returns_: \
json with a list of all funds. Note that transactions and actual fund balance are not updated during this call at the moment (and this will be null/zero respectively). Example:

```json
[
  {
    "id": "c5860dce-bcca-4194-9d60-4da8c3fe7c5a",
    "name": "fund name",
    "description": "fund description",
    "creatorId": "erik",
    "projectId": "p5125-asfa",
    "organizationId": "o1241-asfa",
    "escrow": {
      "address": "...",
      "mnemonic": "..."
    },
    "startDate": 16140432279910,
    "endDate": 1614043227998,
    "goalAmount": 15000.0,
    "currency": "ALGO",
    "fundState": "STARTED",
    "totalAmountRaised": 1.0,
    "fundBalance": 0.0,
    "transactions": null
  },
  {
    "id": "5b8f543d-5208-4d46-96a5-1810a3b3644b",
    "name": "COVID Relief",
    "description": "Relief for all COVID patients",
    "creatorId": "default_creator",
    "projectId": "default_projectid",
    "organizationId": "orgid",
    "escrow": {
      "address": "...",
      "mnemonic": "..."
    },
    "startDate": 1612155600000,
    "endDate": 1617163200000,
    "goalAmount": 100000.0,
    "currency": "ALGO",
    "fundState": "CREATED",
    "totalAmountRaised": 1014.0,
    "fundBalance": 0.0,
    "transactions": null
  }
]
```

### Get Fund

**_TODO: Escrow representation likely to change. At a minimum, will not return escrow mnemonic in practice_**

GET .../crowdfund/fund/{fund-id} \
_Attributes_: \
fund-id (String): the ID of the desired fund
<br><br>
_Returns_: \
json with a new generated address and associated mnemoic, example:

```json
{
  "id": "62c3eeb6-3ed1-4a73-939d-f162218c04d2",
  "name": "Disaster Relief and School Reconstruction",
  "description": "One Step Projects is collaborating with local and international organizations, educational institutions, and community stakeholders to rebuild a school in Badegaun, Nepal using earthquake-proof technology and locally-sourced materials.",
  "creatorId": "Nitya",
  "projectId": "default_projectid",
  "organizationId": "default_orgid",
  "escrow": {
    "address": "...",
    "mnemonic": "..."
  },
  "startDate": 1613538000000,
  "endDate": 1626321600000,
  "goalAmount": 100000.0,
  "currency": "ALGO",
  "fundState": "CREATED",
  "totalAmountRaised": 1.0,
  "fundBalance": 201000.0,
  "transactions": [
    {
      "id": "b137670d-e795-4c57-a8a6-2a4036d5c1b0",
      "senderAddress": "Q3JBCDN5MOTLDL6QW75CR4ITENMSC76R766V3U3AA3TEZA7G5PIY4VKE4I",
      "senderMnemonic": null,
      "receiverAddress": "DMM6IO2RMPD5VCPJWZYZZT7QXAR6PGF5BFXMW7T66XEPYE3LPNJZ2P5SAA",
      "amount": 1.0,
      "personId": "erik",
      "fundId": "62c3eeb6-3ed1-4a73-939d-f162218c04d2",
      "transactionId": "QRTW5ZVGYN3RU44JP5BAMSTZ5FASOHCMVNAWARJKV4MY7H5JIWYA",
      "date": 1617313459554,
      "type": "DONATION",
      "currency": "ALGO"
    }
  ]
}
```

### Create Fund

**_TODO: Escrow representation likely to change. At a minimum, will not return escrow mnemonic in practice_**

POST .../crowdfund/fund \
_Attributes_: \
JSON object, example:

```json
{
  "name": "fund name",
  "description": "fund description",
  "creatorId": "erik",
  "projectId": "ttt",
  "organizationId": "org123",
  "startDate": 16140432279910,
  "endDate": 1614043227998,
  "goalAmount": 15000.0,
  "currency": "ALGO",
  "fundState": "CREATED"
}
```

<br><br>
_Returns_: \
json with a newly generated Fund, example:

```json
{
  "id": "7b3abb71-3cd6-46ce-9019-7d42897678d8",
  "name": "CCCCCCC",
  "description": "Sprint 3 test - validation test",
  "creatorId": "erik",
  "projectId": "ttt",
  "organizationId": "fake org",
  "escrow": {
    "address": "M6LYFL2263OKKZ6GK6HTHCTHWSATC45JQYXAUPYXFLRGRFPN2KQXBV7C2I",
    "mnemonic": "appear nurse mountain romance cup charge predict powder balcony wife ball wrestle august truck brand leave among mixed cute beauty cabin shrug ketchup absorb ill"
  },
  "startDate": 16140432279910,
  "endDate": 1614043227998,
  "goalAmount": 15000.0,
  "currency": "ALGO",
  "fundState": "STARTED",
  "totalAmountRaised": 0.0,
  "fundBalance": 0.0,
  "transactions": null
}
```

### Update Fund

**_TODO: Escrow representation likely to change. At a minimum, will not return escrow mnemonic in practice_**

PUT .../crowdfund/fund \
_Attributes_: \
JSON object, example:

```json
{
  "name": "fund name",
  "description": "fund description",
  "creatorId": "erik",
  "projectId": "ttt",
  "organizationId": "org123",
  "startDate": 16140432279910,
  "endDate": 1614043227998,
  "goalAmount": 15000.0,
  "currency": "ALGO",
  "fundState": "CREATED"
}
```

<br><br>
_Returns_: \
json with the updated Fund, example:

```json
{
  "id": "7b3abb71-3cd6-46ce-9019-7d42897678d8",
  "name": "CCCCCCC",
  "description": "Sprint 3 test - validation test",
  "creatorId": "erik",
  "projectId": "ttt",
  "organizationId": "fake org",
  "escrow": {
    "address": "M6LYFL2263OKKZ6GK6HTHCTHWSATC45JQYXAUPYXFLRGRFPN2KQXBV7C2I",
    "mnemonic": "appear nurse mountain romance cup charge predict powder balcony wife ball wrestle august truck brand leave among mixed cute beauty cabin shrug ketchup absorb ill"
  },
  "startDate": 16140432279910,
  "endDate": 1614043227998,
  "goalAmount": 15000.0,
  "currency": "ALGO",
  "fundState": "STARTED",
  "totalAmountRaised": 0.0,
  "fundBalance": 0.0,
  "transactions": null
}
```

### Delete fund

GET .../crowdfund/fund-delete/{fund-id} \
_Attributes_: \
fund-id (string): ID of fund to delete

_Returns_ \
Blank successful response (204)

### Make Donation

**_TODO: Mnemonics will not be visible in practice_**

POST .../crowdfund/donation \
_Attributes_: \
JSON object, example:

```json
{
  "senderAddress": "...",
  "senderMnemonic": "...",
  "amount": 1.0,
  "personId": "erik",
  "fundId": "..."
}
```

<br><br>
_Returns_: \
json with a newly generated donation (transaction), example:

```json
{
  "id": "b137670d-e795-4c57-a8a6-2a4036d5c1b0",
  "senderAddress": "...",
  "senderMnemonic": "...",
  "receiverAddress": "...",
  "amount": 1.0,
  "personId": "erik",
  "fundId": "...",
  "transactionId": "...",
  "date": 1617313459554,
  "type": "DONATION"
}
```
