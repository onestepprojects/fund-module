package org.onestep.relief.crowdfund.model;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

/**
 * OneStep Relief Platform - Fund Module
 * Team 3 - Erik Subatis, Gregory Retter, Zane Hernandez,
 * Shantha Kumari Rajendran, Joshua Markovic, & Sharjil Kha
 *
 * JUnit testing for DynamoDB service.
 *
 * Author: Erik Subatis
 * Created: 2/19/2021
 * Last Updated: 4/1/2021
 */

public class FundDynamoDBServiceTest {

    private DynamoDBConfiguration dbConfig;
    private FundDynamoDBService dynamoService;

    @Before
    public void setup(){
//        dbConfig = new DynamoDBConfiguration();
//        dynamoService = new FundDynamoDBService(dbConfig);
    }

    @Test
    public void testAddFund() {
//        Fund fund = new Fund();
//        fund.setName("JUNIT test fund");
//        fund.setCreatorId("JUNIT");
//        fund.setOrganizationId("test organization");
//        fund.setDescription("testAddFund");
//        fund.setProjectId("test project");
//        fund.setFundState(FundState.CREATED);
//        fund.setEscrow(new Account("test address", "test mnemonic"));
//        fund.setStartDate(new Date());
//        fund.setEndDate(new Date());
//        fund.setGoalAmount(1000.00);
//        fund.setCurrency(Currency.ALGO);
//        fund.setTotalAmountRaised(100.00);
//
//        dynamoService.save(fund);
//        Fund retrievedExample = dynamoService.loadFund(fund.getId());
//
//        assertEquals(retrievedExample.getId(), fund.getId());
    }

}
