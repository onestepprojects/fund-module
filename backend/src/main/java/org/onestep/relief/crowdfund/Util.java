package org.onestep.relief.crowdfund;

import org.onestep.relief.crowdfund.service.CrowdFundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Util {

    private final static Logger LOGGER = LoggerFactory.getLogger(Util.class);

    // helper function for getting environment vars or setting defaults
    public static String getEnv(String key, String fallback) {
        if (System.getenv(key) == null) {
            LOGGER.info("Env var " + key + " not set. Using default");
            return fallback;
        }
        return System.getenv(key);
    }

}
