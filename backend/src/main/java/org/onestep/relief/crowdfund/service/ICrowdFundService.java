package org.onestep.relief.crowdfund.service;

import org.onestep.relief.crowdfund.model.*;

import java.io.IOException;
import java.util.List;

/**
 * OneStep Relief Platform - Fund Module service interface.
 * Allows CRUD of funds and the ability to make donations/distribute money
 * to/from funds.
 */
public interface ICrowdFundService {

    Fund createFund(Fund fund, String authToken) throws CrowdFundServiceAuthenticationException;

    Account createFundEscrow(String fundId, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException;

    Fund updateFund(Fund fund, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException;

    Fund uploadFundImage(String fundId, FundImageFile file, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException, IOException;

    Fund getFund(String fundID) throws CrowdFundServiceException;

    List<Fund> getFunds(String organizationId, String projectId, String creatorId, String fundVisibility);

    List<Fund> getFunds(FundVisibility fundVisibility);

    List<Fund> getOrgFunds(String orgId);

    List<Fund> getUserFunds(String authToken) throws CrowdFundServiceAuthenticationException;

    void closeFund(String fundId, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException;

    Transaction makeDonation(Transaction donation, String authToken) throws CrowdFundServiceException;

    Transaction distributeFunds(Transaction transaction, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException;

    double getFundBalance(String fundId) throws CrowdFundServiceException;

    List<Transaction> getFundTransactions(String fundId) throws CrowdFundServiceException;

    List<Donation> getFundDonations(String fundId) throws CrowdFundServiceException;

    Donation createDonation(Donation donation);

}
