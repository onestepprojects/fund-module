package org.onestep.relief.crowdfund.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.onestep.relief.crowdfund.model.Account;
import org.onestep.relief.crowdfund.model.Currency;
import org.onestep.relief.crowdfund.model.FundVisibility;

import javax.validation.constraints.Min;
import java.util.Date;

@Data
@NoArgsConstructor
public class FundRequest {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("escrow")
    private Account escrow;

    @JsonProperty("description")
    private String description;

    @JsonProperty("projectId")
    private String projectId;

    @JsonProperty("organizationId")
    private String organizationId;

    // start/end dates are accepted using epoch time
    @JsonProperty("startDate")
    private Date startDate;

    @JsonProperty("endDate")
    private Date endDate;

    @JsonProperty("goalAmount")
    @Min(value = 0L, message = "The value must be positive")
    private double goalAmount;

    @JsonProperty("currency")
    private Currency currency;

    @JsonProperty("fundVisibility")
    private FundVisibility fundVisibility;

    @JsonProperty("imageUrl")
    private String imageUrl;

    @JsonProperty("videoUrl")
    private String videoUrl;

    @JsonProperty("bannerImageUrl")
    private String bannerImageUrl;

    @JsonProperty("bodyLeftImageUrl")
    private String bodyLeftImageUrl;

    @JsonProperty("bodyLeftVideoUrl")
    private String bodyLeftVideoUrl;

    @JsonProperty("bodyRightImageUrl")
    private String bodyRightImageUrl;

    @JsonProperty("bodyRightVideoUrl")
    private String bodyRightVideoUrl;

    @JsonProperty("reportLeftImageUrl")
    private String reportLeftImageUrl;

    @JsonProperty("reportMiddleImageUrl")
    private String reportMiddleImageUrl;

    @JsonProperty("reportRightImageUrl")
    private String reportRightImageUrl;

}
