package org.onestep.relief.crowdfund.model;

/**
 * Models a transaction for a crowd fund.
 * Could be a donation, direct deposit or distribution of funds.
 */

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
@DynamoDBTable(tableName="Transactions")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="Transaction", description="Crowd fund transactions")
public class Transaction {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    @NotNull
    private String id;

    @DynamoDBAttribute
    private String senderAddress;

    @DynamoDBAttribute
    private String payId;

    // passphrase for sender. SHOULD NOT BE STORED IN DB
    @DynamoDBIgnore
    private String senderMnemonic;

    @DynamoDBAttribute
    @NotNull
    private String receiverAddress;

    @DynamoDBAttribute
    @NotNull
    private double amount;

    // can be null, if the donation was made w/o being logged into OneStep
    @DynamoDBAttribute
    private String personId;

    @DynamoDBAttribute
    @NotNull
    private String personName;

    @DynamoDBAttribute
    @NotNull
    private String fundId;

    @DynamoDBAttribute
    @NotNull
    private String transactionId;

    @DynamoDBAttribute
    @NotNull
    private long timestamp;

    @DynamoDBAttribute
    @NotNull
    @DynamoDBTypeConvertedEnum
    private TransactionType type;

    @DynamoDBAttribute
    @NotNull
    @DynamoDBTypeConvertedEnum
    private Currency currency;

}
