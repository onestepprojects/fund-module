package org.onestep.relief.crowdfund.model;

/**
 * Models a blockchain account per the Onestep Blockchain API.
 */

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Account {

    // pago paystring of blockchain account, if applicable
    @DynamoDBAttribute
    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String payId;

    // public address for blockchain account
    @DynamoDBAttribute
    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address;

    // mnemonic represents the secret key for a blockchain account; e.g. a passphrase
    // only required when not using pago
    @DynamoDBAttribute
    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mnemonic;

    public Account() {}

    /**
     * Constructs an <code>Account</code> by supplying address and mnemonic (passphrase).
     *
     * @param address blockchain address
     * @param mnemonic blockchain mnemonic
     */
    public Account(String address, String mnemonic) {
        this.address = address;
        this.mnemonic = mnemonic;
    }

    /**
     * Constructs an <code>Account</code> by supplying payId
     *
     * @param payId pago paystring
     */
    public Account(String payId) {
        this.payId = payId;
    }

}
