package org.onestep.relief.crowdfund.model;

/**
 * Types of stored transactions.
 * Donations are donations to funds; deposits would be non-donation direct deposits to funds;
 * and distribution would be the movement of money from a fund to another account.
 */

public enum TransactionType {
    DONATION,
    DEPOSIT,
    DISTRIBUTION
}
