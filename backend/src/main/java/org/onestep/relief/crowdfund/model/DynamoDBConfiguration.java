package org.onestep.relief.crowdfund.model;

import com.amazonaws.services.dynamodbv2.model.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.onestep.relief.crowdfund.Util;

/**
 * Stores the configuration settings for AWS Dynamo DB, which handles
 * persistence for the Fund module. Notably this class stores the
 * structure of the DB that gets built out when booting up the service,
 * a default region and the location of the DB (endpoint).
 */

public class DynamoDBConfiguration {

  // endpoint and region for DB; defaults here, but should be set in environment
  // (see below)
  private String region = "us-east-1";
  private String endpoint = "https://dynamodb.us-east-1.amazonaws.com";
  // private String region = "us-east-1";
  // private String endpoint = "http://localhost:8000";

  // endpoint env
  private String ENV_DB_REGION = "ONESTEP_CROWDFUND_DB_REGION";
  private String ENV_DB_ENDPOINT = "ONESTEP_CROWDFUND_DB_URL";

  // List of table names to set up, and maps for their definitions
  // Maps are Table name -> List<> pairs, and everything in the tableNames
  // list should be accounted for in the maps.
  private String prefix;
  private List<String> tableNames;
  private Map<String, List<KeySchemaElement>> keySchemaElementMap;
  private Map<String, List<AttributeDefinition>> attributeDefMap;
  private Map<String, ProvisionedThroughput> provisionedThroughputMap;
  private Map<String, GlobalSecondaryIndex> secondaryIndexMap;

  /**
   * Constructs a default configuration. Default endpoint is a local DB for
   * testing.
   * Also generates a "default" DB structure.
   */
  public DynamoDBConfiguration() {
    this.region = Util.getEnv(ENV_DB_REGION, region);
    this.endpoint = Util.getEnv(ENV_DB_ENDPOINT, endpoint);

    // default table names
    String tablePrefix = Util.getEnv("ONESTEP_CROWDFUND_DB_PREFIX", "");
    if (tablePrefix != "") {
      tablePrefix = tablePrefix + "_";
    }

    prefix = tablePrefix;

    tableNames = Arrays.asList(tablePrefix + "Funds", tablePrefix + "Transactions", tablePrefix + "Donation");

    // default table structures and definitions
    keySchemaElementMap = new HashMap<String, List<KeySchemaElement>>();
    attributeDefMap = new HashMap<String, List<AttributeDefinition>>();
    provisionedThroughputMap = new HashMap<String, ProvisionedThroughput>();
    secondaryIndexMap = new HashMap<String, GlobalSecondaryIndex>();

    ProvisionedThroughput throughput = new ProvisionedThroughput(10L, 10L);

    keySchemaElementMap.put("Funds", Arrays.asList(new KeySchemaElement("id", KeyType.HASH)));
    attributeDefMap.put("Funds", Arrays.asList(new AttributeDefinition("id", ScalarAttributeType.S),
        new AttributeDefinition("organizationId", ScalarAttributeType.S)));
    provisionedThroughputMap.put("Funds", throughput);
    GlobalSecondaryIndex fundOrgIndex = new GlobalSecondaryIndex().withIndexName("organizationId")
        .withProvisionedThroughput(throughput)
        .withKeySchema(new KeySchemaElement()
            .withAttributeName("organizationId")
            .withKeyType(KeyType.HASH))
        .withProjection(new Projection().withProjectionType("ALL"));
    secondaryIndexMap.put("Funds", fundOrgIndex);

    keySchemaElementMap.put("Transactions",
        Arrays.asList(new KeySchemaElement("id", KeyType.HASH)));
    attributeDefMap.put("Transactions",
        Arrays.asList(new AttributeDefinition("id", ScalarAttributeType.S),
            new AttributeDefinition("txId", ScalarAttributeType.S)));
    provisionedThroughputMap.put("Transactions", throughput);
    GlobalSecondaryIndex txIndex = new GlobalSecondaryIndex().withIndexName("txId")
        .withProvisionedThroughput(throughput)
        .withKeySchema(new KeySchemaElement().withAttributeName("txId").withKeyType(KeyType.HASH))
        .withProjection(new Projection().withProjectionType("ALL"));
    secondaryIndexMap.put("Transactions", txIndex);

    keySchemaElementMap.put("Donation",
        Arrays.asList(new KeySchemaElement("id", KeyType.HASH)));
    attributeDefMap.put("Donation",
        Arrays.asList(new AttributeDefinition("id", ScalarAttributeType.S),
            new AttributeDefinition("fundId", ScalarAttributeType.S)));
    provisionedThroughputMap.put("Donation", throughput);
    GlobalSecondaryIndex fundIndex = new GlobalSecondaryIndex().withIndexName("fundId")
        .withProvisionedThroughput(throughput)
        .withKeySchema(new KeySchemaElement().withAttributeName("fundId").withKeyType(KeyType.HASH))
        .withProjection(new Projection().withProjectionType("ALL"));
    secondaryIndexMap.put("Donation", fundIndex);
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getRegion() {
    return region;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  public String getEndpoint() {
    return endpoint;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public String getPrefix() {
    return prefix;
  }

  public List<String> getTableNames() {
    return tableNames;
  }

  public Map<String, List<KeySchemaElement>> getKeySchemaElementMap() {
    return keySchemaElementMap;
  }

  public Map<String, List<AttributeDefinition>> getAttributeDefMap() {
    return attributeDefMap;
  }

  public Map<String, ProvisionedThroughput> getProvisionedThroughputMap() {
    return provisionedThroughputMap;
  }

  public Map<String, GlobalSecondaryIndex> getSecondaryIndexMap() {
    return secondaryIndexMap;
  }
}
