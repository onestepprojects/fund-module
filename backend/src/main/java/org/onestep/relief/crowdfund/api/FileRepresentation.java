package org.onestep.relief.crowdfund.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileRepresentation {

    @JsonProperty
    private String name;

    @JsonProperty
    private String data;

    @NotNull
    @Pattern(regexp = "image/jpeg|image/png")
    private String type;

}
