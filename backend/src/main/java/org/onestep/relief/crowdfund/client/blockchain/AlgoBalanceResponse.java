package org.onestep.relief.crowdfund.client.blockchain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@EqualsAndHashCode()
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoBalanceResponse {

    @JsonProperty
    private double algoBalance;

    @JsonProperty
    private List<Asset> assetsList;

}