package org.onestep.relief.crowdfund.client.authorization;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@EqualsAndHashCode()
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponseData {

    @JsonProperty
    private String userId;

    @JsonProperty
    private String username;

    @JsonProperty
    private List<String> roles;

}
