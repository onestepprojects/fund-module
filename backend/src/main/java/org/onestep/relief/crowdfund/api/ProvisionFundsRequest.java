package org.onestep.relief.crowdfund.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ProvisionFundsRequest {

    @JsonProperty("funds")
    private List<FundRequest> funds;

}
