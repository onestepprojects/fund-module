package org.onestep.relief.crowdfund.client.blockchain;

public class BlockchainClientException extends Exception {

    private int statusCode;
    private String message;

    public BlockchainClientException(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public BlockchainClientException(String statusCode, String message) {
        this.statusCode = Integer.parseInt(statusCode);
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() { return message; }

}
