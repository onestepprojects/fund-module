package org.onestep.relief.crowdfund.service;

public class CrowdFundServiceException extends Exception {

    public CrowdFundServiceException(String message) {
        super(message);
    }

}
