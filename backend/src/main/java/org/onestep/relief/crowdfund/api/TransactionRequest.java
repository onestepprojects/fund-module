package org.onestep.relief.crowdfund.api;

import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.onestep.relief.crowdfund.model.Currency;

import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
public class TransactionRequest {

    @JsonProperty("id")
    private String id;

    @JsonProperty("payId")
    private String payId;

    @JsonProperty("receiverAddress")
    private String receiverAddress;

    @JsonProperty("amount")
    @Min(value = 0L, message = "The value must be positive")
    private double amount;

    @JsonProperty("currency")
    private Currency currency;

    @JsonProperty("fundId")
    private String fundId;

    @JsonProperty("personName")
    private String personName;

}
