package org.onestep.relief.crowdfund.api;

import lombok.Data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Data
public class Page<T> {
    private List<T> contents;
    private int total;
    private int page_number;
    private int page_size;

    public Page() {
    }

    public Page(List<T> contents, int total, int page_number, int page_size) {
        this.contents = contents;
        this.total = total;
        this.page_number = page_number;
        this.page_size = page_size;
    }

    public Page(Iterator<T> contentsIterator, int page_number, int page_size) {
        this.page_number = page_number;
        this.page_size = page_size != 0 ? page_size : 10;
        this.buildContents(contentsIterator);
    }

    private void buildContents(Iterator<T> contentsIterator) {
        int minResult = this.page_number * this.page_size;
        int maxResult = (page_number + 1) * this.page_size;
        contents = new ArrayList<>(page_size);
        while (contentsIterator.hasNext()) {
            T currentElement = contentsIterator.next();
            if (total >= minResult && total < maxResult) {
                contents.add(currentElement);
            }
            total++;
        }
    }
}
