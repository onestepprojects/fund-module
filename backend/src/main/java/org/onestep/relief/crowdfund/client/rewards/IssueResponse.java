package org.onestep.relief.crowdfund.client.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueResponse {

    @JsonProperty
    private String message;

    @JsonProperty
    private IssueResponseData data;

}
