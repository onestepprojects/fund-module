package org.onestep.relief.crowdfund.client.blockchain;

/**
 * A client class to access the OneStep blockchain API.
 */

import org.apache.http.HttpStatus;
import org.onestep.relief.crowdfund.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class BlockchainClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(BlockchainClient.class);

    // default URI and paths for blockchain service as of 4/15/21. can (should) be
    // set via env variables
    private String blockchainURL = "https://test.onesteprelief.org/onestep/blockchain";
    private String sendAlgoPath = "/transaction/sendAlgo";
    private String sendUsdcPath = "/transaction/sendUsdc";
    private String getAlgoBalancePath = "/account/getBalance/";
    private String getUsdcBalancePath = "/account/getBalance/usdc/";
    private String provisionPath = "/provision";

    // environment variable name(s)
    private final String ENV_API_URL = "ONESTEP_BLOCKCHAIN_URL";

    // max number of tries to poll provision account
    private final int MAX_PROVISION_ATTEMPTS = 10;

    // Build new jersey client
    private Client client = ClientBuilder.newClient();

    // Constructor attempts to set URL and paths based on environment vars
    public BlockchainClient() {
        blockchainURL = Util.getEnv(ENV_API_URL, blockchainURL);
    }

    /**
     * Calls REST API from blockchain service to provision a blockchain account by
     * seeding it with initial funds and opting into USDC assets. This is a "long"
     * running operation that first returns 202 along with a link to check process
     * status. Continues to return 202 and then returns 200 + account info on
     * success. Link is no longer active after retrieval.
     *
     * Will only poll "MAX_PROVISION_ATTEMPTS" times.
     *
     * @return ProvisionResponse - new account info
     * @throws BlockchainClientException if blockchain API fails
     */
    public ProvisionAccountResponse provisionAccount() throws BlockchainClientException {
        String path = blockchainURL + provisionPath;

        LOGGER.info("Attempting POST: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).buildPost(Entity.json(null));
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_ACCEPTED) {
            // provision request accepted. poll per response recommended time until account
            // is retrieved
            String statusUrl = response.getHeaderString("Location");
            statusUrl = statusUrl.replaceAll("http", "https"); // this occurs b/c of 308 issues otherwise. may need to
                                                               // revisit if target API fixes their end
            long retryTime = Long.parseLong(response.getHeaderString("Retry-After"));

            LOGGER.info("Retrieved provision account URL: " + statusUrl + " ... retry-time: " + retryTime);

            int tries = 0;
            while (response.getStatus() == HttpStatus.SC_ACCEPTED && tries < MAX_PROVISION_ATTEMPTS) {
                try {
                    LOGGER.info("Waiting...");
                    Thread.sleep(retryTime * 1000);

                    LOGGER.info("Attempting GET: " + statusUrl);
                    webTarget = client.target(statusUrl);
                    invocation = webTarget.request(MediaType.APPLICATION_JSON).buildGet();
                    response = invocation.invoke();

                    tries++;
                } catch (InterruptedException e) {
                    throw new BlockchainClientException(HttpStatus.SC_INTERNAL_SERVER_ERROR,
                            "Fatal error in blockchain client");
                }
            }

            if (response.getStatus() == HttpStatus.SC_ACCEPTED && tries >= MAX_PROVISION_ATTEMPTS) {
                throw new BlockchainClientException(HttpStatus.SC_INTERNAL_SERVER_ERROR,
                        "Provision account request timed out. Please try again later");
            }

            if (response != null && response.getStatus() == HttpStatus.SC_OK) {
                return response.readEntity(ProvisionAccountResponse.class);
            }
        }

        LOGGER.info("Provision account request failed");
        throw new BlockchainClientException(response.getStatus(), "Failed to provision account");
    }

    /**
     * Calls REST API from blockchain service to send algos.
     *
     * @param transactionRequest details of transaction
     * @return TransactionResponse - contains transaction ID (txId)
     * @throws BlockchainClientException if blockchain API fails
     */
    public TransactionResponse sendAlgo(TransactionRequest transactionRequest) throws BlockchainClientException {
        String path = blockchainURL + sendAlgoPath;

        LOGGER.info("Attempting POST: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                .buildPost(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            TransactionResponse transactionResponse = response.readEntity(TransactionResponse.class);
            return transactionResponse;
        } else {
            LOGGER.info("Send algo failed");
            throw new BlockchainClientException(response.getStatus(), "Send algo request failed");
        }
    }

    /**
     * Calls REST API from blockchain service to send USDC.
     *
     * @param transactionRequest details of transaction
     * @return TransactionResponse - contains transaction ID (txId)
     * @throws BlockchainClientException if blockchain API fails
     */
    public TransactionResponse sendUsdc(TransactionRequest transactionRequest) throws BlockchainClientException {
        String path = blockchainURL + sendUsdcPath;

        LOGGER.info("Attempting POST: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                .buildPost(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));
        Response response = invocation.invoke();

        System.out.println("Request: " + Entity.entity(transactionRequest, MediaType.APPLICATION_JSON).toString());
        System.out.println(response);

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            TransactionResponse transactionResponse = response.readEntity(TransactionResponse.class);
            return transactionResponse;
        } else {
            LOGGER.info("Send USDC failed");
            throw new BlockchainClientException(response.getStatus(), "Send USDC request failed");
        }
    }

    /**
     * Calls REST API from blockchain service to check an account Algo balance.
     *
     * @param address public address of account to check
     * @return balance of account + asset info
     * @throws BlockchainClientException if blockchain API fails
     */
    public double getAlgoBalance(String address) throws BlockchainClientException {
        String path = blockchainURL + getAlgoBalancePath + address;

        LOGGER.info("Attempting GET: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).buildGet();
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            AlgoBalanceResponse balanceResponse = response.readEntity(AlgoBalanceResponse.class);
            return balanceResponse.getAlgoBalance();
        } else {
            LOGGER.info("Get balance failed");
            throw new BlockchainClientException(response.getStatus(), "Get balance request failed");
        }
    }

    /**
     * Calls REST API from blockchain service to check an account USDC balance.
     *
     * @param address public address of account to check
     * @return balance of account + asset info
     * @throws BlockchainClientException if blockchain API fails
     */
    public double getUsdcBalance(String address) throws BlockchainClientException {
        String path = blockchainURL + getUsdcBalancePath + address;

        LOGGER.info("Attempting GET: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).buildGet();
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            UsdcBalanceResponse balanceResponse = response.readEntity(UsdcBalanceResponse.class);
            return balanceResponse.getAmount();
        } else {
            LOGGER.info("Get USDC balance failed");
            throw new BlockchainClientException(response.getStatus(), "Get USDC balance request failed");
        }
    }

}