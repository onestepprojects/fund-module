package org.onestep.relief.crowdfund.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.crowdfund.model.*;

import java.util.Date;

@Data
@NoArgsConstructor
public class FundResponse {
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("escrow")
    private Account escrow;

    @JsonProperty("description")
    private String description;

    @JsonProperty("creatorId")
    private String creatorId;

    @JsonProperty("projectId")
    private String projectId;

    @JsonProperty("organizationId")
    private String organizationId;

    @JsonProperty("startDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date startDate;

    @JsonProperty("endDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonProperty("goalAmount")
    private double goalAmount;

    @JsonProperty("currency")
    private Currency currency;

    @JsonProperty("fundState")
    public String getFundState() {
        Date today = new Date();

        if (today.before(startDate)) {
            return "WAITING_TO_START";
        }
        if (today.before(endDate)) {
            return "IN_PROGRESS";
        }
        return "CLOSED";
    }

    @JsonProperty("fundVisibility")
    private FundVisibility fundVisibility;

    @JsonProperty("imageUrl")
    private String imageUrl;

    @JsonProperty("videoUrl")
    private String videoUrl;

    @JsonProperty("bannerImageUrl")
    private String bannerImageUrl;

    @JsonProperty("bodyLeftImageUrl")
    private String bodyLeftImageUrl;

    @JsonProperty("bodyLeftVideoUrl")
    private String bodyLeftVideoUrl;

    @JsonProperty("bodyRightImageUrl")
    private String bodyRightImageUrl;

    @JsonProperty("bodyRightVideoUrl")
    private String bodyRightVideoUrl;

    @JsonProperty("reportLeftImageUrl")
    private String reportLeftImageUrl;

    @JsonProperty("reportMiddleImageUrl")
    private String reportMiddleImageUrl;

    @JsonProperty("reportRightImageUrl")
    private String reportRightImageUrl;

    @JsonProperty("totalAmountRaised")
    private double totalAmountRaised;

}
