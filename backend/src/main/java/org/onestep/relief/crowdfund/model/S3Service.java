package org.onestep.relief.crowdfund.model;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.util.Base64;

import org.onestep.relief.crowdfund.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.*;

public class S3Service {

    private final static Logger LOGGER = LoggerFactory.getLogger(S3Service.class);

    private final String S3_ID = Util.getEnv("AWS_ACCESS_KEY_ID", "");
    private final String S3_SECRET = Util.getEnv("AWS_SECRET_ACCESS_KEY", "");
    private final String S3_BUCKET = Util.getEnv("ONESTEP_CROWDFUND_S3_BUCKET", "fundresource");
    private final String S3_REGION = Util.getEnv("ONESTEP_CROWDFUND_S3_REGION", "us-east-1");
    private AmazonS3 s3client;

    /**
     * Initializes the service client with the necessary keys.
     */
    public S3Service() {
        AWSCredentials credentials = new BasicAWSCredentials(S3_ID, S3_SECRET);
        s3client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(S3_REGION)
                .build();
    }

    /**
     * Uploads a file to S3 bucket and returns file key for retrieval.
     *
     * @param id        ID for file (e.g. filepath for retrieval)
     * @param imageFile file to upload; contains data in base64
     * @throws IOException critical failure when trying to write bytes to file
     */
    public String put(String id, FundImageFile imageFile) throws IOException {
        LOGGER.info("Attempting to upload image to S3 bucket: " + S3_BUCKET);

        String path = id + '-' + imageFile.getName();

        // convert to a byte array from base64 and upload
        byte[] bytes = Base64.decode(imageFile.getData());
        InputStream stream = new ByteArrayInputStream(bytes);

        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(bytes.length);

        LOGGER.info("Successfully decoded data from base64 to file, uploading...");

        s3client.putObject(S3_BUCKET, path, stream, meta);

        // get and return image url
        String url = s3client.getUrl(S3_BUCKET, path).toExternalForm();

        LOGGER.info("Upload successful, returning URL: " + url);

        return url;
    }

    /**
     * Uploads a file to S3 bucket and returns file key for retrieval.
     *
     * @param filename filename without ext
     * @param dataUrl  file data url to upload
     */
    public String put(String filename, String dataUrl) {
        LOGGER.info("Attempting to upload image to S3 bucket: " + S3_BUCKET);

        if (dataUrl == null || dataUrl.isEmpty()) {
            return dataUrl;
        }

        // data:image/jpeg;base64,/9j/4AAQSkZ
        Pattern pattern = Pattern.compile("^data:(.+);base64,(.+)$");
        Matcher matcher = pattern.matcher(dataUrl);

        if (!matcher.find()) {
            return dataUrl;
        }

        String mediaType = matcher.group(1);
        String base64String = matcher.group(2);
        String ext = mediaType.split("/")[1];
        String path = filename + "." + ext;

        // convert to a byte array from base64 and upload
        byte[] bytes = Base64.decode(base64String);
        InputStream stream = new ByteArrayInputStream(bytes);

        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(bytes.length);

        LOGGER.info("Successfully decoded data from base64 to file, uploading...");

        s3client.putObject(S3_BUCKET, path, stream, meta);

        // get and return image url
        String url = s3client.getUrl(S3_BUCKET, path).toExternalForm();

        LOGGER.info("Upload successful, returning URL: " + url);

        return url;
    }

}
