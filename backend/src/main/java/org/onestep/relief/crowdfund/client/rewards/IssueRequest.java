package org.onestep.relief.crowdfund.client.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueRequest {

    @JsonProperty
    @NotEmpty
    private String reason;

    @JsonProperty
    @NotEmpty
    private String rewardType;

    @JsonProperty
    @NotNull
    @Positive
    private Integer amount;

    @JsonProperty
    @NotEmpty
    private String receiverId;

    @JsonProperty
    @NotEmpty
    private ReceiverType receiverType;

    public enum ReceiverType {
        Person,
        Org;
    }

}

