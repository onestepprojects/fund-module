package org.onestep.relief.crowdfund.api;

import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.onestep.relief.crowdfund.model.Currency;
import org.onestep.relief.crowdfund.model.TransactionType;

@Data
@NoArgsConstructor
public class TransactionResponse {

    @JsonProperty("id")
    private String id;

    @JsonProperty("senderAddress")
    private String senderAddress;

    @JsonProperty("payId")
    private String payId;

    @JsonProperty("receiverAddress")
    private String receiverAddress;

    @JsonProperty("amount")
    private double amount;

    @JsonProperty("currency")
    private Currency currency;

    @JsonProperty("personId")
    private String personId;

    @JsonProperty("personName")
    private String personName;

    @JsonProperty("fundId")
    private String fundId;

    @JsonProperty("transactionId")
    private String transactionId;

    @JsonProperty("timestamp")
    private long timestamp;

    @JsonProperty("type")
    private TransactionType type;

}
