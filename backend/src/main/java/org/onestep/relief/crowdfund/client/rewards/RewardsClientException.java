package org.onestep.relief.crowdfund.client.rewards;

public class RewardsClientException extends Exception {

    private int statusCode;
    // private String message;

    public RewardsClientException(int statusCode, String message) {
        this.statusCode = statusCode;
        // this.message = message;
    }

    public RewardsClientException(String statusCode, String message) {
        this.statusCode = Integer.parseInt(statusCode);
        // this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
