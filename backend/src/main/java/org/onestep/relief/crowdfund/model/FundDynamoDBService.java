package org.onestep.relief.crowdfund.model;

/**
 * This service interfaces directly with Dynamo and provides a simple API
 * for the actual Fund Service API to store Fund and Donation objects.
 */

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.google.inject.Inject;
import com.amazonaws.services.dynamodbv2.document.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FundDynamoDBService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FundDynamoDBService.class);

    DynamoDBMapper mapper;
    DynamoDBMapperConfig dynamoDBMapperConfig;

    /**
     * When the service is constructed, initializes the mapper by
     * building an AWS Dynamo client and then ensuring that the required
     * tables exist as per the passed-in DynamoDBConfiguration.
     *
     * @param dynamoDBConfiguration settings for the DB including table names and
     *                              endpoint
     */
    @Inject
    public FundDynamoDBService(DynamoDBConfiguration dynamoDBConfiguration) {
        // build client
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder
                .standard()
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(
                                dynamoDBConfiguration.getEndpoint(),
                                dynamoDBConfiguration.getRegion()))
                .build();

        // build mapper configuration
        this.dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder().withTableNameOverride(
                DynamoDBMapperConfig.TableNameOverride.withTableNamePrefix(dynamoDBConfiguration
                        .getPrefix()))
                .build();

        // init mapper
        this.mapper = new DynamoDBMapper(client, this.dynamoDBMapperConfig);
    }

    /**
     * Saves object to database. Object should be annotated for Dynamo.
     * Note that this will effectively blow away an existing object and replace it
     * with a new one,
     * if applicable.
     *
     * @param obj object to save
     */
    public void save(Object obj) {
        this.mapper.save(obj);
    }

    /**
     * Retrieves <code>Fund</code> from database.
     *
     * @param fundId ID (primary key) of fund to retrieve
     */
    public Fund loadFund(String fundId) {
        return this.mapper.load(Fund.class, fundId);
    }

    /**
     * Retrieves <code>Donation</code> from database.
     *
     * @param transactionId ID (primary key) of fund to retrieve
     */
    public Transaction loadTransaction(String transactionId) {
        return this.mapper.load(Transaction.class, transactionId);
    }

    /**
     * Deletes <code>Fund</code> from a table in the database.
     *
     * @param fundId to delete from
     * @return true if deleted
     */
    public boolean deleteFund(String fundId) {
        Fund fund = loadFund(fundId);
        if (fund != null) {
            this.mapper.delete(fund);
            return true;
        }
        return false;
    }

    /**
     * Deletes <code>Donation</code> from a table in the database.
     *
     * @param transactionId to delete from
     * @return true if deleted
     */
    public boolean deleteTransaction(String transactionId) {
        Transaction donation = loadTransaction(transactionId);
        if (donation != null) {
            this.mapper.delete(donation);
            return true;
        }
        return false;
    }

    /**
     * Returns a paginated list of all <code>Funds</code>.
     *
     * @return list of funds
     */
    public PaginatedScanList<Fund> scanFunds() {
        return this.mapper.scan(Fund.class, new DynamoDBScanExpression());
    }

    /**
     * Returns a paginated list of <code>Funds</code> based on a scan expression.
     *
     * @param dynamoDBScanExpression
     * @return filtered list of funds
     */
    public PaginatedScanList<Fund> scanFunds(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Fund.class, dynamoDBScanExpression);
    }

    /**
     * Returns a paginated list of <code>Funds</code> based on a query expression.
     *
     * @param dynamoDBQueryExpression
     * @return filtered list of funds
     */
    public PaginatedQueryList<Fund> queryFunds(DynamoDBQueryExpression dynamoDBQueryExpression) {
        return this.mapper.query(Fund.class, dynamoDBQueryExpression);
    }

    /**
     * Returns a paginated list of all <code>Donations</code>.
     *
     * @return list of donations
     */
    public PaginatedScanList<Transaction> scanTransactions() {
        return this.mapper.scan(Transaction.class, new DynamoDBScanExpression());
    }

    /**
     * Returns a paginated list of <code>Donations</code> based on a scan
     * expression.
     *
     * @param dynamoDBScanExpression
     * @return filtered list of donations
     */
    public PaginatedScanList<Transaction> scanTransactions(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Transaction.class, dynamoDBScanExpression);
    }

    /**
     * Returns a paginated list of <code>Donations</code> based on a scan
     * expression.
     *
     * @param dynamoDBScanExpression
     * @return filtered list of donations
     */
    public PaginatedScanList<Donation> scanDonations(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Donation.class, dynamoDBScanExpression);
    }
}
