package org.onestep.relief.crowdfund.service;

/**
 * This service interface bridges the back-end of Dynamo with the
 * REST API to store funds and donations for Onestep.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import org.onestep.relief.crowdfund.Util;
import org.onestep.relief.crowdfund.model.Currency;
import org.onestep.relief.crowdfund.model.*;

import org.onestep.relief.crowdfund.client.authorization.ServiceLoginResponse;
import org.onestep.relief.crowdfund.client.rewards.IssueRequest;
import org.onestep.relief.crowdfund.client.rewards.IssueResponse;
import org.onestep.relief.crowdfund.client.rewards.RewardsClient;
import org.onestep.relief.crowdfund.client.rewards.RewardsClientException;
import org.onestep.relief.crowdfund.client.authorization.AuthorizationClient;
import org.onestep.relief.crowdfund.client.authorization.AuthorizationClientException;
import org.onestep.relief.crowdfund.client.authorization.UserResponse;
import org.onestep.relief.crowdfund.client.organization.OrganizationClient;
import org.onestep.relief.crowdfund.client.organization.OrganizationClientException;
import org.onestep.relief.crowdfund.client.organization.OrgsByPersonResponse;
import org.onestep.relief.crowdfund.client.organization.PersonResponse;
import org.onestep.relief.crowdfund.client.blockchain.*;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CrowdFundService implements ICrowdFundService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrowdFundService.class);

    private static CrowdFundService serviceInstance = new CrowdFundService();

    // DB configuration/service and clients for blockchain, person-org, rewards &
    // auth APIs
    private DynamoDBConfiguration dbConfig = new DynamoDBConfiguration();
    private FundDynamoDBService dbService = new FundDynamoDBService(dbConfig);
    private S3Service s3service = new S3Service();
    private BlockchainClient blockchainClient = new BlockchainClient();
    private OrganizationClient orgClient = new OrganizationClient();
    private AuthorizationClient authClient = new AuthorizationClient();
    private RewardsClient rewardsClient = new RewardsClient();

    // allowed roles to create/update/distribute funds
    // TODO: reconsider how this is stored in the future
    private final List<String> REQ_ROLES = Arrays.asList("GLOBAL_ADMIN", "ADMIN", "RELIEFORGADMIN");

    // service client/secret keys for backend authentication (mostly for rewards
    // service as of 4/28/21)
    private final String CLIENT_ID = Util.getEnv("ONESTEP_CROWDFUND_CLIENT_ID", "");
    private final String SECRET_KEY = Util.getEnv("ONESTEP_CROWDFUND_SECRET_KEY", "");

    // reward values
    private final int CREATE_FUND_REWARD = 1000;
    private final int DONATION_REWARD = 1000;

    // default image on fund creation
    private final String DEFAULT_IMAGE_URL = "https://fundresource.s3.amazonaws.com/no_img.jpg";

    // singleton - constructor is private.
    private CrowdFundService() {
    }

    /**
     * Returns the singleton instance of the <code>CrowdFundService</code>.
     *
     * @return an instance of <code>CrowdFundService</code>
     */
    public static CrowdFundService getInstance() {
        return serviceInstance;
    }

    /**
     * Creates a new <code>Fund</code>. Escrow is null until created separately.
     *
     * @param fund      details of new <code>Fund</code>
     * @param authToken Bearer token
     * @return the new <code>Fund</code>
     * @throws CrowdFundServiceAuthenticationException if user is unauthorized to
     *                                                 create fund
     */
    public Fund createFund(Fund fund, String authToken) throws CrowdFundServiceAuthenticationException {
        LOGGER.info("Creating fund " + fund.getName() + "...");

        // authorization
        String personId = checkAccess(authToken, fund.getOrganizationId(), REQ_ROLES);

        // set initial values that are not provided by user & save fund
        fund.setCreatorId(personId);
        fund.setFundVisibility(FundVisibility.UNPUBLISHED);
        fund.setTotalAmountRaised(0.0);
        fund.setImageUrl(DEFAULT_IMAGE_URL);
        dbService.save(fund);

        // issue reward for creator
        issueReward("Created crowdfund: " + fund.getName() + " (ID: " + fund.getId() + ")", CREATE_FUND_REWARD,
                personId);

        return fund;
    }

    /**
     * Generates the escrow account for a fund in the blockchain, given fund ID.
     * NOTE: this returns the account's address AND secret passphrase;
     * THIS IS THE ONLY TIME THE MNEMONIC CAN BE RETRIEVED, as it is not stored in
     * the DB.
     *
     * @param fundId    ID of fund for which to generate escrow
     * @param authToken Bearer token
     * @return <code>Account</code> which contains escrow address/mnemonic
     * @throws CrowdFundServiceException               if fund ID not found, or if
     *                                                 blockchain fails
     * @throws CrowdFundServiceAuthenticationException if user is unauthorized to
     *                                                 create account
     */
    public Account createFundEscrow(String fundId, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException {
        try {
            Fund fund = getFund(fundId);

            // authorization
            String personId = checkAccess(authToken, fund.getOrganizationId(), REQ_ROLES);

            // cannot create escrow if it already has one or the fundraiser is over
            Date today = new Date();
            if (fund.getEscrow() != null || today.after(fund.getEndDate())) {
                throw new CrowdFundServiceException("This fund is closed and/or already has an escrow account");
            }

            // generate & set escrow in fund
            ProvisionAccountResponse newAccountInfo = blockchainClient.provisionAccount();
            LOGGER.info("Escrow account generated with address: " + newAccountInfo.getAddress());
            Account escrow = new Account(newAccountInfo.getAddress(), newAccountInfo.getMnemonic());
            fund.setEscrow(escrow);

            dbService.save(fund);
            return escrow;
        } catch (BlockchainClientException e) {
            throw new CrowdFundServiceException("Failed to create escrow account in blockchain - " +
                    e.getStatusCode() + " - " + e.getMessage());
        }
    }

    /**
     * Updates a <code>Fund</code>. Only certain fields can be updated.
     *
     * @param fund      fund to update
     * @param authToken Bearer token
     * @return the updated <code>Fund</code>
     * @throws CrowdFundServiceException               if fund ID not found or other
     *                                                 service failure
     * @throws CrowdFundServiceAuthenticationException if user is unauthorized for
     *                                                 selected fund
     */
    public Fund updateFund(Fund fund, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException {
        LOGGER.info("Updating fund ID: " + fund.getId() + "...");

        Fund fundToUpdate = getFund(fund.getId());

        // authorization
        String personId = checkAccess(authToken, fundToUpdate.getOrganizationId(), REQ_ROLES);

        // check and update updatable fields
        if (fund.getName() != null) {
            fundToUpdate.setName(fund.getName());
        }
        if (fund.getDescription() != null) {
            fundToUpdate.setDescription(fund.getDescription());
        }
        if (fund.getOrganizationId() != null) {
            fundToUpdate.setOrganizationId(fund.getOrganizationId());
        }
        fundToUpdate.setProjectId(fund.getProjectId());
        if (fund.getStartDate() != null) {
            fundToUpdate.setStartDate(fund.getStartDate());
        }
        if (fund.getEndDate() != null) {
            fundToUpdate.setEndDate(fund.getEndDate());
        }
        if (fund.getGoalAmount() != 0.0d) {
            fundToUpdate.setGoalAmount(fund.getGoalAmount());
        }
        if (fund.getImageUrl() != null) {
            fundToUpdate.setImageUrl(fund.getImageUrl());
        }
        if (fund.getVideoUrl() != null) {
            fundToUpdate.setVideoUrl(fund.getVideoUrl());
        }
        if (fund.getBannerImageUrl() != null) {
            fundToUpdate.setBannerImageUrl(fund.getBannerImageUrl());
        }
        if (fund.getBodyLeftImageUrl() != null) {
            fundToUpdate.setBodyLeftImageUrl(fund.getBodyLeftImageUrl());
        }
        if (fund.getBodyLeftVideoUrl() != null) {
            fundToUpdate.setBodyLeftVideoUrl(fund.getBodyLeftVideoUrl());
        }
        if (fund.getBodyRightImageUrl() != null) {
            fundToUpdate.setBodyRightImageUrl(fund.getBodyRightImageUrl());
        }
        if (fund.getBodyRightVideoUrl() != null) {
            fundToUpdate.setBodyRightVideoUrl(fund.getBodyRightVideoUrl());
        }
        if (fund.getReportLeftImageUrl() != null) {
            fundToUpdate.setReportLeftImageUrl(fund.getReportLeftImageUrl());
        }
        if (fund.getReportMiddleImageUrl() != null) {
            fundToUpdate.setReportMiddleImageUrl(fund.getReportMiddleImageUrl());
        }
        if (fund.getReportRightImageUrl() != null) {
            fundToUpdate.setReportRightImageUrl(fund.getReportRightImageUrl());
        }
        if (fund.getFundVisibility() != null) {
            fundToUpdate.setFundVisibility(fund.getFundVisibility());
        }

        uploadFundImageDataUrls(fundToUpdate);
        dbService.save(fundToUpdate);
        return fundToUpdate;
    }

    /**
     * Uploads image data urls for a <code>Fund</code>.
     *
     * @param fund fund
     */
    private void uploadFundImageDataUrls(Fund fund) {
        fund.setImageUrl(s3service.put(fund.getId() + "_cover", fund.getImageUrl()));
        fund.setBannerImageUrl(s3service.put(fund.getId() + "_banner", fund.getBannerImageUrl()));
        fund.setBodyLeftImageUrl(s3service.put(fund.getId() + "_body_left", fund.getBodyLeftImageUrl()));
        fund.setBodyRightImageUrl(s3service.put(fund.getId() + "_body_right", fund.getBodyRightImageUrl()));
        fund.setReportLeftImageUrl(s3service.put(fund.getId() + "_report_left", fund.getReportLeftImageUrl()));
        fund.setReportMiddleImageUrl(s3service.put(fund.getId() + "_report_middle", fund.getReportMiddleImageUrl()));
        fund.setReportRightImageUrl(s3service.put(fund.getId() + "_report_right", fund.getReportRightImageUrl()));
    }

    /**
     * Uploads an image for a <code>Fund</code>.
     *
     * @param fundId    ID of fund for image
     * @param file      <code>File</code> to upload to S3
     * @param authToken Bearer token
     * @return the updated <code>Fund</code>
     * @throws CrowdFundServiceException               if fund ID not found or other
     *                                                 service (auth, s3) failure
     * @throws CrowdFundServiceAuthenticationException if user is unauthorized for
     *                                                 selected fund
     */
    public Fund uploadFundImage(String fundId, FundImageFile file, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException, IOException {
        LOGGER.info("Uploading image: " + file.getName() + " for fund ID: " + fundId + "...");

        Fund fundToUpdate = getFund(fundId);

        // authorization
        String personId = checkAccess(authToken, fundToUpdate.getOrganizationId(), REQ_ROLES);

        // upload image to s3, update fund w/ image url and save
        String url = s3service.put(fundId, file);
        fundToUpdate.setImageUrl(url);
        dbService.save(fundToUpdate);

        return fundToUpdate;
    }

    /**
     * Makes a donation to a <code>Fund</code> by executing a
     * <code>Transaction</code>.
     *
     * @param transaction transaction to execute
     * @param authToken   Bearer token OR NULL, if anonymous donor
     * @return the successful donation as a <code>Transaction</code>
     * @throws CrowdFundServiceException if donation fails
     */
    public Transaction makeDonation(Transaction transaction, String authToken) throws CrowdFundServiceException {
        LOGGER.info("Making donation to fund ID: " + transaction.getFundId() + "...");

        Fund fund = getFund(transaction.getFundId());

        // can only donate to a fund if it has an escrow and today is between start &
        // end dates
        Date today = new Date();
        if (fund.getEscrow() == null || today.before(fund.getStartDate()) || today.after(fund.getEndDate())) {
            throw new CrowdFundServiceException(
                    "Escrow not created and/or cannot donate to a fund that has ended or not yet started");
        }

        // sender account contains only pago pay string
        Account sender = new Account(transaction.getPayId());

        // attempt transaction
        TransactionRequest request = new TransactionRequest(sender,
                fund.getEscrow(),
                transaction.getAmount(),
                "Donation to fund " + fund.getName() + " via onestepprojects.org");

        LOGGER.info(request.toString());

        try {
            TransactionResponse response = null;
            if (transaction.getCurrency() == Currency.ALGO) {
                response = blockchainClient.sendAlgo(request);
            } else if (transaction.getCurrency() == Currency.USDC) {
                response = blockchainClient.sendUsdc(request);
            } else {
                throw new CrowdFundServiceException("This currency type is not supported");
            }

            transaction.setType(TransactionType.DONATION);
            transaction.setTransactionId(response.getData());
            transaction.setReceiverAddress(fund.getEscrow().getAddress());

            // ensure we have some kind of name, though will be overridden if person is
            // logged in
            if (transaction.getPersonName() == null || transaction.getPersonName().trim().isEmpty()) {
                transaction.setPersonName("Anonymous");
            }

            // ID person, if applicable
            if (authToken != null) {
                try {
                    PersonResponse personResponse = orgClient.idPersonByAuthToken(authToken);
                    transaction.setPersonId(personResponse.getUuid());
                    transaction.setPersonName(personResponse.getName());

                    // issue reward for donor
                    issueReward("Donated to fund: " + fund.getName() + " (ID: " + fund.getId() + ")", DONATION_REWARD,
                            personResponse.getUuid());
                } catch (OrganizationClientException e) {
                    LOGGER.info(
                            "Unable to identify bearer (person) of auth token, but donation still allowed. Error logged with code + "
                                    + e.getStatusCode() + ", message: " + e.getMessage());
                }
            }

            transaction.setTimestamp(Instant.now().getEpochSecond());
        } catch (BlockchainClientException e) {
            throw new CrowdFundServiceException("Failed to make donation - " +
                    e.getStatusCode() + " - " + e.getMessage());
        }

        // increment fundraiser and then save transaction & fund
        fund.setTotalAmountRaised(fund.getTotalAmountRaised() + transaction.getAmount());
        LOGGER.info("Saving fund after donation");
        dbService.save(fund);
        LOGGER.info("Saving transaction...");
        dbService.save(transaction);

        return transaction;
    }

    /**
     * Distributes amount from a <code>Fund</code> by executing a
     * <code>Transaction</code>.
     *
     * @param transaction transaction to execute
     * @param authToken   Bearer token
     * @return the successful distribution as a <code>Transaction</code>
     * @throws CrowdFundServiceException               if distribution fails
     * @throws CrowdFundServiceAuthenticationException if user not authorized to
     *                                                 distribute funds
     */
    public Transaction distributeFunds(Transaction transaction, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException {
        LOGGER.info("Distributing funds from fund ID: " + transaction.getFundId() + "...");

        Fund fund = getFund(transaction.getFundId());

        // authorization
        String personId = checkAccess(authToken, fund.getOrganizationId(), REQ_ROLES);

        // can only send funds if it has a valid escrow
        // TODO: can an in progress fund "use" funds?
        if (fund.getEscrow() == null) {
            throw new CrowdFundServiceException("This fund has no escrow account to distribute from");
        }

        Account receiver = new Account();
        receiver.setAddress(transaction.getReceiverAddress());

        TransactionRequest request = new TransactionRequest(fund.getEscrow(),
                receiver,
                transaction.getAmount(),
                "Distribution to fund ID: " + fund.getId());

        try {
            TransactionResponse response = null;
            if (fund.getCurrency() == Currency.ALGO) {
                response = blockchainClient.sendAlgo(request);
            } else if (fund.getCurrency() == Currency.USDC) {
                response = blockchainClient.sendUsdc(request);
            } else {
                throw new CrowdFundServiceException("This currency type is not supported");
            }

            transaction.setType(TransactionType.DISTRIBUTION);
            transaction.setTransactionId(response.getData());
            transaction.setSenderAddress(fund.getEscrow().getAddress());
            transaction.setPersonId(personId);
            transaction.setTimestamp(Instant.now().getEpochSecond());

            PersonResponse personResponse = orgClient.idPersonByAuthToken(authToken);
            transaction.setPersonName(personResponse.getName());
        } catch (BlockchainClientException e) {
            throw new CrowdFundServiceException("Failed to distribute funds - " +
                    e.getStatusCode() + " - " + e.getMessage());
        } catch (OrganizationClientException e) {
            LOGGER.info("Unable to ID person to set name on transaction");
        }

        LOGGER.info("Fund distribution successful, saving transaction...");
        dbService.save(transaction);

        return transaction;
    }

    /**
     * Retrieves a specific fund based on ID.
     *
     * @param fundId ID of fund to retrieve
     * @return <code>Fund</code>
     * @throws CrowdFundServiceException if fund not found
     */
    public Fund getFund(String fundId) throws CrowdFundServiceException {
        LOGGER.info("Getting fund ID: " + fundId + "...");

        Fund fund = dbService.loadFund(fundId);

        if (fund == null) {
            throw new CrowdFundServiceException("Fund ID does not exist");
        }

        return fund;
    }

    /**
     * Retrieves all funds.
     *
     * @return A list of all funds scanned from the database, minus fund
     *         balances/transactions.
     */

    public List<Fund> getFunds(String organizationId, String projectId, String creatorId, String fundVisibility) {
        LOGGER.info("Getting all funds...");

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        ArrayDeque<String> conditions = new ArrayDeque<String>();

        if (organizationId != null) {
            conditions.add("organizationId = :organizationId");
            eav.put(":organizationId", new AttributeValue().withS(organizationId));
        }
        if (projectId != null) {
            conditions.add("projectId = :projectId");
            eav.put(":projectId", new AttributeValue().withS(projectId));
        }
        if (creatorId != null) {
            conditions.add("creatorId = :creatorId");
            eav.put(":creatorId", new AttributeValue().withS(creatorId));
        }
        if (fundVisibility != null) {
            conditions.add("fundVisibility = :fundVisibility");
            eav.put(":fundVisibility", new AttributeValue().withS(fundVisibility));
        }

        if (conditions.isEmpty()) {
            return dbService.scanFunds();
        }

        String conditionExpression = String.join(" and ", conditions);

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression(conditionExpression)
                .withExpressionAttributeValues(eav);

        return dbService.scanFunds(scanExpression);
    }

    /**
     * Retrieves all fund objects from the database for a particular Org ID.
     *
     * @param orgId ID of org to retrieve
     * @return list of funds for given org ID (queried, e.g. fast)
     */
    public List<Fund> getOrgFunds(String orgId) {
        LOGGER.info("Getting all funds for org ID: " + orgId);

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":orgId", new AttributeValue().withS(orgId));
        DynamoDBQueryExpression<Fund> queryExpression = new DynamoDBQueryExpression<Fund>()
                .withIndexName("organizationId").withConsistentRead(false)
                .withKeyConditionExpression("organizationId = :orgId").withExpressionAttributeValues(eav);

        return dbService.queryFunds(queryExpression);
    }

    /**
     * Retrieves funds based on fund visibility.
     *
     * @param fundVisibility - PUBLISHED or UNPUBLISHED
     * @return A list of all funds scanned from the database, minus fund
     *         balances/transactions,
     *         based on visibility (generally PUBLISHED or UNPUBLISHED).
     */

    public List<Fund> getFunds(FundVisibility fundVisibility) {
        LOGGER.info("Getting all funds with visibility: " + fundVisibility);

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":fundVisibility", new AttributeValue().withS(fundVisibility.toString()));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("fundVisibility = :fundVisibility").withExpressionAttributeValues(eav);

        List<Fund> funds = dbService.scanFunds(scanExpression);

        return funds;
    }

    /**
     * Retrieves all fund objects from the database for the authenticated user,
     * minus transactions/current balances.
     *
     * @param authToken Bearer token
     * @return A list of all funds scanned from the database, minus fund
     *         balances/transactions.
     * @throws CrowdFundServiceAuthenticationException if auth token is invalid
     */
    public List<Fund> getUserFunds(String authToken) throws CrowdFundServiceAuthenticationException {
        LOGGER.info("Getting all funds for user...");

        List<String> orgIds = getPersonOrgIds(authToken);

        if (!orgIds.isEmpty()) {
            List<Fund> funds = getOrgFunds(orgIds.get(0));
            int i = 1;
            while (i < orgIds.size()) {
                List<Fund> moreFunds = getOrgFunds(orgIds.get(i));
                funds = Stream.of(funds, moreFunds).flatMap(Collection::stream).collect(Collectors.toList());
                i++;
            }
            return funds;
        }

        // no orgs, return empty list
        return Collections.<Fund>emptyList();
    }

    /**
     * Retrieves a fund balance from the blockchain based on <code>Fund</code> ID.
     * The balance is returned in the same currency as the fund itself.
     *
     * @param fundId ID of fund balance to retrieve
     * @return <code>Fund</code> balance
     * @throws CrowdFundServiceException if fund not found, if escrow is null, or
     *                                   blockchain fails
     */
    public double getFundBalance(String fundId) throws CrowdFundServiceException {
        LOGGER.info("Getting balance for fund ID: " + fundId + "...");

        try {
            Fund fund = getFund(fundId);

            if (fund.getEscrow() == null) {
                throw new CrowdFundServiceException("Fund escrow has not been created");
            }

            if (fund.getCurrency() == Currency.ALGO) {
                double balance = blockchainClient.getAlgoBalance(fund.getEscrow().getAddress());
                return balance;
            } else if (fund.getCurrency() == Currency.USDC) {
                double balance = blockchainClient.getUsdcBalance(fund.getEscrow().getAddress());
                return balance;
            } else {
                throw new CrowdFundServiceException("Could not retrieve balance--this currency is not supported");
            }
        } catch (BlockchainClientException e) {
            throw new CrowdFundServiceException("Failed to retrieve fund balance for fund ID " + fundId +
                    " - " + e.getStatusCode() + " - " + e.getMessage());
        }
    }

    /**
     * A method to retrieve all transactions for a particular fund.
     *
     * @param fundId ID of fund to retrieve transactions for
     * @return A list of all transactions for a fund ID.
     * @throws CrowdFundServiceException if cannot find fund ID
     */

    public List<Transaction> getFundTransactions(String fundId) throws CrowdFundServiceException {
        LOGGER.info("Retrieving transactions for fund ID " + fundId);

        Fund fund = dbService.loadFund(fundId);

        if (fund == null) {
            throw new CrowdFundServiceException("Fund ID does not exist");
        }

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":fundId", new AttributeValue().withS(fund.getId()));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("fundId = :fundId").withExpressionAttributeValues(eav);

        List<Transaction> transactions = dbService.scanTransactions(scanExpression);
        return transactions;
    }

    /**
     * A method to retrieve all donations for a particular fund.
     *
     * @param fundId ID of fund to retrieve donations for
     * @return A list of all donations for a fund ID.
     * @throws CrowdFundServiceException if cannot find fund ID
     */

    public List<Donation> getFundDonations(String fundId) throws CrowdFundServiceException {
        LOGGER.info("Retrieving donations for fund ID " + fundId);

        Fund fund = dbService.loadFund(fundId);

        if (fund == null) {
            throw new CrowdFundServiceException("Fund ID does not exist");
        }

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":fundId", new AttributeValue().withS(fund.getId()));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("fundId = :fundId").withExpressionAttributeValues(eav);

        List<Donation> donations = dbService.scanDonations(scanExpression);
        return donations;
    }

    /**
     * Creates a new <code>Donation</code>. Escrow is null until created separately.
     *
     * @param donation details of new <code>Donation</code>
     * @return the new <code>Donation</code>
     */
    public Donation createDonation(Donation donation) {
        LOGGER.info("Creating donation " + donation.getName() + "...");

        donation.setCreatedAt(new Date());
        dbService.save(donation);

        return donation;
    }

    /**
     * Deletes a fund object from the database.
     *
     * @param fundId    <code>Fund</code> to delete
     * @param authToken Bearer token
     * @throws CrowdFundServiceException               if ID not found
     * @throws CrowdFundServiceAuthenticationException if unauthorized to delete
     *                                                 fund
     */
    public void closeFund(String fundId, String authToken)
            throws CrowdFundServiceException, CrowdFundServiceAuthenticationException {
        LOGGER.info("Deleting fund ID: " + fundId + "...");

        Fund fund = getFund(fundId);

        // authorization
        String personId = checkAccess(authToken, fund.getOrganizationId(), REQ_ROLES);

        dbService.deleteFund(fundId);
    }

    /**
     * Private method to validate access for certain operations.
     * Succeeds when user is a member of fund org ID and has at least 1 of required
     * roles for that organization.
     *
     * @param authToken       Bearer token
     * @param fundOrgId       organization ID for fund
     * @param allowedOrgRoles roles allowed for organization as a list
     * @return personId of logged in person on success
     * @throws CrowdFundServiceAuthenticationException
     */
    private String checkAccess(String authToken, String fundOrgId, List<String> allowedOrgRoles)
            throws CrowdFundServiceAuthenticationException {
        try {
            // validate auth token; this will throw exception if token is invalid
            UserResponse userResponse = authClient.getAuthUser(authToken);

            // retrieve person via auth token, then get their org-roles
            PersonResponse personResponse = orgClient.idPersonByAuthToken(authToken);
            List<OrgsByPersonResponse> orgsByPerson = orgClient.getOrgsByPerson(personResponse.getUuid());

            // check org roles. if person has a matching role, allow access
            for (OrgsByPersonResponse org : orgsByPerson) {
                if (org.getId().equals(fundOrgId) && !Collections.disjoint(allowedOrgRoles, org.getRoles())) {
                    return personResponse.getUuid();
                }
            }
        } catch (AuthorizationClientException e) {
            throw new CrowdFundServiceAuthenticationException("Failed to authenticate user");
        } catch (OrganizationClientException e) {
            // Auth failed in clients
            throw new CrowdFundServiceAuthenticationException(
                    "Authorization failed due to inability to access Organization service");
        }

        throw new CrowdFundServiceAuthenticationException(
                "User does not have required orgId and/or role for that orgId");
    }

    /**
     * Private method to validate auth token and retrieve orgIds for a person.
     *
     * @param authToken Bearer token
     * @return list of orgIds of logged in person on success
     * @throws CrowdFundServiceAuthenticationException
     */
    private List<String> getPersonOrgIds(String authToken) throws CrowdFundServiceAuthenticationException {
        try {
            // validate auth token; this will throw exception if token is invalid
            UserResponse userResponse = authClient.getAuthUser(authToken);

            // retrieve person via auth token, then get their org-roles
            PersonResponse personResponse = orgClient.idPersonByAuthToken(authToken);
            List<OrgsByPersonResponse> orgsByPerson = orgClient.getOrgsByPerson(personResponse.getUuid());

            return orgsByPerson.stream().map(o -> o.getId()).collect(Collectors.toList());
        } catch (AuthorizationClientException e) {
            throw new CrowdFundServiceAuthenticationException("Failed to authenticate user");
        } catch (OrganizationClientException e) {
            // Auth failed in clients
            throw new CrowdFundServiceAuthenticationException(
                    "Authorization failed due to inability to access Organization service");
        }
    }

    /**
     * Private method to issue a reward for some activity.
     * Since rewards are a non-critical function, failure is simply logged.
     *
     * @param reason   reason for reward
     * @param amount   # of step tokens to reward
     * @param personId person to reward
     * @return true on success
     */
    private boolean issueReward(String reason, int amount, String personId) {
        IssueRequest issueRequest = new IssueRequest(reason, "Step Token", amount, personId,
                IssueRequest.ReceiverType.Person);
        try {
            ServiceLoginResponse loginResponse = authClient.serviceLogin(CLIENT_ID, SECRET_KEY);
            String serviceAuthToken = loginResponse.getAccess_token();
            String serviceTokenType = loginResponse.getToken_type();
            IssueResponse issueResponse = rewardsClient.issueReward(issueRequest, serviceTokenType, serviceAuthToken);
            return true;
        }
        // rewards are a non-critical activity, so we simply log failure
        catch (AuthorizationClientException e) {
            LOGGER.info("Authorization failure while trying to issue reward while creating fund: " + e.getMessage());
        } catch (RewardsClientException e) {
            LOGGER.info("Failed to issue reward for creating fund: " + e.getMessage());
        }

        return false;
    }

}
