package org.onestep.relief.crowdfund.model;

/**
 * Models a crowd fund in the Onestep Relief Platform, mapped to DynamoDB.
 */

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.dropwizard.Configuration;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@DynamoDBTable(tableName = "Funds")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Fund", description = "Crowd fund")
public class Fund extends Configuration {

  @DynamoDBHashKey
  @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
  @NotNull
  private String id;

  @DynamoDBAttribute
  @NotNull
  private String name;

  @DynamoDBAttribute
  @NotNull
  private String creatorId;

  @DynamoDBAttribute
  private String description;

  @DynamoDBAttribute
  @NotNull
  private String organizationId;

  @DynamoDBAttribute
  private String projectId;

  @DynamoDBAttribute
  @DynamoDBTypeConvertedJson
  private Account escrow;

  @DynamoDBAttribute
  @NotNull
  private Date startDate;

  @DynamoDBAttribute
  @NotNull
  private Date endDate;

  @DynamoDBAttribute
  @NotNull
  private double goalAmount;

  @DynamoDBAttribute
  @NotNull
  @DynamoDBTypeConvertedEnum
  private Currency currency;

  @DynamoDBAttribute
  @NotNull
  @DynamoDBTypeConvertedEnum
  private FundVisibility fundVisibility;

  @DynamoDBAttribute
  @NotNull
  private double totalAmountRaised;

  @DynamoDBAttribute
  private String imageUrl;

  @DynamoDBAttribute
  private String videoUrl;

  @DynamoDBAttribute
  private String bannerImageUrl;

  @DynamoDBAttribute
  private String bodyLeftImageUrl;

  @DynamoDBAttribute
  private String bodyLeftVideoUrl;

  @DynamoDBAttribute
  private String bodyRightImageUrl;

  @DynamoDBAttribute
  private String bodyRightVideoUrl;

  @DynamoDBAttribute
  private String reportLeftImageUrl;

  @DynamoDBAttribute
  private String reportMiddleImageUrl;

  @DynamoDBAttribute
  private String reportRightImageUrl;

  public Fund() {
  }

}
