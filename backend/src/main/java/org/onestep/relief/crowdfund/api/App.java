package org.onestep.relief.crowdfund.api;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

/**
 * Entry point of the Crowd Fund backend Application.
 */

public class App extends Application<CrowdFundConfiguration> {

    private final static Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws Exception {
        LOGGER.info("OneStep Crowdfund backend started");

        new App().run(args);
    }

    @Override
    public void run(CrowdFundConfiguration defaultConfiguration, Environment environment) {
        // Enable CORS headers
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin,Authorization");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        final CrowdFundServiceResource defaultResource = new CrowdFundServiceResource();
        environment.jersey().register(defaultResource);
        environment.healthChecks().register("default", new DefaultHealthCheck());
    }

}