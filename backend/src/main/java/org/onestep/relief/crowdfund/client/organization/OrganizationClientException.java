package org.onestep.relief.crowdfund.client.organization;

public class OrganizationClientException extends Exception {

    private int statusCode;
    // private String message;

    public OrganizationClientException(int statusCode, String message) {
        this.statusCode = statusCode;
        // this.message = message;
    }

    public OrganizationClientException(String statusCode, String message) {
        this.statusCode = Integer.parseInt(statusCode);
        // this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
