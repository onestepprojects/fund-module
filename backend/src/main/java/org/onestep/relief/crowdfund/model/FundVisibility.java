package org.onestep.relief.crowdfund.model;

public enum FundVisibility {
    PUBLISHED,
    UNPUBLISHED
}
