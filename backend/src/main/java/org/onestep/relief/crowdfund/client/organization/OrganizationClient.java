package org.onestep.relief.crowdfund.client.organization;

import org.apache.http.HttpStatus;
import org.onestep.relief.crowdfund.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class OrganizationClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(OrganizationClient.class);

    // default URI and paths for org service as of 4/14/21. should be set via env in practice (see below)
    private String orgURL = "https://test.onesteprelief.org/onestep/org";
    private String idByAuthTokenPath = "/persons/requester/token";
    private String getPersonOrgRolesPath = "/organizations/get-org-roles-by-person/";

    // environment variable name(s)
    private final String ENV_API_URL = "ONESTEP_ORG_URL";

    // Build new jersey client
    private Client client = ClientBuilder.newClient();

    // Constructor attempts to set URL and paths based on environment vars
    public OrganizationClient() {
        orgURL = Util.getEnv(ENV_API_URL, orgURL);
    }

    /**
     * Calls REST API from org service to ID a Person based on their auth token
     * in the OneStep Relief platform.
     *
     * @param authToken Bearer token
     * @return PersonResponse - person info
     * @throws OrganizationClientException if ID fails or token is invalid
     */
    public PersonResponse idPersonByAuthToken(String authToken) throws OrganizationClientException {
        String path = orgURL + idByAuthTokenPath;

        LOGGER.info("Attempting GET: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                                         .header("Authorization", authToken)
                                         .buildGet();
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            return response.readEntity(PersonResponse.class);
        } else {
            throw new OrganizationClientException(response.getStatus(), "ID person by auth token failed");
        }
    }

    /**
     * Calls REST API from org service to get a person's org roles via person ID.
     *
     * @param personId Id of person for which to retrieve orgs
     * @return OrgsByPersonResponse - person org roles
     * @throws OrganizationClientException if unable to retrieve
     */
    public List<OrgsByPersonResponse> getOrgsByPerson(String personId) throws OrganizationClientException {
        String path = orgURL + getPersonOrgRolesPath + personId;

        LOGGER.info("Attempting GET: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).buildGet();
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            return response.readEntity(new GenericType<List<OrgsByPersonResponse>>() {});
        } else {
            throw new OrganizationClientException(response.getStatus(), "Failed to retrieve org roles for person ID " + personId);
        }
    }

}
