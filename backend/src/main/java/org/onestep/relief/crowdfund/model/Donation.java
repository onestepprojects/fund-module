package org.onestep.relief.crowdfund.model;

/**
 * Models a donation for a crowd fund.
 * Collecting user provided information before donate.
 */

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

import javax.validation.constraints.NotNull;

@Data
@ToString
@DynamoDBTable(tableName = "Donation")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Donation", description = "Crowd fund donations")

public class Donation {
    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    @NotNull
    private String name;

    @DynamoDBAttribute
    @NotNull
    private String email;

    @DynamoDBAttribute
    private String country;

    @DynamoDBAttribute
    @NotNull
    private long amount;

    @DynamoDBAttribute
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date createdAt;

    @DynamoDBAttribute
    private String fundId;
}
