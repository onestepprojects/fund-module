package org.onestep.relief.crowdfund.client.rewards;

import org.apache.http.HttpStatus;
import org.onestep.relief.crowdfund.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RewardsClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(RewardsClient.class);

    // default URI and paths for org service as of 4/14/21. should be set via env in practice (see below)
    private String rewardsURL = "https://test.onesteprelief.org/onestep/rewards";
    private String issuePath = "/reward/issue";

    // environment variable name(s)
    private final String ENV_API_URL = "ONESTEP_REWARDS_URL";

    // Build new jersey client
    private Client client = ClientBuilder.newClient();

    // Constructor attempts to set URL and paths based on environment vars
    public RewardsClient() {
        rewardsURL = Util.getEnv(ENV_API_URL, rewardsURL);
    }

    /**
     * Calls REST API from rewards service to issue an award.
     *
     * @param issueRequest details of rewards issue
     * @param tokenType type of token to award
     * @param serviceAuthToken auth token for service
     * @return IssueResponse details of successful reward
     * @throws RewardsClientException if there is a failure w/ API call
     */
    public IssueResponse issueReward(IssueRequest issueRequest, String tokenType, String serviceAuthToken) throws RewardsClientException {
        String path = rewardsURL + issuePath;

        LOGGER.info("Attempting POST: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                                         .header("Authorization", tokenType + " " + serviceAuthToken)
                                         .buildPost(Entity.entity(issueRequest, MediaType.APPLICATION_JSON));
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            IssueResponse issueResponse = response.readEntity(IssueResponse.class);
            return issueResponse;
        } else {
            LOGGER.info("Issue reward failed");
            System.out.println(response);
            throw new RewardsClientException(response.getStatus(), "Failed to issue reward");
        }
    }
}
