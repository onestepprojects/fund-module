package org.onestep.relief.crowdfund.client.blockchain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode()
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsdcBalanceResponse {

    @JsonProperty
    private double amount;

    @JsonProperty
    private String assetId;

    @JsonProperty
    private String name;

}