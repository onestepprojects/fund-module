package org.onestep.relief.crowdfund.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FundImageFile {

    @NotNull
    private String name;

    @NotNull
    private String data;

    @NotNull
    private String type;

}
