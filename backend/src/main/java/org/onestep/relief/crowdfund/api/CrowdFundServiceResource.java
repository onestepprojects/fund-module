package org.onestep.relief.crowdfund.api;

/**
 * The core REST API path definitions for the OneStep Crowd Fund Module.
 */

import com.google.inject.Inject;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.onestep.relief.crowdfund.model.*;
import org.onestep.relief.crowdfund.service.CrowdFundService;
import org.onestep.relief.crowdfund.service.CrowdFundServiceAuthenticationException;
import org.onestep.relief.crowdfund.service.CrowdFundServiceException;
import org.onestep.relief.crowdfund.service.ICrowdFundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Path("/crowdfund")
public class CrowdFundServiceResource {

    private final static Logger LOGGER = LoggerFactory.getLogger(CrowdFundServiceResource.class);

    @Inject
    private final ICrowdFundService crowdFundServiceInterface;
    private final DozerBeanMapper mapper;

    public CrowdFundServiceResource() {
        mapper = new DozerBeanMapper();

        List<String> mappingFiles = new ArrayList<>();
        mappingFiles.add("dozerBeanMapping.xml");
        mapper.setMappingFiles(mappingFiles);

        crowdFundServiceInterface = CrowdFundService.getInstance();
    }

    public static <T, U> List<U> mapCollection(final Mapper mapper, final Collection<T> source,
            final Class<U> destType) {
        final List<U> dest = new ArrayList<>();
        for (T element : source) {
            dest.add(mapper.map(element, destType));
        }
        return dest;
    }

    /**
     * POST - Creates a new fund. See <code>FundRequest</code> and
     * <code>FundResponse</code>.
     *
     * @param req       JSON representation of new fund details
     * @param authToken Bearer token
     * @return newly created fund details or appropriate error
     */
    @POST
    @Path("fund")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFund(@NotNull @Valid FundRequest req, @HeaderParam("Authorization") String authToken) {
        LOGGER.info("POST: /crowdfund/fund");
        LOGGER.debug(req.toString());

        // validate fields for fund creation
        if (req.getId() != null || req.getName() == null || req.getDescription() == null
                || req.getOrganizationId() == null || req.getStartDate() == null || req.getEndDate() == null
                || req.getCurrency() == null) {
            ErrorResponse error = new ErrorResponse(
                    "Cannot create fund - ensure you have all required fields and/or that fund ID is NOT included");
            return Response.status(Status.BAD_REQUEST).entity(error).build();
        }

        try {
            Fund newFund = crowdFundServiceInterface.createFund(mapper.map(req, Fund.class), authToken);
            return Response.status(Status.OK).entity(mapper.map(newFund, FundResponse.class)).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * GET - Creates an Escrow for fund via fund ID.
     *
     * @param fundId    unique ID of fund for which to create escrow account
     * @param authToken Bearer token
     * @return escrow address or appropriate error
     */
    @GET
    @Path("escrow/{fund-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createEscrow(@PathParam("fund-id") @NotEmpty String fundId,
            @HeaderParam("Authorization") String authToken) {
        LOGGER.info("GET: /escrow/" + fundId);

        try {
            Account escrow = crowdFundServiceInterface.createFundEscrow(fundId, authToken);
            return Response.status(Status.OK).entity(mapper.map(escrow, CreateEscrowResponse.class)).build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(e.getMessage())).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * PUT - updates a fund via fund ID. See <code>FundRequest</code> and
     * <code>FundResponse</code>.
     *
     * @param req       JSON representation of updated fund details
     * @param authToken Bearer token
     * @return updated fund details or appropriate error
     */
    @PUT
    @Path("fund")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateFund(@NotNull @Valid FundRequest req, @HeaderParam("Authorization") String authToken) {
        LOGGER.info("PUT: /crowdfund/fund");
        LOGGER.debug(req.toString());

        // validate fields for fund update
        if (req.getId() == null) {
            ErrorResponse error = new ErrorResponse("Cannot update fund - fund ID not provided");
            return Response.status(Status.BAD_REQUEST).entity(error).build();
        }

        try {
            Fund updatedFund = crowdFundServiceInterface.updateFund(mapper.map(req, Fund.class), authToken);
            return Response.status(Status.OK).entity(mapper.map(updatedFund, FundResponse.class)).build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.CONFLICT).entity(new ErrorResponse(e.getMessage())).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * PUT - Uploads an image for a fund.
     *
     * @param fundId ID of fund for image // * @param authToken Bearer token
     * @return updated fund
     */
    @PUT
    @Path("fund/image/{fund-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFundImage(FileRepresentation fileRep, @PathParam("fund-id") @NotEmpty String fundId,
            @HeaderParam("Authorization") String authToken) {
        LOGGER.info("PUT: /crowdfund/fund/image/" + fundId);
        // LOGGER.debug(file.toString());

        try {
            FundImageFile file = mapper.map(fileRep, FundImageFile.class);
            Fund updatedFund = crowdFundServiceInterface.uploadFundImage(fundId, file, authToken);
            return Response.status(Status.OK).entity(mapper.map(updatedFund, FundResponse.class)).build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.CONFLICT).entity(new ErrorResponse(e.getMessage())).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        } catch (IOException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * POST - Makes a donation to a fund. See <code>TransactionRequest</code> and
     * <code>TransactionResponse</code>.
     *
     * @param req       JSON representation of donation details
     * @param authToken Bearer token
     * @return completed transaction or appropriate error
     */
    @POST
    @Path("donation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response makeDonation(@NotNull @Valid TransactionRequest req,
            @HeaderParam("Authorization") String authToken) {
        LOGGER.info("POST: /crowdfund/donation");
        LOGGER.debug(req.toString());

        try {
            Transaction transaction = mapper.map(req, Transaction.class);
            Transaction completedTransaction = crowdFundServiceInterface.makeDonation(transaction, authToken);
            return Response.status(Status.OK).entity(mapper.map(completedTransaction, TransactionResponse.class))
                    .build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * POST - Makes a distribution from a fund. See <code>TransactionRequest</code>
     * and <code>TransactionResponse</code>.
     *
     * @param req       transaction details
     * @param authToken Bearer token
     * @return completed transaction or appropriate error
     */
    @POST
    @Path("distribution")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response distributeFunds(@NotNull @Valid TransactionRequest req,
            @HeaderParam("Authorization") String authToken) {
        LOGGER.info("POST: /crowdfund/distribution");
        LOGGER.debug(req.toString());

        try {
            Transaction transaction = mapper.map(req, Transaction.class);
            Transaction completedTransaction = crowdFundServiceInterface.distributeFunds(transaction, authToken);
            return Response.status(Status.OK).entity(mapper.map(completedTransaction, TransactionResponse.class))
                    .build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(e.getMessage())).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * GET - Retrieves a <code>Fund</code> by ID.
     *
     * @param fundId unique ID of fund to retrieve
     * @return Retrieved fund or 404
     */
    @GET
    @Path("fund/{fund-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFund(@PathParam("fund-id") @NotEmpty String fundId) {
        LOGGER.info("GET: /fund/" + fundId);

        try {
            Fund fund = crowdFundServiceInterface.getFund(fundId);

            return Response.status(Status.OK).entity(mapper.map(fund, FundResponse.class)).build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.NOT_FOUND).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * GET - Retrieves all funds.
     *
     * @return List of <code>FundResponse</code>s
     */
    @GET
    @Path("funds")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FundResponse> getFunds(@QueryParam("organizationId") String organizationId,
            @QueryParam("projectId") String projectId, @QueryParam("creatorId") String creatorId,
            @QueryParam("fundVisibility") String fundVisibility) {
        LOGGER.info("GET: /funds");
        return mapCollection(mapper,
                crowdFundServiceInterface.getFunds(organizationId, projectId, creatorId, fundVisibility),
                FundResponse.class);
    }

    /**
     * GET - Retrieves all funds for a given org ID.
     *
     * @param orgId ID of organization (org API)
     * @return List of <code>FundResponse</code>s
     */
    @GET
    @Path("funds/org/{org-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FundResponse> getOrgFunds(@PathParam("org-id") @NotEmpty String orgId) {
        LOGGER.info("GET: /funds/org/" + orgId);
        return mapCollection(mapper, crowdFundServiceInterface.getOrgFunds(orgId), FundResponse.class);
    }

    /**
     * GET - Retrieves all active funds, e.g. PUBLISHED and IN_PROGRESS.
     *
     * @return List of <code>FundResponse</code>s
     */
    @GET
    @Path("funds/active")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FundResponse> getActiveFunds() {
        LOGGER.info("GET: /funds/active");
        List<FundResponse> funds = mapCollection(mapper, crowdFundServiceInterface.getFunds(FundVisibility.PUBLISHED),
                FundResponse.class);
        return funds.stream().filter(f -> f.getFundState().equals("IN_PROGRESS")).collect(Collectors.toList());
    }

    /**
     * GET - Retrieves all funds given fund visibility.
     *
     * @param fundVis visibility of fund (generally PUBLISHED/UNPUBLISHED)
     * @return List of <code>FundResponse</code>s
     */
    @GET
    @Path("funds/visibility/{fund-vis}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FundResponse> getFundsByVisibility(@PathParam("fund-vis") @NotEmpty String fundVis) {
        LOGGER.info("GET: /funds/visibility/" + fundVis);

        try {
            FundVisibility fundVisibility = FundVisibility.valueOf(fundVis);
            return mapCollection(mapper, crowdFundServiceInterface.getFunds(fundVisibility), FundResponse.class);
        } catch (IllegalArgumentException e) {
            return Collections.<FundResponse>emptyList();
        }
    }

    /**
     * GET - Retrieves all funds for the authenticated user.
     *
     * @param authToken Bearer token
     * @return List of <code>FundResponse</code>s
     */
    @GET
    @Path("funds/user")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FundResponse> getFundsByUser(@HeaderParam("Authorization") String authToken) {
        LOGGER.info("GET: /funds/user");

        try {
            return mapCollection(mapper, crowdFundServiceInterface.getUserFunds(authToken), FundResponse.class);
        } catch (CrowdFundServiceAuthenticationException e) {
            return Collections.<FundResponse>emptyList();
        }
    }

    /**
     * GET - Retrieves an actual <code>Fund</code> balance by querying blockchain.
     *
     * @param fundId unique ID of fund to retrieve balance
     * @return <code>FundResponse</code> - retrieved fund or 500 error
     */
    @GET
    @Path("fund/{fund-id}/balance")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFundBalance(@PathParam("fund-id") @NotEmpty String fundId) {
        LOGGER.info("GET: /fund/" + fundId + "/balance");

        try {
            Double balance = crowdFundServiceInterface.getFundBalance(fundId);
            String msg = "{\"fundBalance\": " + balance + "}";
            return Response.status(Status.OK).entity(msg).build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * GET - Retrieves all transactions for a given fund.
     *
     * @param fundId ID of fund for which to retrieve transactions
     * @return List of <code>Transaction</code>s
     */
    @GET
    @Path("fund/{fund-id}/transactions")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransactionResponse> getFundTransactions(@PathParam("fund-id") @NotEmpty String fundId) {
        LOGGER.info("GET: /fund/" + fundId + "/transactions");

        try {
            return mapCollection(mapper, crowdFundServiceInterface.getFundTransactions(fundId),
                    TransactionResponse.class);
        } catch (CrowdFundServiceException e) {
            throw new WebApplicationException(e.getMessage(), Status.NOT_FOUND);
        }

    }

    /**
     * GET - Retrieves all donations for a given fund.
     *
     * @param fundId ID of fund for which to retrieve donations
     * @return List of <code>Donation</code>s
     */
    @GET
    @Path("fund/{fund-id}/donations")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Donation> getFundDonations(@PathParam("fund-id") @NotEmpty String fundId) {
        LOGGER.info("GET: /fund/" + fundId + "/donations");

        try {
            return mapCollection(mapper, crowdFundServiceInterface.getFundDonations(fundId),
                    Donation.class);
        } catch (CrowdFundServiceException e) {
            throw new WebApplicationException(e.getMessage(), Status.NOT_FOUND);
        }

    }

    /**
     * POST - Creates a new donation. See <code>DonationRequest</code> and
     * <code>DonationResponse</code>.
     *
     * @param req       JSON representation of new fund details
     * @param authToken Bearer token
     * @return newly created fund details or appropriate error
     */
    @POST
    @Path("donations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createDonation(@NotNull @Valid Donation req) {
        LOGGER.info("POST: /crowdfund/donations");
        LOGGER.debug(req.toString());

        Donation newDonation = crowdFundServiceInterface.createDonation(mapper.map(req, Donation.class));
        return Response.status(Status.OK).entity(mapper.map(newDonation, Donation.class)).build();
    }

    /**
     * DELETE - Deletes a <code>Fund</code> by ID.
     *
     * @param fundId    ID of fund to delete
     * @param authToken Bearer token
     * @return 204 on success or 404 if ID not found
     */
    @DELETE
    @Path("fund/{fund-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteFund(@PathParam("fund-id") @NotEmpty String fundId,
            @HeaderParam("Authorization") String authToken) {
        try {
            crowdFundServiceInterface.closeFund(fundId, authToken);
            return Response.status(Status.NO_CONTENT).build();
        } catch (CrowdFundServiceException e) {
            return Response.status(Status.NOT_FOUND).entity(new ErrorResponse(e.getMessage())).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

    /**
     * POST - creates many funds.
     *
     * NOTE: this is mostly for testing purposes to provision the module with test
     * data. As a result, error handling, etc. is kept to a minimum. THAT SAID, the
     * method calls createFund, and therefore the caller must have authorization for
     * all added funds. Recommended to use a "super user" account to expedite this.
     *
     * @param fundRequests JSON representation of new fund details--list of funds
     * @param authToken    Bearer token
     * @return 204 on success
     */
    @POST
    @Path("create-funds")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response provisionFunds(List<FundRequest> fundRequests, @HeaderParam("Authorization") String authToken) {
        LOGGER.info("POST: /crowdfund/provision-test-funds");

        try {
            for (FundRequest fundReq : fundRequests) {
                Fund newFund = mapper.map(fundReq, Fund.class);
                crowdFundServiceInterface.createFund(newFund, authToken);
            }
            return Response.status(Status.NO_CONTENT).build();
        } catch (CrowdFundServiceAuthenticationException e) {
            return Response.status(Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }

}
