package org.onestep.relief.crowdfund.service;

public class CrowdFundServiceAuthenticationException extends Exception {

    public CrowdFundServiceAuthenticationException(String message) {
        super(message);
    }

}
