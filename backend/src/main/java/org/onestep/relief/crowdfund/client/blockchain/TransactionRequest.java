package org.onestep.relief.crowdfund.client.blockchain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.onestep.relief.crowdfund.model.Account;

@Data
@ToString
@EqualsAndHashCode()
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionRequest {

    @JsonProperty("sender")
    private Account sender;

    @JsonProperty("receiver")
    private Account receiver;

    @JsonProperty("amount")
    private double amount;

    @JsonProperty("note")
    private String note;

    public TransactionRequest(Account sender, Account receiver,
                              double amount, String note) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.note = note;
    }

}
