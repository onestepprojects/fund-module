package org.onestep.relief.crowdfund.client.organization;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode()
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonResponse {

    @JsonProperty("uuid")
    private String uuid;

    @JsonProperty("name")
    private String name;

}
