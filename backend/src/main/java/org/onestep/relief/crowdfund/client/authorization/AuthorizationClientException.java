package org.onestep.relief.crowdfund.client.authorization;

public class AuthorizationClientException extends Exception {

    private int statusCode;
    // private String message;

    public AuthorizationClientException(int statusCode, String message) {
        this.statusCode = statusCode;
        // this.message = message;
    }

    public AuthorizationClientException(String statusCode, String message) {
        this.statusCode = Integer.parseInt(statusCode);
        // this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
