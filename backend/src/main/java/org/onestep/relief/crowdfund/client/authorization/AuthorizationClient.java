package org.onestep.relief.crowdfund.client.authorization;

import org.apache.http.HttpStatus;
import org.onestep.relief.crowdfund.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Base64;

public class AuthorizationClient {
    private final static Logger LOGGER = LoggerFactory.getLogger(AuthorizationClient.class);

    // default URI and paths for org service as of 4/23/21. should be set via env in
    // practice (see below)
    private String authURL = "https://test.onesteprelief.org/onestep/authorization";
    private String userPath = "/user";
    private String serviceLoginPath = "/servicelogin";

    // environment variable name(s)
    private final String ENV_API_URL = "ONESTEP_AUTHORIZATION_URL";

    // Build new jersey client
    private Client client = ClientBuilder.newClient();

    // Constructor attempts to set URL and paths based on environment vars
    public AuthorizationClient() {
        authURL = Util.getEnv(ENV_API_URL, authURL);
    }

    /**
     * Calls REST API from auth service to verify an auth token.
     *
     * @param authToken auth token to check
     * @return PersonResponse - person info
     * @throws AuthorizationClientException if auth fails, or in case of general
     *                                      failure
     */
    public UserResponse getAuthUser(String authToken) throws AuthorizationClientException {
        String path = authURL + userPath;

        LOGGER.info("Attempting GET: " + path);
        LOGGER.info("with header Authorization: " + authToken);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).header("Authorization", authToken)
                .buildGet();
        Response response = invocation.invoke();

        if (response != null) {
            UserResponse userResponse = response.readEntity(UserResponse.class);

            // successful authorization; return user info
            if (response.getStatus() == HttpStatus.SC_OK) {
                return userResponse;
            }

            // not authorized, throw exception with status & message from auth service
            else {
                throw new AuthorizationClientException(response.getStatus(), userResponse.getMessage());
            }
        }

        // something else went wrong
        throw new AuthorizationClientException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Authorization client error");
    }

    /**
     * Calls REST API from auth service to retrieve access token for a service.
     *
     * @param clientId  client ID for service
     * @param secretKey secret key for client
     * @return ServiceLoginResponse - contains access token
     * @throws AuthorizationClientException if auth or API fails
     */
    public ServiceLoginResponse serviceLogin(String clientId, String secretKey) throws AuthorizationClientException {
        String path = authURL + serviceLoginPath;

        // encode clientId:secretKey as base64
        String concatIdKey = clientId + ":" + secretKey;
        String encodedString = Base64.getEncoder().encodeToString(concatIdKey.getBytes());

        LOGGER.info("Attempting POST: " + path);

        WebTarget webTarget = client.target(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic " + encodedString).buildPost(Entity.json(null));
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            ServiceLoginResponse serviceLoginResponse = response.readEntity(ServiceLoginResponse.class);
            return serviceLoginResponse;
        } else {
            LOGGER.info("Service authentication failed");
            throw new AuthorizationClientException(response.getStatus(), "Failed to retrieve auth token for service");
        }
    }

}
