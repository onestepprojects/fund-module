package org.onestep.relief.crowdfund.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateEscrowResponse {

    // public address of blockchain account
    @JsonProperty("address")
    private String address;

    // private/secret key/passphrase for account
    @JsonProperty("mnemonic")
    private String mnemonic;

}
